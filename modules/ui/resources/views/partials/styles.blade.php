@if (env('UI_USE_DEV_ASSETS'))
    <link rel="stylesheet" href="{{ asset(compiled_assets('/components.css')) }}">
    <link rel="stylesheet" href="{{ asset(compiled_assets('/generic.css')) }}">
    <link rel="stylesheet" href="{{ asset(compiled_assets('/objects.css')) }}">
    <link rel="stylesheet" href="{{ asset(compiled_assets('/elements.css')) }}">
    <link rel="stylesheet" href="{{ asset(compiled_assets('/utilities.css')) }}">
@else
    <style>
        {!! file_get_contents(public_path(compiled_assets('/styles.css'))) !!}
    </style>
@endif
