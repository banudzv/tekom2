<?php

namespace WezomCms\Orders\Http\Requests\Site\Cart;

use Illuminate\Foundation\Http\FormRequest;

class AddProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|int|exists:products',
            'count' => 'nullable|int|min:1',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'id' => __('cms-orders::site.cart.Product'),
            'count' => __('cms-orders::site.cart.Incorrect amount'),
        ];
    }
}
