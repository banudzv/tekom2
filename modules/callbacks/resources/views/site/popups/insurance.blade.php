@php
    $services = $services ?? null;
@endphp

<div class="popup popup--only-text">
    <div class="popup__text">{!! settings('contacts.site.popup_text') !!}</div>
</div>

