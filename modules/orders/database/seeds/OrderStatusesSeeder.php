<?php

namespace WezomCms\Orders\Database\Seeds;

use Illuminate\Database\Seeder;
use WezomCms\Orders\Models\OrderStatus;

/**
 * Class OrderStatusesSeeder
 * @package WezomCms\Orders\Database\Seeds
 */
class OrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => OrderStatus::NEW,
                'name' => 'Новый',
                'deletable' => false,
            ],
            [
                'id' => OrderStatus::DONE,
                'name' => 'Завершен',
                'deletable' => false,
            ],
            [
                'id' => OrderStatus::CANCELED,
                'name' => 'Отменен',
                'deletable' => false,
            ],
        ];

        foreach ($data as $index => $datum) {
            $obj = new OrderStatus();
            $obj->id = $datum['id'];
            $obj->sort = $index;
            $obj->deletable = $datum['deletable'];

            $translatable = [];

            foreach (app('locales') as $locale => $language) {
                $translatable[$locale] = [
                    'name' => $datum['name'],
                ];
            }

            $obj->fill($translatable);
            $obj->save();
        }
    }
}
