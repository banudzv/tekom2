<?php

namespace WezomCms\About\Http\Controllers\Admin\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\Contracts\GroupRequest;
use WezomCms\About\Models\Contracts\Group;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class GroupController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Group::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.contracts.group';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-contract-groups';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = GroupRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Contract groups');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}

