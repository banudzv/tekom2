<?php

namespace WezomCms\OurTeam\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Branches\Models\Branch;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\ImageAttachable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 * \WezomCms\OurTeam\Models\Employee
 *
 * @property int $id
 * @property bool $published
 * @property int $sort
 * @property string|null $image
 * @property string|null $email
 * @property string|null $birthday
 * @property array|null $phones
 * @property bool $for_about_page
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \WezomCms\OurTeam\Models\EmployeeTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\OurTeam\Models\EmployeeTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee wherePhones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\OurTeam\Models\Employee withTranslation()
 * @mixin \Eloquent
 * @mixin EmployeeTranslation
 */
class Employee extends Model
{
    use ImageAttachable;
    use Translatable;
    use Filterable;
    use PublishedTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'sort', 'email', 'phones', 'for_about_page', 'birthday'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name', 'position', 'address', 'experience_description', 'education', 'work_description'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'for_about_page' => 'bool',
        'published' => 'bool',
        'phones' => 'array'
    ];

    protected $dates = [
        'birthday'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return array
     */
    public function imageSettings(): array
    {
        return ['image' => 'cms.our-team.our-team.images'];
    }

    public function getImage()
    {
        return url($this->getImageUrl(null, 'image'));
    }

    public function branch()
    {
        return $this->hasOne(Branch::class);
    }
}
