<?php

namespace WezomCms\Services\ModelFilters;

use EloquentFilter\ModelFilter;
use WezomCms\Core\Contracts\Filter\FilterListFieldsInterface;
use WezomCms\Core\Filter\FilterField;
use WezomCms\Services\Models\Service;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\RiskGroupRepository;

/**
 * Class ServiceFilter
 * @package WezomCms\Services\ModelFilters
 * @mixin Service
 */
class RiskFilter extends ModelFilter implements FilterListFieldsInterface
{
    /**
     * Generate array with fields
     * @return iterable|FilterField[]
     */
    public function getFields(): iterable
    {
        $repository = app(RiskGroupRepository::class);

        return [
            FilterField::makeName(),
            FilterField::make()
                ->name('group_id')
                ->label(__('cms-services::admin.risk.Group name'))
                ->type(FilterField::TYPE_SELECT)
                ->options($repository->getBySelect())
                ->class('js-select2')
                ->placeholder(__('cms-core::admin.layout.Not set')),
        ];
    }
    public function name($name)
    {
        $this->related('translations', 'name', 'LIKE', '%' . $name . '%');
    }

    public function Group($groupId)
    {
        $this->where('group_id', $groupId);
    }
}
