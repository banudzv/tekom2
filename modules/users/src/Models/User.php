<?php

namespace WezomCms\Users\Models;

use Greabock\Tentacles\EloquentTentacle;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Users\Notifications\ResetPassword;
use WezomCms\Users\Notifications\ResetPasswordByCode;
use WezomCms\Users\Notifications\VerifyAccount;

/**
 * \WezomCms\Users\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $email_verified_at
 * @property string|null $registered_through
 * @property string $password
 * @property bool $active
 * @property string|null $remember_token
 * @property int|null $temporary_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Users\Models\SocialAccount[] $socialAccounts
 * @method static Builder|User filter($input = array(), $filter = null)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|User query()
 * @method static Builder|User simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|User whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLike($column, $value, $boolean = 'and')
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User whereRegisteredThrough($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereSurname($value)
 * @method static Builder|User whereTemporaryCode($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static Builder|User whereManagerId($value)
 * @method static Builder|User active()
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Filterable;
    use Notifiable;
    use EloquentTentacle;

    public const EMAIL = 'email';
    public const PHONE = 'phone';
    public const TEMPORARY_CODE_LENGTH = 4;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['active', 'name', 'surname', 'email', 'phone', 'registered_through', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['active' => 'bool'];

    /**
     * @return mixed
     */
    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @param  Builder  $query
     */
    public function scopeActive(Builder $query)
    {
        $query->where('active', true);
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        try {
            $this->notify(new VerifyAccount());
        } catch (\Exception $e) {
            logger('sendEmailVerificationNotification exception', [
                'action' => 'Send the email verification notification',
                'user' => $this->id,
                'message' => $e->getMessage(),
            ]);

            // If there was an error sending SMS - mark user as verified.
            if ($this->markEmailAsVerified()) {
                event(new Verified($this));
            }
        }
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the password reset notification.
     *
     * @return bool
     */
    public function sendPasswordResetByCodeNotification()
    {
        try {
            $this->notify(new ResetPasswordByCode());

            return true;
        } catch (\Exception $e) {
            logger('sendPasswordResetByCodeNotification exception', [
                'action' => 'Send the password reset notification',
                'user' => $this->id,
                'message' => $e->getMessage(),
            ]);

            return false;
        }
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
            'temporary_code' => null,
        ])->save();
    }

    /**
     * @return null|string
     */
    public function routeNotificationForTurboSms()
    {
        return $this->phone;
    }

    /**
     * @return null|string
     */
    public function routeNotificationForESputnik()
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function generateTemporaryCode()
    {
        $length = User::TEMPORARY_CODE_LENGTH;
        $this->temporary_code = mt_rand(1 . str_repeat(0, $length - 1), str_repeat(9, $length));

        return $this->save();
    }

    /**
     * @param  string  $value
     * @return string
     */
    public static function emailOrPhone($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) ? User::EMAIL : User::PHONE;
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim("{$this->name} {$this->surname}");
    }

    /**
     * @param  string  $search
     * @param  int  $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function search(string $search = null, int $limit = 10)
    {
        $query = User::query();

        if ($search) {
            $query->orWhere(function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('surname', 'LIKE', '%' . $search . '%');
            });
        }

        return $query->paginate($limit);
    }
}
