<?php

namespace WezomCms\Pages\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Pages\Models\Page;

class PageController extends SiteController
{
    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug)
    {
        /** @var Page $obj */
        $obj = Page::publishedWithSlug($slug)->firstOrFail();

        $this->setLangSwitchers($obj, 'page.inner');

        // Breadcrumbs
        $this->addBreadcrumb($obj->name, $obj->getFrontUrl());

        // SEO
        $this->seo()
            ->setPageName($obj->name)
            ->setTitle($obj->title)
            ->setH1($obj->h1)
            ->setDescription($obj->description)
            ->metatags()
            ->setKeywords($obj->keywords);

        // Render
        return view('cms-pages::site.index', [
            'obj' => $obj,
        ]);
    }
}
