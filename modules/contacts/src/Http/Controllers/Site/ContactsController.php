<?php

namespace WezomCms\Contacts\Http\Controllers\Site;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Notification;
use WezomCms\Branches\Repositories\BranchRepository;
use WezomCms\Contacts\Http\Requests\Site\ContactRequest;
use WezomCms\Contacts\Models\Contact;
use WezomCms\Contacts\Notifications\ContactNotification;
use WezomCms\Core\Facades\NotifyMessage;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Models\Administrator;
use WezomCms\Core\Traits\ImageFromSettings;
use WezomCms\Regions\Repositories\RegionsRepository;

class ContactsController extends SiteController
{
    use ImageFromSettings;

    /**
     * @var RegionsRepository
     */
    private $regionsRepository;
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    private $centerCoords = [];
    private $branchesCoords = [];
    private $settings;

    public function __construct(
        RegionsRepository $regionsRepository,
        BranchRepository $branchRepository
    )
    {
        $this->regionsRepository = $regionsRepository;
        $this->branchRepository = $branchRepository;
        $this->settings = settings('contacts.site', []);
    }

    /** https://docs.google.com/spreadsheets/d/1blKFz4oCEC2BMFlA_BwlauzKPtvj6ssA/edit#gid=1823837287
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $branchIds = $this->branchRepository->getRegionsId();
        $regions = $this->regionsRepository->getFrontBySelect($branchIds);

        $branches = [];
        $selectedId = null;

        if(isset($request['region']) && !empty($request['region'])){

            $region = $this->regionsRepository->getBySlug($request['region']);
            $branches = $region->branches->load(['employee', 'city']);
            $selectedId = $region->id;
            if($region->cities->where('center', true)->isNotEmpty()){
                $this->getCenterCoords($region->cities);
                $this->getBranchesCoords($branches);
            }

        } else {
            $mainBranch = $this->branchRepository->getMainBranch();
            if($mainBranch){
                $branches = $mainBranch->city->region->branches->load(['employee', 'city']);
                $this->getCenterCoords($mainBranch->city->region->cities);
                $this->getBranchesCoords($branches);
                $selectedId = $mainBranch->city->region->id;
            }
        }

        $pageName = array_get($this->settings, 'name');

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('contacts'));

        // SEO
        $this->seo()
            ->setCanonical(route('contacts'))
            ->setPageName($pageName)
            ->setTitle(array_get($this->settings, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setDescription(array_get($this->settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($this->settings, 'keywords'));

        // Render
        return view('cms-contacts::site.index',[
            'banner' => $this->getBannerUrl('contacts.page'),
            'subTitle' => array_get($this->settings, 'sub_title'),
            'regions' => $regions,
            'branches' => $branches,
            'centerCoords' => $this->centerCoords,
            'branchesCoords' => $this->branchesCoords,
            'selectedId' => $selectedId
        ]);
    }

    private function getCenterCoords(Collection $collections)
    {
        if($collections->where('center', true)->isNotEmpty()){
            $center = array_values($collections->where('center', true)->toArray());
            $this->centerCoords['lat'] = (float)$center[0]['lat'];
            $this->centerCoords['lng'] = (float)$center[0]['lon'];
        }
    }

    private function getBranchesCoords(Collection $collections)
    {
        foreach($collections as $key => $item){
            if($item->lat && $item->lon){
                $position['lat'] = (float)$item->lat;
                $position['lng'] = (float)$item->lon;
            } else {
                $position['lat'] = (float)$item->city->lat;
                $position['lng'] = (float)$item->city->lon;
            }
            $arr['position'] = (object)$position;
            $this->branchesCoords[] = (object)$arr;
        }
    }

    /**
     * @param  ContactRequest  $request
     * @return JsResponse
     */
    public function form(ContactRequest $request)
    {
        $data = $request->all('phone', 'email', 'contract_number');
        $data['name'] = $request->get('name');
        $data['message'] = strip_tags($request->get('message'));

        try {
            $contact = new Contact($data);
            $contact->save();

            if($contact->save()){

                $administrators = Administrator::toNotifications('callback-consultations.edit')->get();
                Notification::send($administrators, new ContactNotification($contact));

                return JsResponse::make()
                    ->reset()
                    ->notification(
                        NotifyMessage::success(__('cms-callbacks::site.Form successfully submitted!')));
            } else {
                return JsResponse::make()
                    ->reset()
                    ->notification(NotifyMessage::error(__('cms-callbacks::site.Error creating request!')));
            }

        } catch (\Exception $exception){

            return JsResponse::make()
                ->reset(false)
                ->notification(NotifyMessage::error($exception->getMessage()));
        }
    }
}
