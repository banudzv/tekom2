import tippy from 'tippy.js';

export default function ($elements) {
	$elements.each(function () {
		const $this = $(this);
		const $container = $this.find('[data-tooltip-container]');
		const params = $this.data('tooltip');
		const content = params.content || $container.get(0).innerHTML;
		const customOptions = params.options || {};

		if (content) {
			const options = Object.assign({
				allowHTML: true,
				content: content,
				placement: 'top',
				theme: 'red',
				arrow: true,
				interactive: true,
				animation: 'scale',
				flip: false,
				flipOnUpdate: false,
				flipBehavior: ['top'],
				appendTo: $this.get(0)
			}, customOptions);

			tippy($this.get(0), options);
		}
	});
}
