@php
    /**
     * @var $viewPath string
     */
    $tabs = [
        __('cms-core::admin.layout.Main data') => $viewPath . '.tabs.main',
        __('cms-core::admin.tabs.seo') => $viewPath . '.tabs.seo',
        __('cms-core::admin.tabs.relation element') => $viewPath . '.tabs.relations',
    ];
@endphp

<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @tabs($tabs)
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h5 class="py-2"><strong>@lang('cms-core::admin.layout.Main data')</strong></h5>
            </div>
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('published', __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published') !!}
                </div>
                @if(config('cms.services.services.use_groups'))
                    <div class="form-group">
                        {!! Form::label('service_group_id', __('cms-services::admin.Group')) !!}
                        {!! Form::select('service_group_id', $groups, null, ['class' => 'js-select2']) !!}
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label(str_slug('icon'), __('cms-core::admin.layout.Icon')) !!}
                    <div class="input-group">
                        {!! Form::select('icon', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon', $obj->icon), ['class' => 'js-select2']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('calculate_link', __('cms-services::admin.Calculate link')) !!}
                    {!! Form::text('calculate_link', old('calculate_link', $obj->calculate_link)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image', __('cms-services::admin.Image')) !!}
                    {!! Form::imageUploader('image', $obj, route($routeName . '.delete-image', $obj->id)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image_banner', __('cms-services::admin.Image banner')) !!}
                    {!! Form::imageUploader('image_banner', $obj, route($routeName . '.delete-image', ['id' => $obj->id, 'field' => 'image_banner'])) !!}
                </div>
            </div>
        </div>
    </div>
</div>
