<?php

Route::namespace('WezomCms\\Catalog\\Http\\Controllers\\Admin')
    ->group(function () {
        // Categories
        Route::adminResource('categories', 'CategoriesController')->settings();

        // Products
        Route::adminResource('products', 'ProductController')->softDeletes()->settings();
        Route::get('products/search', ['uses' => 'ProductController@search', 'as' => 'products.search']);
        Route::post('products/{id}/set-sort', ['uses' => 'ProductController@setSort', 'as' => 'products.set-sort']);
        Route::post('products/change-category-popup', 'ProductController@changeCategoryPopup')
            ->name('products.change-category-popup');
        Route::post('products/change-category', 'ProductController@changeCategory')->name('products.change-category');
        Route::get('products/copy/{id}', 'ProductController@copy')->name('products.copy');

        if (config('cms.catalog.brands.enabled')) {
            // Brands
            Route::adminResource('brands', 'BrandsController')->settings();
            Route::get('brands/search', ['uses' => 'BrandsController@search', 'as' => 'brands.search']);
            Route::get('brands/get-all', ['uses' => 'BrandsController@getAll', 'as' => 'brands.get-all']);
            Route::post('brands/{id}/set-sort', ['uses' => 'BrandsController@setSort', 'as' => 'brands.set-sort']);
        }

        if (config('cms.catalog.models.enabled')) {
            // Models
            Route::adminResource('models', 'ModelsController')->settings();
            Route::get('models/search', ['uses' => 'ModelsController@search', 'as' => 'models.search']);
        }

        // SEO templates
        Route::adminResource('catalog-seo-templates', 'CatalogSeoTemplatesController')->settings();

        // Specifications
        Route::adminResource('specifications', 'SpecificationsController')->settings();
        Route::get(
            'spec-values/{specification_id}',
            ['uses' => 'SpecValuesController@list', 'as' => 'spec-values.list']
        );
        Route::post(
            'spec-values/{specification_id}/update-sort',
            ['uses' => 'SpecValuesController@updateSort', 'as' => 'spec-values.update-sort']
        );
        Route::post(
            'spec-values/{specification_id}/create',
            ['uses' => 'SpecValuesController@create', 'as' => 'spec-values.create']
        );
        Route::post(
            'spec-values/{specification_id}/save',
            ['uses' => 'SpecValuesController@save', 'as' => 'spec-values.save']
        );
        Route::post(
            'spec-values/{id}/delete',
            ['uses' => 'SpecValuesController@delete', 'as' => 'spec-values.delete']
        );

        Route::settings('search', 'SearchController');
    });
