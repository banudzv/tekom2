<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutContractOwnershipGroupTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_contract_ownership_group_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id');
            $table->string('locale')->index();
            $table->string('name');

            $table->unique(['group_id','locale'], 'ownership_group_translations_group_id_locale_unique');
            $table->foreign('group_id', 'contract_ownership_groups')
                ->references('id')
                ->on('about_contract_ownership_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_contract_ownership_group_translations');
    }
}

