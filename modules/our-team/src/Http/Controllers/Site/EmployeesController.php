<?php

namespace WezomCms\OurTeam\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\OurTeam\Models\Employee;

class EmployeesController extends SiteController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = settings('employees.site', []);

        // Selection
        $result = Employee::published()
            ->orderBy('sort')
            ->latest('id')
            ->paginate(array_get($settings, 'limit', 10));

        // Breadcrumbs
        $this->addBreadcrumb(array_get($settings, 'name'), route('our-team'));

        // SEO
        $this->seo()
            ->setPageName(array_get($settings, 'name'))
            ->setTitle(array_get($settings, 'title'))
            ->setH1(array_get($settings, 'h1'))
            ->setSeoText(array_get($settings, 'text'))
            ->setDescription(array_get($settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'))
            ->setNext($result->nextPageUrl())
            ->setPrev($result->previousPageUrl());

        // Render
        return view('cms-our-team::site.index', compact('result'));
    }
}
