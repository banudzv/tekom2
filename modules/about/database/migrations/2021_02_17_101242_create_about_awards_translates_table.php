<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutAwardsTranslatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('about_award_translations', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('award_id');
			$table->string('locale')->index();
			$table->string('name')->nullable();
			$table->mediumText('text')->nullable();

			$table->unique(['award_id', 'locale']);
			$table->foreign('award_id')
				->references('id')
				->on('about_awards')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('about_award_translations');
	}
}

