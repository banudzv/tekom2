<?php

class EmployeeSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('employees')->truncate();
        \DB::table('employee_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                factory(\WezomCms\OurTeam\Models\Employee::class,10)->create();

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}



