<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideLinkTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_link_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('slide_link_id');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->string('link')->nullable();

            $table->unique(['slide_link_id', 'locale']);
            $table->foreign('slide_link_id')
                ->references('id')
                ->on('slide_links')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide_link_translations');
    }
}

