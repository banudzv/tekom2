<?php

use WezomCms\Ui\Widgets;

return [
    'all_icons' => 'svg/all_icons.svg',
    'svg' => [
        'sprite' => 'svg/icons.svg',
        'adminSprite' => 'svg/icons.svg',
        'accident_insurance' => 'svg/accident_insurance.svg',
        'agricultural_insurance' => 'svg/agricultural_insurance.svg',
        'air_transport_insurance' => 'svg/air_transport_insurance.svg',
        'animal_insurance' => 'svg/animal_insurance.svg',
        'avtograzhdanka' => 'svg/avtograzhdanka.svg',
        'car_insurance' => 'svg/car_insurance.svg',
        'cargo_insurance' => 'svg/cargo_insurance.svg',
        'construction_and_installation_risks_insurance' => 'svg/construction_and_installation_risks_insurance.svg',
        'health_insurance' => 'svg/health_insurance.svg',
        'insurance_of_vegetables_and_fruit' => 'svg/insurance_of_vegetables_and_fruit.svg',
        'liability_insurance' => 'svg/liability_insurance.svg',
        'property_insurance' => 'svg/property_insurance.svg',
        'railway_transport_insurance' => 'svg/railway_transport_insurance.svg',
        'travel_insurance' => 'svg/travel_insurance.svg',
        'water_transport_insurance' => 'svg/water_transport_insurance.svg',
        'prefix' => '',
    ],
    'widgets' => [
        // Site
        'ui:header' => Widgets\Header::class,
        'ui:breadcrumbs' => Widgets\Breadcrumbs::class,
        'ui:footer' => Widgets\Footer::class,
        'ui:hidden-data' => Widgets\HiddenData::class,
        'ui:lang-switcher' => Widgets\LangSwitcher::class,
        'ui:share' => Widgets\Share::class,
		'ui:button' => Widgets\Button::class,
		'ui:search-button' => Widgets\SearchBlock::class,
		'ui:search' => Widgets\Search::class,
		'ui:mobile-header' => Widgets\MobileHeader::class,
		'ui:socials-widget' => Widgets\SocialsWidget::class,
		'ui:heading-section' => Widgets\HeadingSection::class,
    ],
    'pagination' => [
        'default' => 'cms-ui::pagination.wezom',
        'simple' => 'cms-ui::pagination.simple-wezom',
    ],
];
