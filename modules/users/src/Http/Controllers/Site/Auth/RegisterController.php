<?php

namespace WezomCms\Users\Http\Controllers\Site\Auth;

use Auth;
use Hash;
use Illuminate\View\View;
use Illuminate\Http\Request;
use WezomCms\Core\Rules\Phone;
use WezomCms\Users\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Auth\Events\Registered;
use WezomCms\Users\Rules\EmailOrPhone;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use Illuminate\Contracts\View\Factory as ViewFactory;
use WezomCms\Users\Http\Requests\Auth\RegisterRequest;

class RegisterController extends SiteController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return ViewFactory|View
     */
    public function showRegistrationForm()
    {
        return view('cms-users::site.auth.popups.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  RegisterRequest  $request
     * @return RedirectResponse|JsResponse
     */
    public function register(RegisterRequest $request)
    {
        // $login = $request['login'];
        // $loginField = User::emailOrPhone($login);

        // $arr =[
        //     'name' => $request['username'],
        //     'phone' => $request['phone'],
        //     $loginField => $login,
        //     'registered_through' => $loginField,
        //     'active' => true,
        //     'password' => Hash::make($request['password']),
        // ];

        // $validatedData = [
        //     'name' => ['required', 'string', 'max:255'],
        //     User::PHONE => ['nullable', 'string', new Phone()],
        //     'email' => ['required', 'string', 'unique:users'],
        //     'password' => [
        //         'required',
        //         'string',
        //         'min:' . config('cms.users.users.password_min_length'),
        //         'max:255',
        //         'confirmed'
        // ]
        // ];
        // dd($request);
        event(new Registered($user = $this->create($request->all())));

        Auth::guard()->login($user);

        if ($request->expectsJson()) {
            return JsResponse::make()->redirect(route('cabinet'));
        } else {
            return redirect()->route('cabinet');
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $login = $data['email'];
        $loginField = User::emailOrPhone($login);

        return User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            $loginField => $login,
            'registered_through' => $loginField,
            'active' => true,
            'password' => Hash::make($data['password']),
        ]);
    }
}
