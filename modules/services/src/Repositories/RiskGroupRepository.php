<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\RiskGroup;

class RiskGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return RiskGroup::class;
    }

    public function getBySelect()
    {
        return $this->getModelQuery()
            ->orderBy('sort')
            ->get()
            ->pluck('name', 'id')
            ->toArray();
    }

    public function getBySelectWithRisk(): array
    {
        $data = [];

        $query = $this->getModelQuery()
            ->with('risks')
            ->orderBy('sort')
            ->get()
            ->toArray();

        foreach ($query ?? [] as $items){
            foreach ($items['risks'] ?? [] as $item){
                $data[$items['name']][$item['id']] = $item['name'];
            }
        }

        return $data;
    }
}
