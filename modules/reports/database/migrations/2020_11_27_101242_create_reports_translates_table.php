<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTranslatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_translations', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('report_id');
			$table->string('locale')->index();
			$table->string('name')->nullable();

			$table->unique(['report_id', 'locale']);
			$table->foreign('report_id')
				->references('id')
				->on('reports')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('report_translations');
	}
}

