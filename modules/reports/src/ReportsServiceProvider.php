<?php

namespace WezomCms\Reports;

use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\Assets\AssetManagerInterface;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;

class ReportsServiceProvider extends BaseServiceProvider
{
	use SidebarMenuGroupsTrait;

	/**
	 * All module widgets.
	 *
	 * @var array|string|null
	 */
	protected $widgets = 'cms.reports.reports.widgets';

    /**
     * Dashboard widgets.
     *
     * @var array|string|null
     */
    protected $dashboard = 'cms.reports.reports.dashboards';

	/**
	 * @param  PermissionsContainerInterface  $permissions
	 */
	public function permissions(PermissionsContainerInterface $permissions)
	{
		$permissions->add('reports', __('cms-reports::admin.Reports'))->withEditSettings();
		$permissions->add('report-categories', __('cms-reports::admin.Categories report'));
	}

	public function adminMenu()
	{
        $group = $this->contentGroup()
            ->add(__('cms-reports::admin.Reports'), route('admin.reports.index'))
            ->data('icon', 'fa-file')
            ->nickname('reports');

        $group->add(__('cms-reports::admin.Reports'), route('admin.reports.index'))
            ->data('permission', 'reports.view')
            ->data('icon', 'fa-file')
            ->data('position', 1);

        $group->add(__('cms-reports::admin.Categories report'), route('admin.report-categories.index'))
            ->data('permission', 'report-categories.view')
            ->data('icon', 'fa-object-group')
            ->data('position', 3);

	}

    protected function afterBootForAdminPanel()
    {
        app(AssetManagerInterface::class)->addJs('vendor/cms/report/report.js', 'report');
    }

    /**
     * @return array
     */
    public function sitemap()
    {
        return [
            [
                'id' => 'reports',
                'parent_id' => 0,
                'name' => settings('report.site.name'),
                'url' => route('reports'),
            ],
        ];
    }

    /**
     * @param  SitemapXmlGeneratorInterface  $sitemap
     */
    public function sitemapXml(SitemapXmlGeneratorInterface $sitemap)
    {
        $sitemap->addLocalizedRoute('reports');
    }
}
