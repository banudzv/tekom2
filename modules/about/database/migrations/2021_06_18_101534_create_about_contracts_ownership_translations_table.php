<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutContractsOwnershipTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_contracts_ownership_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contract_id');
            $table->string('locale')->index();
            $table->string('name')->nullable();

            $table->unique(['contract_id', 'locale']);
            $table->foreign('contract_id', 'contract_contracts_ownership_key')
                ->references('id')
                ->on('about_contracts_ownership')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_contracts_ownership_translations');
    }
}
