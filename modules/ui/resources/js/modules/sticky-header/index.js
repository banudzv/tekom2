import $ from 'jquery';
import throttle from 'lodash.throttle';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el instanceof StickyHeader) {
			return true;
		}

		const instance = new StickyHeader($el);
		instance.init();
	});
}

class StickyHeader {
	constructor () {
		if (typeof StickyHeader.instance === 'object') {
			return StickyHeader.instance;
		}

		this.$sHead = $('[data-sticky-header]');
		this.$window = $(window);
		this.eventS = 'scroll';
		this.scrolledout = null;
		this.scrollDown = 'down';
		this.scrollUp = 'up';
		this.sdif = false;
		this.startPos = 0;
		this._min = this.headerHeight * 3;
		this.scrollHide = Math.max(this._min, 150);
		this.scrollShow = Math.max(this._min, 150);
		this.classHide = 'is-page-scrolled';

		StickyHeader.instance = this;
		return this;
	}

	get headerHeight () {
		return this.$sHead.outerHeight();
	}

	hide () {
		this.scrolledout = true;
		this.sdif = false;
		this.$sHead.addClass(this.classHide);
	}

	show () {
		this.scrolledout = false;
		this.sdif = false;
		this.$sHead.removeClass(this.classHide);
	}

	sHead () {
		let pos = this.$window.scrollTop();
		let dif = this.startPos - pos;
		let dir = dif < 0 ? this.scrollDown : this.scrollUp;

		dif = Math.abs(dif);
		this.startPos = pos;

		if (this.scrolledout === null) {
			this.scrolledout = pos < this.scrollShow;
		}

		if (this.scrolledout) {
			if (this.sdif === false) {
				this.sdif = pos;
			}
			if (dir === this.scrollUp) {
				if (pos < this.scrollShow || (this.sdif - pos > 4)) {
					this.show();
				}
			} else if (dif > 40) {
				this.sdif = false;
			}
		} else {
			if (dir === this.scrollDown)	{
				if (pos > this.scrollHide && dif > 4) {
					this.hide();
				}
			}
		}

		if (pos > 0) {
			this.$sHead.addClass('moved');
		} else {
			this.$sHead.removeClass('moved');
		}
	}

	init () {
		this.$window.on(this.eventS, throttle(() => this.sHead(), 100)).trigger(this.eventS);
	}
}
