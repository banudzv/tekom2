@extends('cms-ui::layouts.main')

@php
    /**
     * @var $article \WezomCms\Articles\Models\Article
     * @var $articles \WezomCms\Articles\Models\Article[]
     * @var $tag \WezomCms\Articles\Models\ArticleTag
     * @var $articlesListLink string
     */

    $tags = [];
    foreach($article->tags ?? [] as $tag) {
        array_push($tags, (object)[
            'name' => $tag->name,
            'url' =>  $tag->getFrontUrl()
        ]);
    }
@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $article->getImage(),
        'title' => SEO::getH1(),
        'classes' => 'heading-section--blog-inner',
        'blogPanel' => (object)[
            'date' => $article->publishedHuman,
            'tags' => $tags
        ]
    ])

    <div class="section section--bg-grey section--blog-inner">
        <div class="container container--theme-blog-inner">
            @widget('ui:share')
            <div class="wysiwyg wysiwyg--theme-default js-import" data-wrap-media data-draggable-table>
                {!! $article->text !!}
            </div>

            <div class="news-nav _grid _grid--3">
                @if ($prev = $article->getPrev())
                    <a href="{{ $prev->getFrontUrl() }}" class="news-nav__button news-nav__button--left">
                        <div class="news-nav__btn-icon">
                            @svg('arrow-left-thick', 12, 24, '')
                        </div>
                        <div class="news-nav__btn-text">
                            @lang('cms-articles::site.Предыдущая статья')
                        </div>
                    </a>
                @else
                    <span></span>
                @endif
                <a href="{{ $articlesListLink }}" class="news-nav__button news-nav__button--center">
                    <div class="news-nav__btn-icon">
                        @svg('menu2', 30, 30, '')
                    </div>
                    <div class="news-nav__btn-text">
                        @lang('cms-articles::site.Назад к статьям')
                    </div>
                </a>
                @if ($next = $article->getNext())
                        <a href="{{ $next->getFrontUrl() }}" class="news-nav__button news-nav__button--right">
                            <div class="news-nav__btn-text">
                                @lang('cms-articles::site.Следующая статья')
                            </div>
                            <div class="news-nav__btn-icon">
                                @svg('arrow-right-thick', 12, 24, '')
                            </div>
                        </a>
                @else
                    <span></span>
                @endif
            </div>
        </div>
    </div>
    <div class="section section--might-be-interesting">
        <div class="container">
            @if($articles->isNotEmpty())
                <div class="title title--size-h2 _text-center _mb-hg">@lang('cms-articles::site.Other articles')</div>
                <div class="blog-slider js-import" data-slick-carousel='{!! r2d2()->attrJsonEncode([
                        'factory' => 'BlogCardCarousel'
                        ])
                    !!}'>
                    <div class="blog-slider__list" data-slick-carousel-list>
                        @foreach($articles ?? [] as $article)
                            @widget('articles:card', ['article' => $article, 'classes' => 'blog-card--theme-slider'])
                        @endforeach
                    </div>
                    <div class="blog-slider__arrows">
                        <div class="carousel-arrow" data-slick-carousel-prev-arrow>
                            @svg('arrow-left', 22, 23, '')
                        </div>
                        <div class="carousel-arrow" data-slick-carousel-next-arrow>
                            @svg('arrow-right', 22, 23, '')
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @widget('branches:list')
@endsection

