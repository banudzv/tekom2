<?php

namespace WezomCms\PrivacyPolicy\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;

class PrivacyPolicyController extends SiteController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $settings = settings('privacy-policy.site', []);

        $name = array_get($settings, 'name');

        // Seo
        $this->seo()
            ->setPageName($name)
            ->setTitle(array_get($settings, 'title'))
            ->setDescription(array_get($settings, 'description'))
            ->setH1(array_get($settings, 'h1'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'));

        // Breadcrumbs
        $this->addBreadcrumb($name, route('privacy-policy'));

        return view('cms-privacy-policy::site.index', ['settings' => $settings]);
    }
}
