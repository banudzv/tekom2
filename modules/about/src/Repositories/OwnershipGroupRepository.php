<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Ownership\Group;
use WezomCms\Core\Repositories\AbstractRepository;

class OwnershipGroupRepository extends AbstractRepository
{
    protected function model()
    {
        return Group::class;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->with(['contracts'])
            ->orderBy('sort')
            ->get();
    }
}
