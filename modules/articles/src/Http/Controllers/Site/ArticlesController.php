<?php

namespace WezomCms\Articles\Http\Controllers\Site;

use Illuminate\Pagination\Paginator;
use WezomCms\Articles\Models\Article;
use WezomCms\Articles\Models\ArticleTag;
use WezomCms\Articles\Repositories\ArticleGroupRepository;
use WezomCms\Articles\Repositories\ArticleRepository;
use WezomCms\Articles\Repositories\TagRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class ArticlesController extends SiteController
{
    use ImageFromSettings;

    /**
     * @var ArticleRepository
     */
    private $articleRepository;
    /**
     * @var ArticleGroupRepository
     */
    private $articleGroupRepository;
    /**
     * @var TagRepository
     */
    private $tagRepository;

    private $limit;
    private $settings;

    public function __construct(
        ArticleRepository $articleRepository,
        ArticleGroupRepository $articleGroupRepository,
        TagRepository $tagRepository
    )
    {
        $this->articleRepository = $articleRepository;
        $this->articleGroupRepository = $articleGroupRepository;
        $this->tagRepository = $tagRepository;

        $this->settings = settings('articles.site', []);
        $this->limit = array_get($this->settings, 'limit', Article::DEFAULT_LIMIT);
    }

    public function index()
    {
        $pageName = array_get($this->settings, 'name');

        $models = $this->articleRepository->getAllByFront($this->limit, [
            'translations', 'tags'
        ]);

        $groups = $this->articleGroupRepository->getByFront();

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('articles'));

        // SEO
        $this->seo()
            ->setTitle(array_get($this->settings, 'title'))
            ->setPageName($pageName)
            ->setH1(array_get($this->settings, 'h1'))
            ->setDescription(array_get($this->settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($this->settings, 'keywords'));

        return view('cms-articles::site.articles.index', [
            'articles' => $models,
            'groups' => $groups,
            'banner' => $this->getBannerUrl('articles.page'),
            'subTitle' => array_get($this->settings, 'sub_title')
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag($slug)
    {
        /** @var ArticleTag $tag */
        $tag = $this->tagRepository->getBySlugForFront($slug,[
            'translations'
        ]);

        /** @var Paginator $articles */
        $articles = $tag->articles()
            ->published()
            ->with('tags')
            ->orderByDesc('published_at')
            ->latest('id')
            ->paginate($this->limit);

//        $this->setLangSwitchers($tag, 'article-tag', ['slug' => 'slug'], false);

        $groups = $this->articleGroupRepository->getByFront();
        $pageName = array_get($this->settings, 'name');

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('articles'));
        $this->addBreadcrumb($tag->name, $tag->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($tag->title ?: array_get($this->settings, 'title'))
            ->setPageName($tag->name ?: $pageName)
            ->setH1($tag->h1 ?: array_get($this->settings, 'h1'))
            ->setDescription($tag->description ?: array_get($this->settings, 'description'))
            ->metatags()
            ->setKeywords($tag->keywords ?: array_get($this->settings, 'keywords'));

        // Render
        return view('cms-articles::site.articles.index', [
            'articles' => $articles,
            'groups' => $groups,
            'banner' => $this->getBannerUrl('articles.page'),
            'subTitle' => array_get($this->settings, 'sub_title')
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inner($slug)
    {
        /** @var Article $article */
        $article = $this->articleRepository->getBySlugForFront($slug,[
            'translations', 'group', 'tags'
        ]);

        $this->setLangSwitchers($article, 'articles.inner');
        $pageName = array_get($this->settings, 'name');

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('articles'));
        $this->addBreadcrumb($article->group->name, $article->group->getFrontUrl());
        $this->addBreadcrumb($article->name, $article->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($article->title)
            ->setH1($article->h1)
            ->setPageName($article->name)
            ->setDescription($article->description)
            ->metatags()
            ->setKeywords($article->keywords);

        $articlesListLink = $article->group->getFrontUrl();

        // Render
        return view('cms-articles::site.articles.inner', [
            'article' => $article,
            'articlesListLink' => $articlesListLink,
            'articles' => $article->otherArticles()
        ]);
    }
}
