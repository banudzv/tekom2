@extends('cms-ui::layouts.main', ['lightHeader' => true])

@php
    /**
     * @var $obj \WezomCms\Pages\Models\Page
     */
@endphp

@section('content')
    <div class="section section--bg-grey section--text-page">
        <div class="container container--theme-blog-inner">
            <h1>{{ SEO::getH1() }}</h1>
            <div class="wysiwyg wysiwyg--theme-default js-import" data-wrap-media data-draggable-table>
                {!! $obj->text !!}
            </div>
        </div>
    </div>
@endsection
