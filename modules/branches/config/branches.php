<?php

use WezomCms\Branches\Dashboards;
use WezomCms\Branches\Widgets;

return [
    'widgets' => [
        'branches:list' => Widgets\Branches::class,
        'branches:phones' => Widgets\Phones::class,
        'branch:main-contact' => Widgets\MainBranchContact::class,
    ],
    'dashboards' => [
        Dashboards\BranchesDashboard::class,
    ],
];
