<?php

namespace WezomCms\Orders\Widgets\Cart;

use WezomCms\Orders\Models\Buck;
use Illuminate\Support\Facades\Auth;
use WezomCms\Orders\Contracts\CartInterface;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class HeaderButton extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string
     */
    protected $view = 'cms-orders::site.widgets.cart.header-button';

    /**
     * @param  CartInterface  $cart
     * @return array|null
     */
    public function execute(Buck $cart): ?array
    {
        if(Auth::check()){
           return ['count' => $cart->where('user_id', Auth::id())->count()]; 
        }
        return null;
    }
    // /**
    //  * @param  CartInterface  $cart
    //  * @return array|null
    //  */
    // public function execute(CartInterface $cart): ?array
    // {
    //     return ['count' => $cart->count()];
    // }
}
