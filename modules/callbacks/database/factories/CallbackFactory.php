<?php

use Faker\Generator as Faker;
use WezomCms\Callbacks\Models\Callback;

$factory->define(Callback::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'read' => $faker->boolean(80),
    ];
});
