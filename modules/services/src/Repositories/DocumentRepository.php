<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\Document;

class DocumentRepository extends AbstractRepository
{
    protected function model()
    {
        return Document::class;
    }
}

