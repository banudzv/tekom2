<?php

namespace WezomCms\Home\Http\Controllers\Site;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use WezomCms\Articles\Repositories\ArticleRepository;
use WezomCms\Core\Facades\NotifyMessage;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Services\Repositories\ServiceRepository;

class SearchController extends SiteController
{
    /**
     * @var ServiceRepository
     */
    private $serviceRepository;
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    public function __construct(
        ServiceRepository $serviceRepository,
        ArticleRepository $articleRepository
    )
    {
        $this->serviceRepository = $serviceRepository;
        $this->articleRepository = $articleRepository;
    }

    public function search(Request $request)
    {
        $search = $request['search'];

        return JsResponse::make()
            ->addAction('redirect', [route('search-results', ['query' => $search])], [route('search-results', ['query' => $search])])
            ->reset()
            ;
    }

    public function result(Request $request)
    {
        $count = 0;
        $search = $request['query'];

        $services = $this->serviceRepository->search($search);
        $count += $services->count();

        $articles = $this->articleRepository->search($search);
        $count += resolve(ArticleRepository::class)->searchCount($search);

        // SEO
        $this->seo()
            ->metatags()
            ->setTitle('Search');
        SEOMeta::addMeta('robots', 'noindex,follow');

        return view('cms-home::site.search',[
            'search' => $search,
            'count' => $count,
            'services' => $services,
            'articles' => $articles
        ]);
    }
}
