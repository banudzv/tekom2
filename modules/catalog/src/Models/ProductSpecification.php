<?php

namespace WezomCms\Catalog\Models;

use Cache;

/**
 * Class ProductSpecification
 * @property int $id
 * @property int $product_id
 * @property int $spec_id
 * @property int $spec_value_id
 */
class ProductSpecification extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['spec_id', 'spec_value_id'];

    /**
     * Perform any actions required after the model boots.
     *
     * @return void
     */
    protected static function booted()
    {
        static::saved(function () {
            if (method_exists(Cache::getStore(), 'tags')) {
                Cache::tags(ProductSpecification::class)->flush();
            }
        });

        static::deleted(function () {
            if (method_exists(Cache::getStore(), 'tags')) {
                Cache::tags(ProductSpecification::class)->flush();
            }
        });
    }
}
