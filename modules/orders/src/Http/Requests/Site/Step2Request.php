<?php

namespace WezomCms\Orders\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use WezomCms\Orders\Enums\DeliveryTypes;

class Step2Request extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delivery' => ['required', 'string', Rule::in(DeliveryTypes::getValues())],
            'delivery_id' => [
                'required',
                'int',
                Rule::exists('deliveries', 'id')
                    ->where('published', true),
            ],
            'payment_id' => [
                'required',
                'int',
                Rule::exists('payments', 'id')
                    ->where('published', true),
            ],
            'comment' => 'nullable|string|max:65535',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'delivery' => __('cms-orders::site.checkout.Delivery type'),
            'delivery_id' => __('cms-orders::site.checkout.Delivery'),
            'payment_id' => __('cms-orders::site.checkout.Payment'),
            'comment' => __('cms-orders::site.checkout.Comment'),
        ];
    }
}
