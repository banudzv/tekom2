<div class="form-group">
    {!! Form::label('risks[]', __('cms-services::admin.Risks')) !!}
    <div class="input-group">
        {!! Form::select('risks[]', $risks, old('risks', $selectedRisks), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
