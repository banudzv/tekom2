<?php

namespace WezomCms\Branches\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\GetForSelectTrait;
use WezomCms\Core\Traits\Model\PublishedTrait;
use WezomCms\OurTeam\Models\Employee;
use WezomCms\Regions\Models\City;
use WezomCms\Services\Models\Service;

/**
 * \WezomCms\Branches\Models\Branch
 *
 * @property int $id
 * @property bool $published
 * @property bool $is_main
 * @property string|null $lat
 * @property string|null $lon
 * @property integer $city_id
 * @property integer $employee_id
 * @property integer $type
 * @property string|null $code
 * @property string|null $zip_code
 * @property array|null $phones
 * @property string|null $email
 * @property array|null $map
 * @property int $sort
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read BranchTranslation $translation
 * @property-read Collection|BranchTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static Builder|Branch filter($input = array(), $filter = null)
 * @method static Builder|Branch listsTranslations($translationField)
 * @method static Builder|Branch newModelQuery()
 * @method static Builder|Branch newQuery()
 * @method static Builder|Branch notTranslatedIn($locale = null)
 * @method static Builder|Branch orWhereTranslation($translationField, $value, $locale = null)
 * @method static Builder|Branch orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static Builder|Branch orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static Builder|Branch paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Branch published()
 * @method static Builder|Branch publishedWithSlug($slug, $slugField = 'slug')
 * @method static Builder|Branch query()
 * @method static Builder|Branch simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static Builder|Branch translated()
 * @method static Builder|Branch translatedIn($locale = null)
 * @method static Builder|Branch whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|Branch whereCreatedAt($value)
 * @method static Builder|Branch whereEmail($value)
 * @method static Builder|Branch whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|Branch whereId($value)
 * @method static Builder|Branch whereLike($column, $value, $boolean = 'and')
 * @method static Builder|Branch whereMap($value)
 * @method static Builder|Branch wherePhones($value)
 * @method static Builder|Branch wherePublished($value)
 * @method static Builder|Branch whereSort($value)
 * @method static Builder|Branch whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static Builder|Branch whereTranslationLike($translationField, $value, $locale = null)
 * @method static Builder|Branch whereUpdatedAt($value)
 * @method static Builder|Branch withTranslation()
 * @mixin \Eloquent
 * @mixin BranchTranslation
 */
class Branch extends Model
{
    use Translatable;
    use Filterable;
    use GetForSelectTrait;
    use PublishedTrait;

    const TYPE_OFFICE = 1;
    const TYPE_COK = 2;

    const LIMIT_MAIN = 5;

    /**
     * Selection options for GetForSelectTrait
     *
     * @var array
     */
    protected static $selectOptions = ['name_key' => 'address'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'address', 'time_working', 'date_create', 'services'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'published',
        'phones',
        'email',
        'map',
        'published',
        'sort',
        'is_main',
        'lat',
        'lon',
        'city_id',
        'code',
        'type',
        'employee_id',
        'zip_code'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool',
        'is_main' => 'bool',
        'phones' => 'array',
        'map' => 'array',
    ];

    public static function getTypeBySelect(): array
    {
        return [
            self::TYPE_OFFICE => __('cms-branches::admin.Type office'),
            self::TYPE_COK => __('cms-branches::admin.Type cok')
        ];
    }

    public function typeForFront()
    {
        if($this->is_main){
            return __('cms-branches::site.Central branch');
        }

        if($this->type == self::TYPE_OFFICE){
            return __('cms-branches::site.Type office');
        }

        if($this->type == self::TYPE_COK){
            return __('cms-branches::site.Type cop');
        }
    }

    public function city()
    {
        return $this->belongsTo(City::class,'city_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
   /* public function services()
    {
        return $this->belongsToMany(
            Service::class,
            'branch_services_relation',
            'branch_id', 'service_id'
        );
    }*/

    public function getFullAddressAttribute() {

        $address = '';
        if($this->zip_code){
            $address .= $this->zip_code . ', ';
        }

        $address .= __('cms-branches::site.word city') . ' ' . $this->city->name . ',';

        if($this->address){
            $address .= ' ' . $this->address;
        }

        return $address;
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
