<?php

namespace WezomCms\Services\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use WezomCms\Core\Contracts\Filter\FilterFieldInterface;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Http\Requests\Admin\RiskGroupRequest;
use WezomCms\Services\Http\Requests\Admin\ServiceGroupRequest;
use WezomCms\Services\ModelFilters\ServiceGroupFilter;
use WezomCms\Services\Models\BuyModule;
use WezomCms\Services\Models\RiskGroup;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\RiskRepository;

class RiskGroupController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = RiskGroup::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-services::admin.risk-groups';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.risk-groups';

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = RiskGroupRequest::class;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-services::admin.risk.Group name');
    }

    /**
     * @param Builder $query
     * @param Request $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}

