@extends('cms-users::site.layouts.cabinet')

@php
    /**
     * @var $user \WezomCms\Users\Models\User
     */
@endphp

@section('content')
    <div style="margin-top: 100px" class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            @widget('cabinet-menu')
        </div>
        <div class="grid">
            <div class="gcell">
                <table>
                    <tr>
                        <td>@lang('cms-orders::site.Имя')</td>
                        <td>{{ $user->full_name }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cms-orders::site.Электронная почта')</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>@lang('cms-orders::site.Номер телефона')</td>
                        <td>{{ $user->phone }}</td>
                    </tr>
                </table>
            </div>
            <div class="gcell">
                @widget('cabinet-socials')
                @widget('cabinet-submenu')
            </div>
        </div>
    </div>
@endsection
