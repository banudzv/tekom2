<?php

Route::namespace('WezomCms\\Pages\\Http\\Controllers\\Site')
    ->group(function () {
        Route::get('page/{slug}', 'PageController@index')->name('page.inner');
    });
