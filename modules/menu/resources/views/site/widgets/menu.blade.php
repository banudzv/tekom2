@php
    /**
     * @var $menu array|\WezomCms\Menu\Models\Menu[][]
     * @var $maxDepth int
     */
@endphp
<div class="aside" data-aside-sticky>
    <div class="aside-menu">
        @foreach($menu[null] as $item)
            <div class="aside-menu__item">
                @if($mode = $item->activeMode())
                    @if($mode === \WezomCms\Menu\Models\Menu::MODE_SPAN)
                        <span class="aside-menu__link is-active">{{ $item->name }}</span>
                    @else
                        <a href="{{ url($item->url) }}" class="aside-menu__link is-active">{{ $item->name }}</a>
                    @endif
                @else
                    <a href="{{ url($item->url) }}" class="aside-menu__link">{{ $item->name }}</a>
                @endif
            </div>
        @endforeach
    </div>
</div>
