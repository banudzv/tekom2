<?php
use WezomCms\Menu\Http\Controllers\Admin\MenuController;

Route::adminResource('menu', MenuController::class);
Route::get('menu/{group}/get-parents-list', [MenuController::class, 'getParentsList'])->name('menu.get-parents-list');
Route::get('menu/copy/{id}/{group}', [MenuController::class, 'copy'])->name('menu.copy');
