export default ($element) => {
	let height = 0;
	$element.children('*').each((index, el) => {
		let $el = $(el);
		height += $el.outerHeight() + parseFloat($el.css('margin-top')) + parseFloat($el.css('margin-bottom'));
	});
	return height;
};
