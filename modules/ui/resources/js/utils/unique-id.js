'use strict';

/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import { getRandomInt } from 'js#/utils/get-random-int';

// ----------------------------------------
// Inner data
// ----------------------------------------

/**
 * @type {string[]}
 * @private
 */
const _cache = [];

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {string} [prefix]
 * @return {string}
 */
export const uniqueId = (prefix = 'id-') => {
	const id = prefix + new Date().getTime() + '-' + getRandomInt(100, 999999);
	if (~_cache.indexOf(id)) {
		return uniqueId(prefix);
	} else {
		_cache.push(id);
		return id;
	}
};
