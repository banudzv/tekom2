@php
    /**
     * @var $models \Illuminate\Database\Eloquent\Collection|\WezomCms\Pages\Models\Page[]
     */
@endphp
<div class="reports-banner">
    <div class="reports-banner__content">
        <div class="reports-banner__title">@lang('cms-pages::site.report title')</div>
        <div class="reports-banner__description">@lang('cms-pages::site.report description')</div>
        <div class="reports-banner__links">
            @foreach($models ?? [] as $page)
                <a class="reports-banner__link" href="{{ $page->getFrontUrl() }}">{{ $page->name }}</a>
            @endforeach
        </div>
    </div>
    <div class="_flex _justify-center">
        @widget('ui:button', ['component' => 'a', 'text' => 'подробнее о финансовых показателях', 'classes' => '_text-center', 'attrs' => ['href' => '#'], 'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']])
    </div>
</div>
