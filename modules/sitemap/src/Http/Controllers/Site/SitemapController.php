<?php

namespace WezomCms\Sitemap\Http\Controllers\Site;

use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Http\Controllers\SiteController;

class SitemapController extends SiteController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        // Seo
        $title = __('cms-sitemap::site.Sitemap');
        $this->addBreadcrumb($title, route('sitemap'));
        $this->seo()
            ->setTitle($title)
            ->setDescription($title)
            ->setH1($title)
            ->metatags()
            ->setKeywords($title);

        $items = [];
        foreach (event('sitemap:site') as $eventData) {
            $items = array_merge($items, $eventData);
        }

        // Render links only with name
        $items = array_filter($items, function ($item) {
            return !empty($item['name']);
        });

        // Sort items by sort field
        usort($items, function ($a, $b) {
            $aSort = is_array($a)
                ? array_get($a, 'sort', 0)
                : (isset($a->sort) ? $a->sort : 0);

            $bSort = is_array($b)
                ? array_get($b, 'sort', 0)
                : (isset($b->sort) ? $b->sort : 0);

            return $aSort <=> $bSort;
        });

//        $items = Helpers::groupByParentId($items);


        return view('cms-sitemap::site.index', [
            'items' => $items,
            'hotline' => settings('contacts.page-settings.hotline')
        ]);
    }
}
