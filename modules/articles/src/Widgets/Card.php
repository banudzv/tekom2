<?php

namespace WezomCms\Articles\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class Card extends AbstractWidget
{
	/**
	 * View name.
	 *
	 * @var string|null
	 */
	protected $view = 'cms-articles::site.widgets.card';
}
