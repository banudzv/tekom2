<?php

use Faker\Generator as Faker;
use WezomCms\Catalog\Models\Brand;

$factory->define(Brand::class, function (Faker $faker) {
    $data = [
        'published' => $faker->boolean,
    ];

    return array_merge($data, array_fill_keys(array_keys(app('locales')), ['name' => $faker->realText(15)]));
});

$factory->state(Brand::class, 'published', function ($faker) {
    return [
        'published' => true
    ];
});
