<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @langTabs
                    <div class="form-group">
                        {!! Form::label($locale . '[name]', __('cms-articles::admin.Name')) !!}
                        {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name)) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label($locale . '[slug]', __('cms-core::admin.layout.Slug')) !!}
                        {!! Form::slugInput($locale . '[slug]', old($locale . '.slug', $obj->translateOrNew($locale)->slug), ['source' => 'input[name="' . $locale . '[name]"']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label($locale . '[text]', __('cms-articles::admin.Text')) !!}
                        {!! Form::textarea($locale . '[text]', old($locale . '.text', $obj->translateOrNew($locale)->text), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
                    </div>

                    @include('cms-core::admin.partials.form-meta-inputs', compact('obj', 'locale'))
                @endLangTabs
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-header">
                <h4>@lang('cms-core::admin.layout.Main data')</h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label(str_slug('published'), __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published', old('published', $obj->exists ? $obj->published : true))  !!}
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('for_main'), __('cms-articles::admin.For main')) !!}
                    {!! Form::status('for_main', old('for_main', $obj->exists ? $obj->for_main : true))  !!}
                </div>
                <div class="form-group">
                    {!! Form::label('published_at', __('cms-articles::admin.Published at')) !!}
                    {!! Form::text('published_at', old('published_at', $obj->published_at ? $obj->published_at->format('d.m.Y') : null), ['class' => 'js-datepicker', 'placeholder' => __('cms-news::admin.Published at')]) !!}
                </div>
                @if(config('cms.articles.articles.use_groups'))
                    <div class="form-group">
                        {!! Form::label('article_group_id', __('cms-articles::admin.Group')) !!}
                        {!! Form::select('article_group_id', $groups, null, ['class' => 'js-select2']) !!}
                    </div>
                @endif
                @if(config('cms.articles.articles.use_tags'))
                    <div class="form-group">
                        {!! Form::label('tags[]', __('cms-articles::admin.Tags')) !!}
                        <div class="input-group">
                            {!! Form::select('tags[]', $tags, old('tags', $selectedTags), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label('image', __('cms-articles::admin.Image')) !!}
                    {!! Form::imageUploader('image', $obj, route($routeName . '.delete-image', $obj->id)) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('preview_image', __('cms-articles::admin.Image preview')) !!}
                    {!! Form::imageUploader('preview_image', $obj, route($routeName . '.delete-image', ['id' => $obj->id, 'field' => 'preview_image'])) !!}
                </div>
            </div>
        </div>
    </div>
</div>
