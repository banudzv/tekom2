@extends('cms-ui::layouts.main')

@php
    /**
     * @var $text1 string
     * @var $text2 string
     * @var $text3 string
     * @var $deliveryVariants \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\DeliveryVariant[]
     * @var $paymentVariants \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\PaymentVariant[]
     */
@endphp

@section('content')
    <div style="margin-top: 100px" class="container">
        {{$text}}
        <br>
        {{$orderId}}
    </div>
@endsection
