/**
 * @fileOverview Описываем справочник доступных путей
 * который используеться при сборке файлов
 */

const fromCWD = require('from-cwd');

module.exports = {
	src: {
		js: fromCWD('./modules/ui/resources/js/'),
		sass: fromCWD('./modules/ui/resources/sass/')
	},
	svgSpriteMap: {
		folders: fromCWD('./modules/ui/resources/svg/*'),
		entry: fromCWD('./modules/ui/resources/svg/readme.js')
	},
	resolveModules: [fromCWD('./node_modules/'), fromCWD('./modules/ui/resources/sass/')],
	public: {
		base: '/',
		build: '/build/',
		dev: '/dev/',
		svg: '/svg/'
	},
	dist: {
		base: fromCWD('./public/'),
		dev: fromCWD('./public/dev'),
		svg: fromCWD('./public/svg'),
		build: fromCWD('./public/build/'),
		buildTemp: fromCWD('./public/.build-temp/')
	}
};
