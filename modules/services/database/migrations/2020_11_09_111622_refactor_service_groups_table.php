<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorServiceGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_groups', function (Blueprint $table) {
            $table->integer('sort')->default(0)->index()->change();
            $table->boolean('is_top')->default(false);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')
                ->references('id')
                ->on('service_groups')
                ->onDelete('cascade');
            $table->string('image')->nullable();
            $table->string('icon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_groups', function (Blueprint $table) {
            $table->dropForeign('service_groups_parent_id_foreign');
            $table->dropIndex('service_groups_sort_index');
            $table->integer('sort')->default(0)->change();
            $table->dropColumn('parent_id');
            $table->dropColumn('image');
            $table->dropColumn('is_top');
            $table->dropColumn('icon');
        });
    }
}
