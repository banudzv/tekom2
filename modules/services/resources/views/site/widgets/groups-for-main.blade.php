@php
    /**
    * @var $models \WezomCms\Services\Models\ServiceGroup[]
    */
	$sliderConfig = config('cms.ui.sliderConfigs.servicesSlider');
    // todo рекомендованный размер изображения - 237x237
@endphp

<div class="section section--services">
    <div class="container">
        <div class="_grid _justify-center _spacer _spacer--df">
            <div class="_cell _cell--12 _df:cell--5 _flex-order-1 _df:flex-order-0 _flex _justify-center">
                <div class="services" data-services-wrapper>
                    @foreach($models ?? [] as $group)
                        <div class="service" data-service="{{ $loop->index }}">
                            <div class="service__title">{{ $group->name }}</div>
                            <div class="service__icon">
                                <div class="service__path">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                        <path fill="none" d="M49,25c0,13.3-10.7,24-24,24S1,38.3,1,25S11.7,1,25,1 S49,11.7,49,25z"></path>
                                        <path transform="translate(1, 1)" class="js-progress" stroke-width="3" fill="none" d="M49,25c0,13.3-10.7,24-24,24S1,38.3,1,25S11.7,1,25,1 S49,11.7,49,25z" style="stroke: #67CBAD; opacity: 1; stroke-dashoffset: 153; stroke-dasharray: 151.896px, 161.896px;"></path>
                                    </svg>
                                </div>
                                <svg>
                                    @svg($group->icon)
                                </svg>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="_cell _cell--12 _xs:cell--10 _df:cell--7 _flex-order-0 _df:flex-order-1">
                <div class="services-slider js-import" data-slick-carousel='{!! json_encode($sliderConfig) !!}'>
                    <div class="services-slider__list" data-slick-carousel-list>
                        @foreach($models ?? [] as $group)
                            <div class="services-slider__slide">
                                <div class="services-slider__slide-content">
                                    @if($group->h1)
                                        <div class="services-slider__slide-title">{{ $group->h1 }}</div>
                                    @endif
                                    @if($group->sub_title)
                                        <div class="services-slider__slide-subtitle">{{ $group->sub_title }}</div>
                                    @endif
                                    @if($group->short_description)
                                        <div class="services-slider__slide-description">{{ $group->short_description }}</div>
                                    @endif
                                    <div class="_flex _justify-start">
                                        @widget('ui:button', [
                                            'component' => 'a',
                                            'classes' => '_text-center ',
                                            'attrs' => ['href' => $group->getFrontUrl()],
                                            'text' => __('cms-callbacks::site.Order service'),
                                            'modificators' => ['color-accent', 'size-def', 'uppercase', 'ls-def', 'bold']])
                                    </div>
                                </div>
                                <div class="services-slider__slide-image">
                                    <div class="services-slider__slide-image-decor"></div>
                                    <img src="{{ url('/images/empty.gif') }}" class="lozad js-import" data-src="{{ $group->getImage() }}" alt="">
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
