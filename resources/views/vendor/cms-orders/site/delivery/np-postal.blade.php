<div>
    <input type="text" name="city" disabled="disabled" required="required"
           data-np-input
           spellcheck="true"
           inputmode="text"
           data-route="{{ route('nova-poshta.get-cities') }}"
           data-empty="@lang('cms-orders::site.Ничего не найдено')"
           placeholder="@lang('cms-orders::site.Введите город')...">
    <ul hidden></ul>
</div>
<div>
    <select name="postal_office"
            required="required"
            disabled="disabled"
            data-ignore
            data-np-select
            data-route="{{ route('nova-poshta.get-city-warehouses') }}">
        <option disabled selected>@lang('cms-orders::site.Выберите номер отделения')</option>
    </select>
</div>
