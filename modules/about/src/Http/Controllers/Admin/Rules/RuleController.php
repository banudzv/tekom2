<?php

namespace WezomCms\About\Http\Controllers\Admin\Rules;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\Rules\GroupRequest;
use WezomCms\About\Http\Requests\Admin\Rules\RuleRequest;
use WezomCms\About\Models\Rules\Group;
use WezomCms\About\Models\Rules\Rule;
use WezomCms\About\Repositories\RuleGroupRepository;
use WezomCms\About\Repositories\RuleRepository;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class RuleController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Rule::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.rules.rule';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-rules';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = RuleRequest::class;

    /**
     * @var RuleGroupRepository
     */
    private $ruleGroupRepository;

    public function __construct(RuleGroupRepository $ruleGroupRepository)
    {
        parent::__construct();

        $this->ruleGroupRepository = $ruleGroupRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Rules');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param  Rule  $model
     * @param  array  $viewData
     * @return array
     */
    protected function formData($model, array $viewData): array
    {
        return [
            'groups' => $this->ruleGroupRepository->getBySelect(),
        ];
    }
}

