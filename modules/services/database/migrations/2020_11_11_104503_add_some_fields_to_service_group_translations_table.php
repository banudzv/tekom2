<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldsToServiceGroupTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_group_translations', function (Blueprint $table) {
            $table->string('sub_title')->nullable();
            $table->string('short_description', 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_group_translations', function (Blueprint $table) {
            $table->dropColumn('sub_title');
            $table->dropColumn('short_description');
        });
    }
}

