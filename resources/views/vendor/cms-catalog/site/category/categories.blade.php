@php
    /**
     * @var $result \Illuminate\Pagination\LengthAwarePaginator|\WezomCms\Catalog\Models\Category[]
     **/
@endphp

@extends('cms-ui::layouts.main')

@section('content')
    <div style="margin-top:100px" class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            @forelse($result as $category)
                <a href="{{ $category->getFrontUrl() }}">
                    <img style="width:200px" src="{{ $category->getImageUrl() }}" alt="{{ $category->name }}">
                    <span>{{ $category->name }}</span>
                </a>
            @empty
                @emptyResult
            @endforelse

            @if($result->hasPages())
                {!! $result->links() !!}
            @endif
        </div>
    </div>
    <hr>
    <h2>Віджет популярних:</h2>
    @widget('catalog:popular')
@endsection

@section('hideH1', true)
