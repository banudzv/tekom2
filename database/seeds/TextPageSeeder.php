<?php

use Faker\Generator as Faker;

class TextPageSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('pages')->truncate();
        \DB::table('page_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = $this->getData();

        try {
            \DB::transaction(function () use ($data) {

                foreach ($data as $item){
                    $model = new \WezomCms\Pages\Models\Page();
                    $model->for_main_report = $item['for_main_report'];

                    $model->save();
                    foreach ($item['translates'] as $lang => $trans){
                        $translations = new \WezomCms\Pages\Models\PageTranslation();
                        $translations->locale = $lang;
                        $translations->name = $trans['name'];
                        $translations->slug = $trans['slug'];
                        $translations->text = $trans['text'];
                        $translations->page_id = $model->id;
                        $translations->save();
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function getData()
    {
        return [
            [
                'for_main_report' => true,
                'translates' => [
                    'ru' => [
                        'name' => 'Финансовые отчеты',
                        'slug' => 'finansovye-otchety',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 200)),
                    ],
                    'uk' => [
                        'name' => 'Фінансові звіти',
                        'slug' => 'finansovi-zviti',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 200)),
                    ]
                ],
            ],
            [
                'for_main_report' => true,
                'translates' => [
                    'ru' => [
                        'name' => 'Сообщения о проведении общих собраний',
                        'slug' => 'soobscheniya-o-provedenii-obschih-sobraniy',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 200)),
                    ],
                    'uk' => [
                        'name' => 'Повідомлення про проведення загальних зборів',
                        'slug' => 'povidomlennya-pro-provedennya-zagalnih-zboriv',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 200)),
                    ]
                ],
            ],
            [
                'for_main_report' => true,
                'translates' => [
                    'ru' => [
                        'name' => 'Особенная информация',
                        'slug' => 'osobennaya-informatsiya',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 200)),
                    ],
                    'uk' => [
                        'name' => 'Особлива інформація',
                        'slug' => 'osobliva-informatsiya',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 200)),
                    ]
                ],
            ],
        ];
    }
}
