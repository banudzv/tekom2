## Installation

#### Use console
```
composer require wezom-cms/branches
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/branches": "^7.0"
}
```
#### Install dependencies:
```
composer update
```
#### Package discover
```
php artisan package:discover
```
#### Run migrations
```
php artisan migrate
```

## Publish
#### Views
```
php artisan vendor:publish --provider="WezomCms\Branches\BranchesServiceProvider" --tag="views"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\Branches\BranchesServiceProvider" --tag="lang"
```
#### Migrations
```
php artisan vendor:publish --provider="WezomCms\Branches\BranchesServiceProvider" --tag="migrations"
```
