<?php

use Faker\Generator as Faker;
use WezomCms\Pages\Models\Page;

$factory->define(Page::class, function (Faker $faker) {
    $name = $faker->realText(25);

    return [
        'published' => $faker->boolean(80),
        'name' => $name,
        'h1' => $name,
        'title' => $name,
        'text' => $faker->realText(500),
    ];
});
