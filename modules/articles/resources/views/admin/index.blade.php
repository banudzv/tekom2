@extends('cms-core::admin.crud.index')

@section('content')
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th width="1%">#</th>
                <th>@lang('cms-articles::admin.Name')</th>
                <th>@lang('cms-articles::admin.Group')</th>
                <th>@lang('cms-articles::admin.Text')</th>
                <th>@lang('cms-articles::admin.Published at')</th>
                <th>@lang('cms-core::admin.layout.Go to the website')</th>
                <th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
            </tr>
            </thead>
            <tbody class="js-sortable"
                   data-params="{{ json_encode(['model' => encrypt(\WezomCms\Articles\Models\Article::class), 'page' => $result->currentPage(), 'limit' => $result->perPage()]) }}">
            @foreach($result as $obj)
                <tr data-id="{{ $obj->id }}">
                    <td>@massCheck($obj)</td>
                    <td>@editResource($obj)</td>
                    <td>@editResource(['obj'=>$obj->group, 'text' => $obj->group->name, 'ability' => 'article-groups.edit', 'target' => '_blank'])</td>
                    <td>@editResource(['obj' => $obj, 'text' => str_limit(strip_tags(html_entity_decode($obj->text)), 50) ?: null])</td>
                    <td>{{ $obj->published_at }}</td>
                    <td>@gotosite($obj)</td>
                    <td>
                        <div class="btn-group list-control-buttons" role="group">
                            @smallStatus($obj)
                            @editResource($obj, false)
                            @deleteResource($obj)
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
