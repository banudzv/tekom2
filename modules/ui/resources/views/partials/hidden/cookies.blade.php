<div class="cookie is-hidden" data-cookie-info>
    <div class="container">
        <div class="_grid _spacer _spacer--sm _justify-end _pt-sm _items-center _def-flex-nowrap">
            <div class="_cell _cell--12 _flex-grow">
                <p class="cookie__content">
                    @lang('cms-ui::site.Этот сайт использует файлы cookie и похожие технологии, чтобы гарантировать максимальное удобство пользователям.')
                    @lang('cms-ui::site.При использовании данного сайта, вы подтверждаете свое согласие на использование файлов cookie в соответствии с настоящим уведомлением в отношении данного типа файлов')
                    @lang('cms-ui::site.Если вы не согласны с тем, чтобы мы использовали данный тип файлов, то вы должны соответствующим образом установить настройки вашего браузера или не использовать этот сайт.')
                </p>
            </div>
            <div class="_cell _cell--12 _flex _justify-center _df:justify-start _flex-noshrink">
                @widget('ui:button', ['component' => 'button', 'attrs' => ['type' => 'button', 'data-cookie-submit'], 'text' => __('cms-ui::site.Согласен'), 'modificators' => ['color-accent', 'size-md', 'uppercase', 'ls-def', 'bold']])
            </div>
        </div>
    </div>
</div>
