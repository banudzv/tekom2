<div class="card">
    <div class="card-body">
        @langTabs
            <div class="form-group">
                {!! Form::label($locale . '[name]', __('cms-orders::admin.statuses.Name')) !!}
                {!! Form::text($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name))  !!}
            </div>
        @endLangTabs
    </div>
</div>
