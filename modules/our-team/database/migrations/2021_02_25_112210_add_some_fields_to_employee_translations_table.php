<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldsToEmployeeTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_translations', function (Blueprint $table) {
            $table->string('education')->nullable();
            $table->string('work_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_translations', function (Blueprint $table) {
            $table->dropColumn('education');
            $table->dropColumn('work_description');
        });
    }
}

