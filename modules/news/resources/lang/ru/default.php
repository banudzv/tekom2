<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'News' => 'Новости',
        'Name' => 'Название',
        'Published at' => 'Опубликовано',
        'Image' => 'Изображение',
        'Text' => 'Текст',
        'Tags' => 'Теги',
        'News tags' => 'Новости: Теги',
        'News tags list' => 'Список тегов',
        'Create new tag' => 'Создать тег',
        'News no publish' => 'Не опубликованные новости',
    ],
    TranslationSide::SITE => [
        'News' => 'Новости',
    ]
];
