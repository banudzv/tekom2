import $ from 'jquery';
import Preloader from '../preloader';
import { reInitScripts } from 'js#/utils/re-init-scripts';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('loadMoreInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($element) {
	const loadingClass = 'is-loading';
	const {
		url,
		method = 'GET'
	} = $element.data('load-more');
	const $appendContainer = $element.find('[data-load-more-container]');
	const $trigger = $element.find('[data-load-more-trigger]');
	const preloader = new Preloader($appendContainer);
	const $countInput = $element.find('[data-current-count]');
	const modelId = $element.find('[data-service-group-id]').val();

	$trigger.on('click', () => {
		const currentCount = $countInput.val();

		preloader.show();
		$appendContainer.addClass(loadingClass);
		$trigger.addClass(loadingClass);

		$.ajax(url, {
			method: method,
			data: {
				count: currentCount,
				id: modelId
			}
		}).then((resp) => {
			preloader.hide();
			const {
				current_count,
				more,
				html
			} = resp;

			$appendContainer.append(html);
			$trigger.removeClass(loadingClass);
			reInitScripts($appendContainer);

			if (!more) {
				$trigger.off('click')
					.parent()
					.remove();
			}

			$countInput.val(current_count);
		}).catch((err) => {
			console.warn(err);
			$trigger.removeClass(loadingClass);
			preloader.hide();
		})
	});
}
