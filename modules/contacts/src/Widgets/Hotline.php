<?php

namespace WezomCms\Contacts\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Core\Models\Setting;

class Hotline extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
//    public static $models = [Setting::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        $siteSettings = settings('contacts.page-settings', []);
        $hotline = array_get($siteSettings, 'hotline');

        if (!$hotline) {
            return null;
        }

        return compact('hotline');
    }
}
