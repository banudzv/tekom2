<?php

namespace WezomCms\Callbacks\Dashboards;

use WezomCms\Callbacks\Models\Callback;
use WezomCms\Callbacks\Models\Consultation;
use WezomCms\Callbacks\Models\QualityControl;
use WezomCms\Callbacks\Models\Request;
use WezomCms\Core\Foundation\Dashboard\AbstractValueDashboard;

class RequestDashboard extends AbstractValueDashboard
{
    /**
     * @var null|int - cache time in minutes.
     */
    protected $cacheTime = 5;

    /**
     * @var null|string - permission for link
     */
    protected $ability = 'callback-requests.view';

    /**
     * @return int
     */
    public function value(): int
    {
        return Request::count();
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return __('cms-callbacks::admin.Request');
    }

    /**
     * @return string
     */
    public function icon(): string
    {
        return 'fa-phone';
    }

    /**
     * @return string
     */
    public function iconColorClass(): string
    {
        return 'color-success';
    }

    /**
     * @return null|string
     */
    public function url(): ?string
    {
        return route('admin.callback-requests.index');
    }
}


