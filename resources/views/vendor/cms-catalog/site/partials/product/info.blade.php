@php
    /**
     * @var $product \WezomCms\Catalog\Models\Product
     * @var $variations \Illuminate\Support\Collection|\WezomCms\Catalog\Models\Product[]
     */
@endphp
<div>
    <h1>{{ SEO::getH1() }}</h1>
    <div>@lang('cms-catalog::site.Код товара'): {{ $product->id }}</div>
    @if($product->availableForPurchase())
        <div>@lang('cms-catalog::site.Есть в наличии')!</div>
    @else
        <div>@lang('cms-catalog::site.Нет в наличии')!</div>
    @endif
    <div>
        <div>@money($product->cost, true)</div>
        @if($product->old_cost)
            <span>@money($product->old_cost)</span>
            <small>{{ money()->siteCurrencySymbol() }}</small>
        @endif
    </div>
    @widget('product-labels', compact('product'))
    <div>
        @if($variations->isNotEmpty())
            <div>@lang('cms-catalog::site.Цвет'):</div>
            @foreach($variations as $variation)
                @if($product->id === $variation->id)
                    <span title="{{ $variation->color->name }}" style="width: 20px; height:20px; display:block; background-color: {{ $variation->color->color }}"></span>
                @else
                    <a href="{{ $variation->getFrontUrl() }}" title="{{ $variation->color->name }}"
                       style="width: 20px; height:20px; display:block; background-color: {{ $variation->color->color }}"></a>
                @endif
            @endforeach
        @endif
<form action="{{route('cart.add')}}" method="POST">
            @csrf
        
        @if($product->availableForPurchase())
            <div>@lang('cms-catalog::site.Количество:')</div>
            <div>
                @svg('common', 17)
                <input type="number" name="count"
                       value="{{ $product->minCountForPurchase() }}"
                       min="{{ $product->minCountForPurchase() }}"
                       max="9999"
                       step="{{ $product->stepForPurchase() }}">
                @svg('common', 17)
                <input type="hidden" name="product_id" value="{{ $product->id }}">
            </div>
        @endif
    </div>
    <div>
        
        @widget('comparison:product-button', ['product' => $product])
        @if($product->availableForPurchase())
            <button class= "{{ $product->in_cart ? 'in-cart' : '' }}"
                    data-cart-button="{{ $product->id }}"
                    title="{{ $product->in_cart ? __('cms-catalog::site.Открыть корзину') : __('cms-catalog::site.Добавить в корзину') }}"
                    data-cart-target="{{ json_encode(['action' => 'add', 'id' => $product->id, 'count' => $product->minCountForPurchase()]) }}">
                {{ $product->in_cart ? __('cms-catalog::site.в корзине') : __('cms-catalog::site.купить') }}
            </button>
        @else
        
            <button disabled="disabled">@lang('cms-catalog::site.Нет в наличии')</button>
          </form>
        @endif
{{--        @widget('favorites:product-button', ['favorable' => $product])--}}

        @if($product->availableForPurchase())
            @widget('buy-one-click:button', compact('product'))
        @endif
    </div>
</div>



{{-- <pre> 
    // $r = '{"success":true,"reset":true,"reload":false,"cart":{"subtotal_price":"11 444.00","discount":"0.00","total_price":"11 444.00","item_count":3,"promo_code":false,"currency":"\u0433\u0440\u043d","checkout_path":"http:\/\/tekom2\/checkout","items":[{"row_id":"aa5136608d24425b8ba345ba10667fef","url":"http:\/\/tekom2\/catalog\/ayfon\/p2","src":"static\/images\/placeholders\/no-image.png","title":"\u0430\u0439\u0444\u043e\u043d","code":2,"price":"444","quantity":{"value":1,"unit":"pieces","min":1,"max":9999,"step":1},"discount":"0.00","total_price":"444.00","promo_code":false,"in_favorite":false},{"row_id":"1a8cc5fdb496d288d30aaeb889fe352c","url":"http:\/\/tekom2\/catalog\/sumsung\/p4","src":"static\/images\/placeholders\/no-image.png","title":"sumsung","code":4,"price":"2 000","quantity":{"value":5,"unit":"pieces","min":1,"max":9999,"step":1},"discount":"0.00","total_price":"10 000.00","promo_code":false,"in_favorite":false},{"row_id":"8666d9932f41e8ebda55cc4bac766c5d","url":"http:\/\/tekom2\/catalog\/mayka\/p1","src":"static\/images\/placeholders\/no-image.png","title":"\u043c\u0430\u0439\u043a\u0430","code":1,"price":"100","quantity":{"value":10,"unit":"pieces","min":1,"max":9999,"step":1},"discount":"0.00","total_price":"1 000.00","promo_code":false,"in_favorite":false}]},"actions":[]}';
    // print_r($r);
    // ?> 
  
</pre> --}}


{{-- <script>
    let btn = document.querySelector('.bbb');
    let url = 'http://tekom2/cart/add';

    let body = btn.dataset.cartTarget;

    function sendRequest(method, url, body){
            return fetch(url, {
                "method": method,
                "body": JSON.stringify(body)
            })
        }

    btn.addEventListener('click', () =>{
        sendRequest('POST', url, body);
    })

</script> --}}

