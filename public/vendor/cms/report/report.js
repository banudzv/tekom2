$(document).ready(function () {

	$selector = $('.js-datetime-picker');

	let options = {
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
		timePickerIncrement: 1,
		timePickerSeconds: false,
		locale: {
			format: 'HH:mm',
			"applyLabel": window.translations.daterangepicker.applyLabel,
			"cancelLabel": window.translations.daterangepicker.cancelLabel,
			"fromLabel": window.translations.daterangepicker.fromLabel,
			"toLabel": window.translations.daterangepicker.toLabel,
			"customRangeLabel": window.translations.daterangepicker.customRangeLabel,
		}
	};

	$selector.daterangepicker(options, function (start_date, end_date) {
		$(this.element).val(start_date.format('HH:mm'));
	}).on('show.daterangepicker', function (ev, picker) {
		picker.container.find(".calendar-table").hide();
	});
});
