<?php

namespace WezomCms\Services\Widgets;

use Illuminate\Support\Collection;
use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\ServiceGroupRepository;

class Groups extends AbstractWidget
{
    /**
     * A list of models that, when changed, will clear the cache of this widget.
     *
     * @var array
     */
    public static $models = [ServiceGroup::class];

    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        if (!config('cms.services.services.use_groups')) {
            return null;
        }

        $repository = resolve(ServiceGroupRepository::class);
        $result = $repository->getByFrontAsMenu();

        dd($result);

        /** @var Collection $result */
        $result = ServiceGroup::published()
            ->orderBy('sort')
            ->latest('id')
            ->get();

        if ($result->isEmpty()) {
            return null;
        }

        return compact('result');
    }
}
