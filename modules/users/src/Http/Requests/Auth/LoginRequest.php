<?php

namespace WezomCms\Users\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Users\Models\User;
use WezomCms\Users\Rules\EmailOrPhone;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            User::emailOrPhone($this->get('email')) => ['required', 'string', 'unique:users'],
            'password' => [
                'required',
                'string',
                'min:' . config('cms.users.users.password_min_length'),
                'max:255',
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            User::EMAIL => __('cms-users::site.cabinet.E-mail'),
            User::PHONE => __('cms-users::site.cabinet.Phone'),
            'password' => __('cms-users::site.cabinet.Password'),
        ];
    }
}
