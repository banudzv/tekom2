@php
    /**
     * @var $searchForm iterable
     */
@endphp
<div>
    @foreach ($searchForm as $group)
        @php
            $allDisabled = true;
            $hasDisabled = false;
            foreach ($group['options'] ?? [] as $option) {
                if ($option['disabled']) {
                    $hasDisabled = true;
                } else {
                    $allDisabled = false;
                }
            }
            @endphp
            <pre>
            @php
     
        @endphp
        </pre> 
        
        @if($group['type'] === \WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_HIDDEN)
            <input type="hidden" name="{{ $group['name'] }}" value="{{ $group['value'] }}">
        @else
            @switch($group['type'])
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_RADIO)
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_CHECKBOX)
                <div>
                    <div>{{ $group['title'] }}</div>
                    <div>
                        @foreach($group['options'] as $option)
                            <div {!! $option['disabled'] && ! $allDisabled ? 'hidden' : '' !!}>
                                    <a href="{{ $option['url'] }}">{{ $option['name'] }}</a>
                            </div>
                        @endforeach
                        @if(!$allDisabled && $hasDisabled)
                            <div>@lang('cms-catalog::site.Все')</div>
                        @endif
                    </div>
                </div>
                @break
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_NUMBER_RANGE)
                @if($group['from']['min'] && $group['to']['max'])
                    <div>{{ $group['title'] }}</div>
                    <div>
                        <div>@lang('cms-catalog::site.products.from')</div>
                        <input class="min" type="number"
                               name="{{ $group['from']['name'] }}"
                               value="{{ $group['from']['value'] ?? $group['from']['min'] ?? '' }}"
                               placeholder="{{ $group['from']['placeholder'] ?? '' }}"
                               min="{{ $group['from']['min'] }}"
                        />
                        <div>@lang('cms-catalog::site.products.to')</div>
                        <input class="max" type="number"
                               name="{{ $group['to']['name'] }}"
                               value="{{ $group['to']['value'] ?? $group['to']['max'] ?? '' }}"
                               placeholder="{{ $group['to']['placeholder'] ?? '' }}"
                               max="{{ $group['to']['max'] ?? '' }}"
                        />
                        <div>{{ $group['currency'] ?? '' }}</div>
                        <a class="btn" href="{{ $baseUrl }}">ok</a>
                    </div>
                    <label>
                        <input type="hidden"
                               data-min="{{ $group['from']['min'] ?? 0 }}"
                               data-max="{{ $group['to']['max'] ?? 0 }}"
                               data-from="{{ $group['from']['value'] ?? $group['from']['min'] ?? 0 }}"
                               data-to="{{ $group['to']['value'] ?? $group['to']['max'] ?? 0 }}"/>
                    </label>
                @endif
                @break
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_CHECKBOX_WITH_ICON)
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_RADIO_WITH_ICON)
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_SELECT)
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_SELECT_RANGE)
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_NUMBER)
                @case(\WezomCms\Catalog\Filter\Contracts\FilterFormBuilder::TYPE_DOUBLE)
                @break
            @endswitch
        @endif
    @endforeach
</div>
<form action="" method="GET">
<div> 
<div>@lang('cms-catalog::site.Сортировать по'):</div>
    <select class="js-import"
            name="{{ $sort->getUrlKey() }}"
            data-params="{{ $queryParams ?? false ? json_encode($queryParams) : null }}"
            data-select-sort>
        @foreach($sort->getAllSortVariants() as $key => $name)
            <option value="{{ str_replace('default', 'created-at', $key) }}" {{ $sort->isThisSort($key) ? 'selected' : '' }}>{{ $name }}</option>
        @endforeach
    </select>
    <button type="submit">sort</button>
</div>
</form>
  {{-- Формування посилань для фільтрів і сортування --}}
<script> 
    let min = document.querySelector('.min');
    let max = document.querySelector('.max');
    let btn = document.querySelector('.btn');
    let baseLink = location.href;
    let delStr;
    btn.addEventListener('click', () =>
    {
    if(baseLink.includes('filter')){   
        if(baseLink.includes(';cost_to=')){
            baseLink = location.href.replace(localStorage.getItem('delStr'), '');
        }
        if(location.search){
            baseLink = baseLink.replace(location.search, '');
            btn.href = baseLink + ';cost_from=' + min.value + ';cost_to=' + max.value + location.search;
        } else{
            btn.href = baseLink + ';cost_from=' + min.value + ';cost_to=' + max.value;
        }
        delStr = 'cost_from=' + min.value + ';cost_to=' + max.value;
        localStorage.setItem('delStr', delStr);
    } else {
        if(location.search){
        baseLink = btn.href;
        btn.href = baseLink + '/filter/cost_from=' + min.value + ';cost_to=' + max.value + location.search;
        } else{
            baseLink = btn.href;
            btn.href = baseLink + '/filter/cost_from=' + min.value + ';cost_to=' + max.value;
        }
        delStr =  'cost_from=' + min.value + ';cost_to=' + max.value;
        localStorage.setItem('delStr', delStr);
    }
    });
</script>