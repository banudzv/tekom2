@php

	/**
	 * @var array|null $selectOptions
	 * @var array|null $attrs
	 * @var string|null $name
	 * @var string|null $class
	 */

	$_selectOptions = array_merge([
		'Factory' => 'DefaultSelect2'
	], $selectOptions ?? []);

@endphp

<div class="form-item form-item--select2 {{ $class ?? null }}">
	<div
			class="js-import form-item__control form-item__control--select2"
			data-select2
			data-initialize-select2='{{ json_encode($_selectOptions) }}'
	>
		<select {!! Html::attributes($attrs) !!} id="{{ uniqid($name) }}-select">
			{!! $slot !!}
		</select>
	</div>
</div>
