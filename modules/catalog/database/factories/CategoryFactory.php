<?php

use Faker\Generator as Faker;
use WezomCms\Catalog\Models\Category;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->realText(15);

    $parent = Category::published()
        ->whereNull('parent_id')
        ->inRandomOrder()
        ->first();

    return [
        'published' => true,
        'parent_id' => $parent ? $parent->id : null,
        // Translation
        'name' => $name,
        'text' => $faker->text,
        'title' => $name,
        'h1' => $name,
    ];
});
