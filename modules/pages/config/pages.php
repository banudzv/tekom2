<?php

use WezomCms\Pages\Widgets;

return [
    'widgets' => [
        'pages:report-for-main' => Widgets\ReportForMain::class,
    ]
];

