<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'icon' => [
            'sprite' => 'иконки',
            'adminSprite' => 'adminSprite',
            'accident_insurance' => 'accident insurance',
            'agricultural_insurance' => 'agricultural insurance',
            'air_transport_insurance' => 'air transport insurance',
            'animal_insurance' => 'animal insurance',
            'avtograzhdanka' => 'avtograzhdanka',
            'car_insurance' => 'car insurance',
            'cargo_insurance' => 'cargo_insurance',
            'construction_and_installation_risks_insurance' => 'construction and installation risks insurance',
            'health_insurance' => 'health insurance',
            'insurance_of_vegetables_and_fruit' => 'insurance of vegetables and fruit',
            'liability_insurance' => 'liability insurance',
            'property_insurance' => 'property insurance',
            'railway_transport_insurance' => 'railway transport insurance',
            'travel_insurance' => 'travel insurance',
            'water_transport_insurance' => 'water transport insurance',
        ]
    ],
    TranslationSide::SITE => [
        'currency_symbol' => 'грн',
        'Page not found' => 'Сторінка не знайдена',
        'Page: :page' => 'Сторінка: :page',
        'The given data was invalid' => 'Введені не коректні дані',
        'Method not allowed' => 'Метод не дозволений',
        'Server error' => 'Вибачте, сталася помилка сервера',
        'CSRF token mismatch' => 'Ваша сесія застаріла. Будь ласка обновіть сторінку і повторно надішліть форму',
        'Unauthenticated' => 'не авторизований',
        'To many attempts Retry after :seconds seconds' => 'Занадто багато запитів. Повторіть після :seconds секунд',
        'To many attempts' => 'Занадто багато запитів. Повторіть пізніше',
        'For security reasons your request has been canceled Please try again later' => 'З метою безпеки ваш запит скасований. Будь ласка спробуйте пізніше.',
        'Phone is entered incorrectly' => 'Телефон введений невірно. Коректний формат: :format',
        'or' => 'або',
        'icon' => [
            'sprite' => 'иконки',
            'adminSprite' => 'adminSprite',
            'accident_insurance' => 'accident insurance',
            'agricultural_insurance' => 'agricultural insurance',
            'air_transport_insurance' => 'air transport insurance',
            'animal_insurance' => 'animal insurance',
            'avtograzhdanka' => 'avtograzhdanka',
            'car_insurance' => 'car insurance',
            'cargo_insurance' => 'cargo_insurance',
            'construction_and_installation_risks_insurance' => 'construction and installation risks insurance',
            'health_insurance' => 'health insurance',
            'insurance_of_vegetables_and_fruit' => 'insurance of vegetables and fruit',
            'liability_insurance' => 'liability insurance',
            'property_insurance' => 'property insurance',
            'railway_transport_insurance' => 'railway transport insurance',
            'travel_insurance' => 'travel insurance',
            'water_transport_insurance' => 'water transport insurance',
        ]
    ],
];
