<?php

namespace WezomCms\Slider\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Repositories\ServiceRepository;
use WezomCms\Slider\Http\Requests\Admin\SliderRequest;
use WezomCms\Slider\Models\Slide;
use WezomCms\Slider\Repositories\SlideLinkRepository;

class SlideController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Slide::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-slider::admin';

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.slides';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = SliderRequest::class;

    /**
     * @var ServiceRepository
     */
    private $serviceRepository;
    /**
     * @var SlideLinkRepository
     */
    private $slideLinkRepository;

    public function __construct(
        ServiceRepository $serviceRepository,
        SlideLinkRepository $slideLinkRepository
    )
    {
        parent::__construct();
        $this->serviceRepository = $serviceRepository;
        $this->slideLinkRepository = $slideLinkRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-slider::admin.Sliders');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param $result
     * @param  array  $viewData
     * @return array
     */
    protected function indexViewData($result, array $viewData): array
    {
        $result = Helpers::groupByParentId($result, 'slider');

        return ['sliders' => config('cms.slider.slider.sliders'), 'result' => $result];
    }

    /**
     * @param  Slide  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function formData($obj, array $viewData): array
    {
        $sliders = collect(config('cms.slider.slider.sliders'))
            ->map(function ($el) {
                return __($el['name']);
            })
            ->prepend(__('cms-core::admin.layout.Not set'), '');

        return compact('sliders');
    }

    /**
     * @param  Slide  $obj
     * @param  FormRequest  $request
     * @return array
     */
    protected function fill($obj, FormRequest $request): array
    {
        $data = parent::fill($obj, $request);
        if(isset($data['service_id']) && $data['service_id'] == 0){
            $data['service_id'] = null;
        }

        return $data;
    }

    /**
     * @param  Slide  $model
     * @param  Request  $request
     */
    protected function afterSuccessfulSave($model, Request $request)
    {
        $data = $request->get('services', []);

        if(in_array('0', $data)){
            $mid = array_flip($data);
            unset($mid[0]);
            $data = array_flip($mid);
        }

        $model->services()->sync($data);
        $model->links()->sync($request->get('links', []));

    }

    /**
     * @param  Slide $model
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {
        return [
            'services' => $this->serviceRepository->getBySelect(),
            'selectedService' => [],
            'selectedServices' => [],
            'links' => $this->slideLinkRepository->getBySelect(),
            'selectedLinks' => [],

        ];
    }

    /**
     * @param  Slide $model
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        return [
            'services' => $this->serviceRepository->getBySelect(),
            'selectedService' => [$model->service_id],
            'selectedServices' => $model->services()->pluck('service_id')->toArray(),
            'links' => $this->slideLinkRepository->getBySelect(),
            'selectedLinks' => $model->links()->pluck('slide_link_id')->toArray(),
        ];
    }
}
