<?php

namespace WezomCms\Orders\Contracts;

interface CheckoutStorageInterface
{
    /**
     * @param  array  $data
     * @return CheckoutStorageInterface
     */
    public function put(array $data): CheckoutStorageInterface;

    /**
     * @param $key
     * @param $value
     * @return CheckoutStorageInterface
     */
    public function set($key, $value): CheckoutStorageInterface;

    /**
     * @param  string  $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * Get a subset containing the provided keys with values from the storage.
     *
     * @param  array|mixed  $keys
     * @return array
     */
    public function only($keys): array;

    /**
     * @return array
     */
    public function getAll(): array;

    /**
     * @return void
     */
    public function clear();
}
