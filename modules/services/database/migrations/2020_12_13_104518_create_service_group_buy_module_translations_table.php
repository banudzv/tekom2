<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceGroupBuyModuleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_module_service_group_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('buy_module_id');
            $table->string('locale')->index();
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->text('text_by_phone')->nullable();
            $table->text('text_by_office')->nullable();

            $table->unique(['buy_module_id', 'locale'], 'unique-bmsgt_buy-module');
            $table->foreign('buy_module_id', 'fk-bmsgt_buy-module')
                ->references('id')
                ->on('buy_module_service_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_module_service_group_translations');
    }
}

