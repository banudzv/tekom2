<?php

use WezomCms\Slider\Widgets\Slider;

return [
    'images' => [
        'directory' => 'slides',
        'default' => 'big',
        'sizes' => [
            'big' => [
                'width' => 1920,
                'height' => 900,
                'mode' => 'resize',
            ],
        ],
    ],
    'sliders' => [
        'main' => [
            'name' => 'cms-slider::admin.Main slider',
            'size' => null,
        ],
    ],
    'widgets' => [
        'slider' => Slider::class,
    ],
];
