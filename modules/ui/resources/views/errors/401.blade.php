@extends('cms-ui::layouts.error')

@section('code', '401')

@section('message', __('cms-ui::site.no access to this page'))
