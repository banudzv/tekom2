<?php

namespace WezomCms\Articles\Repositories;

use WezomCms\Articles\Models\ArticleTag;
use WezomCms\Core\Repositories\AbstractRepository;

class TagRepository extends AbstractRepository
{
    protected function model()
    {
        return ArticleTag::class;
    }

    public function getBySelect()
    {
        return $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                }
            ])
            ->get()
            ->pluck('name', 'id')
            ->toArray();
    }
}
