<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\InsuranceRates\InsuranceRate;
use WezomCms\Core\Repositories\AbstractRepository;

class InsuranceRateRepository extends AbstractRepository
{
    protected function model()
    {
        return InsuranceRate::class;
    }

    public function getByFront()
    {
        return $this->getModelQuery()
            ->published()
            ->orderBy('sort')
            ->get();
    }

}
