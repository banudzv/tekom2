<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Pages' => 'Текстовые страницы',
        'Pages list' => 'Список страниц',
        'Name' => 'Название',
        'Text' => 'Текст',
        'For main report' => 'Выводить на главной в блоке "Отчетность"',
    ],
    TranslationSide::SITE => [
        'report title' => 'Отчетность',
        'report description' => 'Мы гордимся тем, что финансовая деятельность СК "Теком" являеться публичной и открытой. Предлагаем ознакомиться с финансовыми показателями и результатами СК "Теком"',
        'report button' => 'Подробнее о финансовых показателях'
    ],
];
