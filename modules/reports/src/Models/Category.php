<?php

namespace WezomCms\Reports\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Traits\Model\Filterable;
use WezomCms\Core\Traits\Model\PublishedTrait;
use WezomCms\Partners\Models\CategoryTranslation;
use WezomCms\Reports\Helpers\ReportQueryParams;
use WezomCms\Reports\Models\Report;

/**
 * \WezomCms\Reports\Models\Category
 *
 * @property int $id
 * @property bool $published
 * @property bool $for_main
 * @property int $sort
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\Service[] $services
 * @property-read int|null $services_count
 * @property-read \WezomCms\Services\Models\ServiceGroupTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\ServiceGroupTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceGroup withTranslation()
 * @mixin \Eloquent
 * @mixin CategoryTranslation
 */
class Category extends Model
{
    use Translatable;
    use PublishedTrait;
    use Filterable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'report_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'parent_id', 'sort', 'for_main'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = [
        'name',
        'slug',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published' => 'bool',
        'for_main' => 'bool',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * Perform any actions required after the model boots.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleting(function (Category $category) {
            $category->children()->update(['parent_id' => $category->parent_id]);
        });
    }

    public function isBase()
    {
        return $this->parent_id == null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function getFrontUrl()
    {
        $query = [];
        if($this->isBase()){
            $query[ReportQueryParams::CATEGORY] = $this->slug;
        } else {
            $query[ReportQueryParams::CATEGORY] = $this->parent->slug;
            $query[ReportQueryParams::SUB_CATEGORY] = $this->slug;
        }

        return route_localized('reports', $query);
    }

    /**
     * @return array
     */
    public function getAllRootsCategoriesId()
    {
        $result = [];

        $category = $this;

        while ($category->parent_id !== null) {
            $category = Category::published()->find($category->parent_id);
            if (!$category) {
                break;
            }

            $result[] = $category->id;
        }

        return $result;
    }

    /**
     * @param callable|null $callback
     * @return array
     */
    public static function getForSelect(callable $callback = null)
    {
        $query = static::select()
            ->orderBy('sort')
            ->latest('id');

        if (null !== $callback) {
            $callback($query);
        }

        $tree = Helpers::groupByParentId($query->get());

        return static::addTreeSpaces($tree);
    }

    /**
     * @param array $tree
     * @param null $id
     * @param array $result
     * @param string $space
     * @return array
     */
    private static function addTreeSpaces(array $tree, $id = null, array &$result = [], $space = '')
    {
        foreach ($tree[$id] ?? [] as $group) {
            if (isset($tree[$group->id])) {
                $result[$group->id] = ['disabled' => true, 'name' => $space . $group->name];
                static::addTreeSpaces($tree, $group->id, $result, $space . '&nbsp;&nbsp;&nbsp;&nbsp;');
            } else {
                $result[$group->id] = ['name' => $space . $group->name];
            }
        }

        return $result;
    }
}
