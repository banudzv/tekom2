<div class="form-group">
    {!! Form::label('service_id', __('cms-slider::admin.Main service')) !!}
    <div class="input-group">
        {!! Form::select('service_id', $services, old('service_id', $selectedService), ['class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('services[]', __('cms-slider::admin.Services additional')) !!}
    <div class="input-group">
        {!! Form::select('services[]', $services, old('services', $selectedServices), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('links[]', __('cms-slider::admin.Slide Links')) !!}
    <div class="input-group">
        {!! Form::select('links[]', $links, old('links', $selectedLinks), ['multiple' => 'multiple', 'class' => 'js-select2']) !!}
    </div>
</div>
