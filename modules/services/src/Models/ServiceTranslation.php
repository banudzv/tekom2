<?php

namespace WezomCms\Services\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\Traits\Model\MultiLanguageSluggableTrait;

/**
 * \WezomCms\Services\Models\ServiceTranslation
 *
 * @property int $id
 * @property int $service_id
 * @property string $name
 * @property string $slug
 * @property string|null $text
 * @property string|null $title
 * @property string|null $h1
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $text_for_document
 * @property string|null $text_for_relation
 * @property string|null $seo_text
 * @property string|null $sub_title
 * @property string|null $parental_case_name
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\ServiceTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class ServiceTranslation extends Model
{
    use MultiLanguageSluggableTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'text',
        'title',
        'h1',
        'keywords',
        'description',
        'text_for_document',
        'text_for_relation',
        'seo_text',
        'sub_title',
        'parental_case_name'
    ];
}
