<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorSlidesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slide_translations', function (Blueprint $table) {
            $table->dropColumn('published');
            $table->dropColumn('image');
            $table->text('text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slide_translations', function (Blueprint $table) {
            $table->boolean('published')->default(true);
            $table->string('image')->nullable();
            $table->dropColumn('text');
        });
    }
}
