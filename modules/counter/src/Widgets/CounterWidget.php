<?php

namespace WezomCms\Counter\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Counter\Models\Counter;
use WezomCms\Counter\Repositories\CounterRepository;
use WezomCms\Partners\Models\Category;

class CounterWidget extends AbstractWidget
{
    /**
     * View name.
     *
     * @var string|null
     */
    protected $view = 'cms-counter::site.widgets.counter';

	/**
	 * @return array|null
	 */
    public function execute(): ?array
	{
        /** @var $models Counter[] */
		$models = app(CounterRepository::class)->getByFront();

		if ($models->isEmpty()) {
			return null;
		}

		return compact('models');
	}
}
