<?php

use Faker\Generator as Faker;
use WezomCms\Catalog\Models\Brand;
use WezomCms\Catalog\Models\Category;
use WezomCms\Catalog\Models\Product;

$factory->define(Product::class, function (Faker $faker) {
    /** @var Brand $brand */
    $brand = Brand::inRandomOrder()->first();
    $model = $brand ? $brand->models()->inRandomOrder()->first() : null;

    $name = $faker->realText(50);

    $category = Category::doesntHave('children')->inRandomOrder()->first();

    $cost = $faker->numberBetween(0, 9999);
    $sale = $faker->boolean(80);

    return [
        'published' => true,
        'category_id' => $category ? $category->id : null,
        'cost' => $cost,
        'sale' => $sale,
        'old_cost' => $sale ? $faker->numberBetween($cost, 9999) : 0,
        'brand_id' => $brand ? $brand->id : null,
        'model_id' => $model ? $model->id : null,
        'novelty' => $faker->boolean(80),
        'popular' => $faker->boolean(80),
        'videos' => [$faker->url, $faker->url, $faker->url],
        // Translation
        'name' => $name,
        'text' => $faker->realText($faker->numberBetween(250, 500)),
        'title' => $name,
        'h1' => $name,
    ];
});
