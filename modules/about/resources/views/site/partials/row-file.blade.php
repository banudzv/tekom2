<div class="report report--theme-info">
    <div class="report__name">{{ $item->name }}</div>
    @if($item->fileExists())
        <a class="report__icon" href="{{ url($item->getFileUrl()) }}" download target="_blank" >
            @svg('pdf-file', 24, 30)
        </a>
    @endif
</div>
