import $ from 'jquery';
import throttle from 'lodash.throttle';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('fixedButtonInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($element) {
	$(window).on('scroll', throttle(() => {
		const $footer = $('.footer');
		const windowHeight = window.innerHeight;
		const offsetTop = $(document).scrollTop();
		const footerHeight = $footer.outerHeight();
		const footerOffset = $footer.offset().top;

		if (offsetTop + windowHeight > footerOffset) {
			$element.addClass('is-hidden');
		} else {
			$element.removeClass('is-hidden');
		}
	}, 100));
}
