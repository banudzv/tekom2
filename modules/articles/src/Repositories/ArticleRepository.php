<?php

namespace WezomCms\Articles\Repositories;

use WezomCms\Articles\Models\Article;
use WezomCms\Core\Repositories\AbstractRepository;

class ArticleRepository extends AbstractRepository
{
    protected function model()
    {
        return Article::class;
    }

    public function getAllByFront($limit = Article::DEFAULT_LIMIT, $relations = ['translations'])
    {
        return $this->getModelQuery()
            ->published()
            ->with($relations)
            ->latest('published_at')
            ->latest('id')
            ->paginate($limit);
    }

    public function getAllByGroup($groupId, array $relations = ['translations'], $withoutId = false)
    {
        return $this->getModelQuery()
            ->published()
            ->with($relations)
            ->where('article_group_id', $groupId)->get();

//        if($withoutId){
//            $models->where('id', '!=', $withoutId);
//        }
//
//        return $models
//            ->latest('published_at')
//            ->latest('id')
//            ->get();
    }

    public function searchCount($search)
    {
        return $this->getModelQuery()
        ->published()
        ->whereHas('translations', function ($query) use ($search) {
            $query->where('name', 'like', '%'.$search.'%');
        })
        ->count();
    }


    public function search($search)
    {
        $data = [];
        $articles = $this->getModelQuery()
            ->published()
            ->with('group')
            ->whereHas('translations', function ($query) use ($search) {
                $query->where('name', 'like', '%'.$search.'%');
            })
            ->get();

        foreach ($articles as $key => $article){
            $data[$article->group->search_title ?? $article->group->name][$key] = $article;
        }

        return $data;
    }
}

