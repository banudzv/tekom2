<?php

use Faker\Generator as Faker;
use WezomCms\News\Models\NewsTag;

$factory->define(NewsTag::class, function (Faker $faker) {
    $name = $faker->word;

    return [
        'published' => $faker->boolean(80),
        'name' => $name,
        'title' => $name,
        'h1' => $name,
    ];
});
