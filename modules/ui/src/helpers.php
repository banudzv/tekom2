<?php

use WezomCms\Core\Contracts\Assets\AssetManagerInterface;

if (!function_exists('svg')) {
    /**
     * Render svg element.
     *
     * @param  string  $id
     * @param  int|null  $width
     * @param  int|null  $height
     * @param  string|null  $class
     * @return string
     */
    function svg(string $id, ?int $width = null, ?int $height = null, ?string $class = null, string $customSprite = 'sprite')
    {
//        $sprite = asset(config('cms.ui.ui.svg.' . $customSprite));
        $sprite = asset(config('cms.ui.ui.all_icons'));

        if ($width && !$height) {
            $height = $width;
        }
        return sprintf(
            '<svg%s><use xlink:href="%s"></use></svg>',
            Html::attributes(array_filter(compact('width', 'height', 'class'))),
            "{$sprite}#{$id}"
        );
    }
}

if (!function_exists('svgAll')) {
    function svgAll(string $id)
    {
        return \WezomCms\Core\Foundation\Icon::renderForAdminAllIcon($id);
    }
}

if (!function_exists('browserizr')) {
    /**
     * Get Browserizr instance
     *
     * @return \WezomAgency\Browserizr
     */
    function browserizr()
    {
        return \WezomAgency\Browserizr::detect();
    }
}

if (!function_exists('r2d2')) {
    /**
     * Get R2D2 instance
     * @return \WezomAgency\R2D2
     */
    function r2d2()
    {
        return \WezomAgency\R2D2::eject();
    }
}

if (!function_exists('compiled_assets')) {
	/**
	 * Replaces paths compiled assets from generated manifests
	 *
	 * @param  string  $path
	 * @return string
	 */
	function compiled_assets(string $path)
	{
		static $manifest;

		if (null === $manifest) {
			$source = env('UI_USE_DEV_ASSETS')
				? '/dev/manifest.json'
				: (browserSupportCheck()
					? '/build/manifest.production.json'
					: '/build/manifest.legacy.json');
			$json = file_get_contents(public_path($source));
			$manifest = json_decode($json, true);
		}

		return $manifest[$path];
	}
}

if (!function_exists('compiled_svg')) {
	/**
	 * Replaces paths compiled SVG sets from generated manifests
	 *
	 * @param  string  $path
	 * @param  string  $symbol
	 * @return string
	 */
	function compiled_svg(string $path, string $symbol)
	{
		static $manifest;

		if (null === $manifest) {
			$json = file_get_contents(public_path('/svg/manifest.json'));
			$manifest = json_decode($json, true);
		}

		return $manifest[$path] . '#' . $symbol;
	}
}

if (!function_exists('browserSupportCheck')) {
	/**
	 * @return string
	 */
	function browserSupportCheck()
	{
		$browserVersions = config('cms.ui.ui.support_browser_versions');
		return true;
		//(!empty($browserVersions[Browser::browserFamily()]) && ($browserVersions[Browser::browserFamily()
		//] < Browser::browserVersionMajor()));
	}
}
