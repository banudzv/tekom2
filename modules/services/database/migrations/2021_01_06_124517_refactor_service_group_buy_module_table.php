<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorServiceGroupBuyModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buy_module_service_groups', function (Blueprint $table) {
            $table->string('icon_by_phone')->nullable();
            $table->string('icon_by_office')->nullable();
            $table->string('icon_by_online')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('buy_module_service_groups', function (Blueprint $table) {
            $table->dropColumn('icon_by_phone');
            $table->dropColumn('icon_by_office');
            $table->dropColumn('icon_by_online');
        });
    }
}

