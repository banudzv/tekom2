<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Agent::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\AgentTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'text' => $faker->address
    ];
});
