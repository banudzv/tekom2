@extends('cms-ui::layouts.main')

@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            <div>@lang('cms-orders::site.У нас есть много отличных товаров, которые ждут вас. Начните совершать покупки и ищите кнопку Купить')</div>
            <div>@lang('cms-orders::site.Вы можете добавить несколько товаров в свою корзину и заплатить за все товары одновременно')</div>
            <div>
                <a href="{{ route('home') }}">@lang('cms-orders::site.Вернуться на главную')</a>
            </div>
        </div>
    </div>
@endsection
