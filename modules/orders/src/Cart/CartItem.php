<?php

namespace WezomCms\Orders\Cart;

use Illuminate\Support\Collection;
use WezomCms\Orders\Contracts\CartInterface;
use WezomCms\Orders\Contracts\CartItemConditionInterface;
use WezomCms\Orders\Contracts\CartItemInterface;

class CartItem implements CartItemInterface
{
    /**
     * @var string
     */
    private $uniqueId;
    /**
     * @var int
     */
    private $id;
    /**
     * @var float
     */
    private $price;
    /**
     * @var float
     */
    private $quantity;
    /**
     * @var Collection
     */
    private $options;
    /**
     * @var CartInterface
     */
    private $cart;

    /**
     * CartItemInterface constructor.
     * @param  CartInterface  $cart
     * @param  int  $id
     * @param  float  $price
     * @param  float  $quantity
     * @param  array  $options
     */
    public function __construct(
        CartInterface $cart,
        int $id,
        float $price,
        float $quantity,
        array $options = []
    ) {
        $this->id = $id;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->options = collect($options);

        $this->uniqueId = $this->generateUniqueId();

        $this->cart = $cart;
    }

    /**
     * @param  CartInterface  $cart
     * @return CartItemInterface
     */
    public function setCart(CartInterface $cart): CartItemInterface
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * @return string
     */
    public function getUniqueId(): string
    {
        return $this->uniqueId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param  bool  $format
     * @return float|string
     */
    public function getPrice(bool $format = true)
    {
        $value = $this->round($this->price);

        return $format ? $this->formatCost($value) : $value;
    }

    /**
     * Get sub total price. Without applied discount, promo etc.
     *
     * @param  bool  $format
     * @return float|string
     */
    public function getSubTotal(bool $format = true)
    {
        $value = $this->round($this->getQuantity() * $this->getPrice(false));

        return $format ? $this->formatCost($value) : $value;
    }

    /**
     * @param  bool  $format
     * @return float|string
     */
    public function getTotal(bool $format = true)
    {
        $price = $this->getSubTotal(false);

        // If enabled cart conditions
        if (method_exists($this->cart, 'getItemConditions')) {
            foreach ($this->cart->getItemConditions() as $condition) {
                /** @var CartItemConditionInterface $condition */
                if ($condition->isApplicable($this)) {
                    $price = $condition->apply($this, $price);
                }
            }
        }

        return $format ? $this->formatCost($price) : $price;
    }

    /**
     * Get total discounted price.
     *
     * @param  bool  $format
     * @return float|string
     */
    public function totalDiscounted(bool $format = true)
    {
        $value = $this->round($this->getSubTotal(false) - $this->getTotal(false));

        return $format ? $this->formatCost($value) : $value;
    }

    /**
     * @return Collection
     */
    public function getAppliedConditions(): Collection
    {
        if (method_exists($this->cart, 'getItemConditions')) {
            return $this->cart->getItemConditions()
                ->filter(function (CartItemConditionInterface $condition) {
                    return $condition->isApplicable($this);
                });
        } else {
            return collect();
        }
    }

    /**
     * @param  string  $conditionName
     * @return CartItemConditionInterface|null
     */
    public function getAppliedCondition(string $conditionName): ?CartItemConditionInterface
    {
        $appliedConditions = $this->getAppliedConditions();

        $index = $appliedConditions->search(function (CartItemConditionInterface $condition) use ($conditionName) {
            return $condition instanceof $conditionName;
        });

        return $appliedConditions->get($index);
    }

    /**
     * @param  string  $conditionName
     * @return bool
     */
    public function isAppliedCondition(string $conditionName): bool
    {
        return $this->getAppliedConditions()
            ->filter(function (CartItemConditionInterface $condition) use ($conditionName) {
                return $condition instanceof $conditionName;
            })
            ->isNotEmpty();
    }

    /**
     * @param  float  $quantity
     * @return CartItemInterface
     */
    public function setQuantity(float $quantity): CartItemInterface
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return round($this->quantity, $this->cart->getQuantityPrecision());
    }

    /**
     * @return iterable
     */
    public function getOptions(): iterable
    {
        return $this->options;
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'unique_id' => $this->getUniqueId(),
            'id' => $this->getId(),
            'price' => $this->getPrice(),
            'quantity' => $this->getQuantity(),
            'options' => $this->getOptions(),
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return array_filter(array_keys(get_object_vars($this)), function ($key) {
            return $key !== 'cart';
        });
    }

    /**
     * @return string
     */
    protected function generateUniqueId()
    {
        return md5($this->id . serialize($this->options->sortKeys()));
    }

    /**
     * @param $value
     * @return float
     */
    protected function round($value): float
    {
        return round($value, $this->cart->getPrecision());
    }

    /**
     * @param  float  $value
     * @return string
     */
    protected function formatCost(float $value): string
    {
        return number_format($value, $this->cart->getPrecision(), '.', ' ');
    }
}
