<?php

use WezomCms\Counter\Models\Counter;
use WezomCms\Counter\Models\CounterTranslation;

class CounterSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('counters')->truncate();
        \DB::table('counter_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                for($i = 1; $i != 5; $i++){

                    factory(Counter::class)->create([
                        'sort' => $i
                    ]);

                    factory(CounterTranslation::class)->create(['locale' => 'ru', 'counter_id' => $i]);
                    factory(CounterTranslation::class)->create(['locale' => 'uk', 'counter_id' => $i]);
                }

            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}


