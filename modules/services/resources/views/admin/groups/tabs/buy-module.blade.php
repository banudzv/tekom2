<div class="form-group">
    {!! Form::label('buy_module[icon_by_office]', __('cms-services::admin.buy module.icon by office')) !!}
    <div class="input-group">
        {!! Form::select('buy_module[icon_by_office]', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon_by_office', $obj->buyModule->icon_by_office ?? null), ['class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('buy_module[icon_by_phone]', __('cms-services::admin.buy module.icon by phone')) !!}
    <div class="input-group">
        {!! Form::select('buy_module[icon_by_phone]', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon_by_phone', $obj->buyModule->icon_by_phone ?? null), ['class' => 'js-select2']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('buy_module[icon_by_online]', __('cms-services::admin.buy module.icon by online')) !!}
    <div class="input-group">
        {!! Form::select('buy_module[icon_by_online]', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon_by_online', $obj->buyModule->icon_by_online ?? null), ['class' => 'js-select2']) !!}
    </div>
</div>
@langTabs
<div class="form-group">
    {!! Form::label('buy_module[' . $locale . '][title]', __('cms-services::admin.buy module.title')) !!}
    {!! Form::text('buy_module[' .  $locale . '][title]',
            old('buy_module' . $locale . '.title', $obj->buyModule ? $obj->buyModule->translateOrNew($locale)->title : null))
    !!}
</div>
<div class="form-group">
    {!! Form::label('buy_module[' . $locale . '][sub_title]', __('cms-services::admin.buy module.sub_title')) !!}
    {!! Form::text('buy_module[' .  $locale . '][sub_title]',
            old('buy_module' . $locale . '.sub_title', $obj->buyModule ? $obj->buyModule->translateOrNew($locale)->sub_title : null))
    !!}
</div>
<div class="form-group">
    {!! Form::label('buy_module[' . $locale . '][text_by_phone]', __('cms-services::admin.buy module.text by phone')) !!}
    {!! Form::textarea('buy_module[' .  $locale . '][text_by_phone]',
            old('buy_module' . $locale . '.text_by_phone', $obj->buyModule ? $obj->buyModule->translateOrNew($locale)->text_by_phone : null))
    !!}
</div>
<div class="form-group">
    {!! Form::label('buy_module[' . $locale . '][text_by_office]', __('cms-services::admin.buy module.text by office')) !!}
    {!! Form::textarea('buy_module[' .  $locale . '][text_by_office]',
            old('buy_module' . $locale . '.text_by_office', $obj->buyModule ? $obj->buyModule->translateOrNew($locale)->text_by_office : null))
    !!}
</div>
<div class="form-group">
    {!! Form::label('buy_module[' . $locale . '][text_by_online]', __('cms-services::admin.buy module.text by online')) !!}
    {!! Form::textarea('buy_module[' .  $locale . '][text_by_online]',
            old('buy_module' . $locale . '.text_by_online', $obj->buyModule ? $obj->buyModule->translateOrNew($locale)->text_by_online : null))
    !!}
</div>
@endLangTabs
