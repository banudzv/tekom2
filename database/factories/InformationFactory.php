<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Information::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\InformationTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'text' => $faker->realText($faker->numberBetween(1200, 1300))
    ];
});
