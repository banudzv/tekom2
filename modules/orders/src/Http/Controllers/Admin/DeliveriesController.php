<?php

namespace WezomCms\Orders\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\Fields\Input;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\RenderSettings;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\Orders\Http\Requests\Admin\DeliveryRequest;
use WezomCms\Orders\Models\Delivery;
use WezomCms\Orders\Models\Payment;

class DeliveriesController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Delivery::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-orders::admin.deliveries';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.deliveries';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = DeliveryRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-orders::admin.deliveries.Deliveries');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param  Delivery  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function formData($obj, array $viewData): array
    {
        return [
            'payments' => Payment::getForSelect(false),
            'selectedPayments' => $obj->payments()->pluck('id')->toArray(),
        ];
    }

    /**
     * @param  Delivery  $obj
     * @param  Request  $request
     */
    protected function afterSuccessfulSave($obj, Request $request)
    {
        $obj->payments()->sync($request->get('PAYMENTS', []));
    }

    /**
     * @param  Delivery  $obj
     * @param  FormRequest  $request
     * @return array
     */
    protected function fillUpdateData($obj, FormRequest $request): array
    {
        $data = array_except($this->fill($obj, $request), 'type');

        $obj->type = $obj->driver ? $obj->type : $request->get('type');

        return $data;
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        return [
            Input::make(RenderSettings::siteTab())
                ->setKey('nova_poshta_key')
                ->setName(__('cms-orders::admin.deliveries.Nova Poshta API key'))
                ->setRules('required')
                ->setSort(1),
        ];
    }
}
