<?php

use Faker\Generator as Faker;

$factory->define(WezomCms\Faq\Models\FaqQuestion::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\Faq\Models\FaqQuestionTranslation::class, function (Faker $faker) {
    return [
        'question' => $faker->realText($faker->numberBetween(10, 50)),
        'answer' => $faker->realText($faker->numberBetween(250, 300)),
    ];
});
