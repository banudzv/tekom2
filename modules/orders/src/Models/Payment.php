<?php

namespace WezomCms\Orders\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\GetForSelectTrait;
use WezomCms\Core\Traits\Model\PublishedTrait;
use WezomCms\Orders\Contracts\PaymentDriverInterface;

/**
 * \WezomCms\Orders\Models\Payment
 *
 * @property int $id
 * @property int $sort
 * @property bool $published
 * @property string $name
 * @property string|null $driver
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\Delivery[] $deliveries
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Orders\Models\PaymentTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|Payment listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Payment published()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment translated()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereDriver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment withTranslation()
 * @mixin \Eloquent
 * @mixin PaymentTranslation
 */
class Payment extends Model
{
    use Translatable;
    use GetForSelectTrait;
    use PublishedTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['published', 'sort'];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['name'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['published' => 'bool'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deliveries()
    {
        return $this->belongsToMany(Delivery::class);
    }

    /**
     * @param  Order  $order
     * @param  string  $namespace
     * @return PaymentDriverInterface|null
     */
    public function makeDriver(Order $order, $namespace = 'WezomCms\\Orders\\Drivers\\Payment')
    {
        if ($this->driver) {
            $fullClassName = rtrim($namespace, '\\') . '\\' . Str::studly($this->driver);

            if (class_exists($fullClassName)) {
                return new $fullClassName($order);
            }
        }

        return null;
    }
}
