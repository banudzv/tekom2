'use strict';

/**
 * @module
 */

/**
 * @param {number} min
 * @param {number} max
 * @param {boolean} [includeMaxValue=true]
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values}
 * @returns {number}
 * @private
 */
export function getRandomInt (min, max, includeMaxValue = true) {
	min = Math.ceil(min);
	max = Math.floor(max);
	let inclusive = includeMaxValue ? 1 : 0;
	return Math.floor(Math.random() * (max - min + inclusive)) + min;
}
