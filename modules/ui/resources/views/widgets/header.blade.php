@php
    /**
     * @var $lightHeader = boolean|null
     */

    $lightHeader = $lightHeader ?? false;
@endphp

<header class="header js-import _lg:show {{ $staticHeader ? 'header--static' : '' }} {{ $lightHeader ? 'header--light' : '' }}" data-sticky-header data-header>
    <div class="container">
        <div class="_grid _items-center _justify-between">
            <div class="_flex _items-center">
                @if(Request::is('/'))
                    <div class="logo logo--header">
                        <svg>
                            <use xlink:href="#logo-symbol"></use>
                        </svg>
                    </div>
                @else
                    <a class="logo logo--header" href="{{ route('home') }}">
                        <svg>
                            <use xlink:href="#logo-symbol"></use>
                        </svg>
                    </a>
                @endif
                <div class="burger" data-burger data-dropdown-selector="burger">
                    @svg('menu', null, null, 'burger__icon burger__icon--open')
                    @svg('close', null, null, 'burger__icon burger__icon--close')
                </div>
                @widget('services:groups-as-menu')
                @widget('menu', ['position' => 'header'], 'cms-menu::site.widgets.header')
                {{--@widget('favorites:header-button')--}}
                <svg class="search-icon js-search js-import" width="27" height="27" viewBox="0 0 27 27" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M10.2143 19.3825C15.1375 19.3825 19.1286 15.3914 19.1286 10.4682C19.1286 5.54494 15.1375 1.55388 10.2143 1.55388C5.29106 1.55388 1.3 5.54494 1.3 10.4682C1.3 15.3914 5.29106 19.3825 10.2143 19.3825ZM10.2143 20.6824C15.8555 20.6824 20.4286 16.1094 20.4286 10.4682C20.4286 4.82697 15.8555 0.253876 10.2143 0.253876C4.57309 0.253876 0 4.82697 0 10.4682C0 16.1094 4.57309 20.6824 10.2143 20.6824Z"
                    />
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M26 26.2538L17.55 17.8038L18.4693 16.8846L26.9193 25.3346L26 26.2538Z" />
                </svg>
            </div>
            <div class="_flex _justify-end _items-center">
                @widget('contacts:hotline', ['classes' => '_mr-sm _nmb-sm'])
                @widget('ui:button', [
                    'component' => 'button',
                    'classes' => 'js-import',
                    'attrs' => [
                        'type' => 'button',
                        'data-mfp' => 'ajax',
                        'data-mfp-src' => route('callbacks.insurance-popup'),
                    ],
                    'text' => __('cms-services::site.Insurance case button'),
                    'modificators' => ['color-accent-2', 'theme-pulse-animated', 'size-md', 'uppercase', 'ls-def', 'bold']
                    ])
                {{-- @todo когда будет лк заменить класс на кнопке ниже с _mx-md _ml-md --}}
                @widget('ui:button', [
                    'component' => 'a',
                    'attrs' => [
                        'href' => route('home')
                    ],
                    'text' => __('cms-payments::site.Pay online button'),
                    'classes' => '_mx-md',
                    'modificators' => ['color-light', 'size-md', 'uppercase', 'ls-md', 'bold']])
                {{--<a href="#" class="link link--theme-profile">--}}
                    {{--@svg('user', 22, 23, 'link__icon')--}}
                {{--</a>--}}
                @widget('ui:lang-switcher')
            </div>
            {{--@widget('branch:main-contact')--}}
        </div>
    </div>
</header>
