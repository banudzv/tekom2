<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\Contracts\Group::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\Contracts\GroupTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
    ];
});
