@php
         /**
         * @var array|null $attributes
         * @var array|null $icon
         * @var string|null $label
         * @var string|null $classes
         * @var string $name
         * @var string|null $value
         * @var string|null $type
         * @var string|null $mode
         * @var string|null $component input|textarea|select2
         * @var string[]|null $modificators
         * @var array|null $selectOptions
         */

        $_mainClass = 'form-item';
        $_component = $component ?? 'input';

        $name = $name ?? uniqid('input-');

        $attributes = array_merge([
            'class' => $_mainClass . '__control',
            'type' => $type ?? 'text',
            'name' => $name,
            'id' => uniqid($name),
            'value' => $value ?? null
        ], $attributes ?? []);

        $mode = $mode ?? null;

        switch ($mode) {
            case 'email':
                $attributes = array_merge($attributes, [
                    'inputmode' => 'email',
                    'type' => $mode,
                ]);
                break;
            case 'password':
                $attributes = array_merge($attributes, [
                    'data-rule-minlength' => '8',
                    'type' => $mode,
                ]);
                break;
            case 'password_confirmation':
                $attributes = array_merge($attributes, [
                    'type' => 'password',
                    'data-rule-minlength' => '8',
                    'data-rule-equalTo' => '[name="' . ($equalTo ?? 'password') . '"]'
                ]);
                break;
            case 'tel':
                $attributes = array_merge($attributes, [
                    'type' => $mode,
                    'inputmode' => 'tel',
                ]);
                break;
            case 'time':
                $attributes = array_merge($attributes, [
                    'type' => $mode,
                    'inputmode' => 'text',
                ]);
                break;
            default:
                $attributes = array_merge($attributes, [
                    'inputmode' => 'text',
                    'spellcheck' => 'true'
                ]);
                break;
        }

        $_hasIcon = isset($icon) && count($icon);

        $modificators = $modificators ?? [];
        $_bemModifier = '';

        foreach ($modificators as $modificator) {
            $_bemModifier = $_bemModifier . ' ' . $_mainClass . '--' . $modificator;
        }

        $_bemModifier =  $_bemModifier . ' ' . $_mainClass . '--' . $_component;
@endphp


<div class="form-item {{ $_bemModifier }} {{ $_hasIcon ? 'form-item--with-icon' : '' }} {{ isset($classes) ? $classes : '' }}" data-control>
    @if (isset($label) && strlen($label))
        <label for="{{ $attributes['id'] }}" class="form-item__label">{{ $label }}</label>
    @endif

	<div class="form-item__body">
		@if($_component == 'textarea')
			<div class="form-item__box-reletive">
		@endif

		@switch($_component)
			@case('input')
			<input {!! Html::attributes($attributes) !!} id="{{ $name }}" data-field>
			@break
			@case('textarea')
			<textarea {!! Html::attributes($attributes) !!} id="{{ $name }}" data-field>{{ $value ?? '' }}</textarea>
			@break
			@case('select2')
			@component('cms-ui::components.form.select2', [
				'selectOptions' => $selectOptions ?? [],
				 'name' => $name,
				 'options' => $options,
				 'attrs' => $attributes
			])
				@if(isset($slot))
					{!! $slot !!}
				@else
					@foreach($options ?? [] as $key => $option)
						<option value="{{ $key }}">{{ $option }}</option>
					@endforeach
				@endif
			@endcomponent
			@break
			@default
			<input {!! Html::attributes($attributes) !!} id="{{ $name }}" data-field>
			@break
		@endswitch
	</div>

	@if($_component == 'textarea')
		</div>
	@endif
</div>
