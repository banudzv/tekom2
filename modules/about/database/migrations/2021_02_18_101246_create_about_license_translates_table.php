<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutLicenseTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_license_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('license_id');
            $table->string('locale')->index();
            $table->string('name')->nullable();

            $table->unique(['license_id', 'locale']);
            $table->foreign('license_id')
                ->references('id')
                ->on('about_licenses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_license_translations');
    }
}
