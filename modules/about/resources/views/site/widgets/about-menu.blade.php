<div class="section">
    <div class="container">
        <div class="_grid _justify-center">
            <div class="_cell _cell--12 _lg:cell--10">
                <div class="nav-block nav-block--theme-anchors nav-block--theme-about">
                    <div class="nav-block__anchors js-import" data-nav-tabs>
                        @foreach($menu as $item)
                            @php($isCurrentPage = url($item->link) === url()->current())
                            <a {{ $isCurrentPage ? null : 'href=' . url($item->link) }}
                               class="nav-block__anchor {{ $isCurrentPage ? 'is-active' : null }}"
                               data-nav-tab
                            >
                                {{ $item->name }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
