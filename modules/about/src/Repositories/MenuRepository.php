<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Menu;
use WezomCms\Core\Repositories\AbstractRepository;

class MenuRepository extends AbstractRepository
{
    protected function model()
    {
        return Menu::class;
    }
}
