@php
    /**
     * @var $assetManager \WezomCms\Core\Contracts\Assets\AssetManagerInterface
     */
@endphp
@widget('ui:search')
<div class="search-dim _lg:show" data-dim></div>
<div data-scroll-window="up"
     data-fixed-button
     class="button button--theme-up js-import"
     title="@lang('cms-ui::site.Вверх')">
    @svg('arrow-top-type-2', null, null, 'button__icon')
</div>

@widget('mobile-menu')

@foreach($assetManager->getCss(\WezomCms\Core\Contracts\Assets\AssetManagerInterface::POSITION_END_BODY) as $style)
    {!! $style !!}
@endforeach

@foreach($assetManager->getJs(\WezomCms\Core\Contracts\Assets\AssetManagerInterface::POSITION_END_BODY) as $script)
    {!! $script !!}
@endforeach

<script>
    (function (window) {
        window.env = {
			language: '<?php echo e(app()->getLocale()); ?>',
            maps: {
                source: 'https://maps.googleapis.com/maps/api/js',
                query: {
                    key: '{{ settings('settings.site.google_map_key') }}',
                    language: '{{ app()->getLocale() }}'
                }
            },
			validation: {
				bySelector: {}
			},
            mfpAdjust: {
                closeButton: '<button title="%title%" type="button" class="mfp-close">' +
                    '<svg width="15" height="15"><use xlink:href="{{ asset('svg/icons.svg#close') }}" /></svg>' +
                    '</button>',
            },
            messages: {
                'The given data was invalid.': '{{ __('cms-ui::site.Указанные данные неверные') }}',
                'Method Not Allowed': '{{ __('cms-ui::site.Метод не разрешен') }}',
                'Server Error': '{{ __('cms-ui::site.Ошибка сервера') }}',
                'Too Many Attempts.': '{{ __('cms-ui::site.Слишком много попыток') }}'
            },
			magnificPopup: {},
            i18n: {
                mmenu: {
                    menu: '{{ __('cms-ui::site.Меню') }}',
                    language: '{{ __('cms-ui::site.Язык') }}',
                    catalog: '{{ __('cms-ui::site.Каталог') }}'
                },
                confirm: {
                    yes: '{{ __('cms-ui::site.Да') }}',
                    no: '{{ __('cms-ui::site.Нет') }}'
                }
            }
        };
    })(window);
</script>
@include('cms-ui::partials.translations')
@includeIf('cms-orders::partials.cart-translations')
<script src="{{ asset('/js/jquery-3.5.1.min.js') }}" defer></script>
<script src="{{ compiled_assets('/app.js') }}" defer></script>
@if(session('flash-notifications'))
    <script>
        window.ResponseActions.showNotices(@json(session('flash-notifications')));
    </script>
@endif
{{-- .Render flash messages after redirect --}}

@include('cms-ui::partials.hidden.noscript')
@include('cms-ui::partials.hidden.hidden-sprite')
@include('cms-ui::partials.hidden.cookies')
