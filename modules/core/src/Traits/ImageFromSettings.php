<?php

namespace WezomCms\Core\Traits;

use WezomCms\Core\Image\ImageHandler;

trait ImageFromSettings
{
    public function getBannerUrl($key)
    {
        $imageSettings = settings($key, []);
        $image = array_get($imageSettings, 'image', null);

        if($image){
            return $image->getImageUrl(null,'settings', 'ru') ?? url(ImageHandler::PATH_TO_NO_IMAGE);
        }

        return url(ImageHandler::PATH_TO_NO_IMAGE);
    }
}
