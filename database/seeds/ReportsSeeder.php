<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use WezomCms\Reports\Models\Category;
use WezomCms\Reports\Models\CategoryTranslation;
use WezomCms\Reports\Models\Report;
use WezomCms\Reports\Models\ReportTranslation;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Models\ServiceGroupTranslation;

class ReportsSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('reports')->truncate();
        \DB::table('report_translations')->truncate();
        \DB::table('report_categories')->truncate();
        \DB::table('report_category_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = $this->getData();
        $groups = $this->getCategories();

        try {
            \DB::transaction(function () use ($data, $groups) {

                // категории
                foreach ($groups as $cat){

                    $category = new Category();
                    $category->sort = $cat['sort'];

                    $category->save();
                    foreach ($cat['translates'] as $lang => $t){
                        $translations = new CategoryTranslation();
                        $translations->locale = $lang;
                        $translations->name = $t['name'];
                        $translations->slug = $t['slug'];

                        $translations->category_id = $category->id;
                        $translations->save();
                    }

                    foreach ($cat['children'] ?? [] as $c){
                        $child = new Category();
                        $child->sort = $c['sort'];
                        $child->parent_id = $category->id;
                        $child->save();

                        foreach ($c['translates'] as $l => $ct){
                            $translations_child = new CategoryTranslation();
                            $translations_child->locale = $l;
                            $translations_child->name = $ct['name'];
                            $translations_child->slug = $ct['slug'];

                            $translations_child->category_id = $child->id;
                            $translations_child->save();
                        }
                    }
                }

                foreach ($data as $item){
                    $model = new Report();
                    $model->category_id = $item['category'];
                    $model->published_at = $item['published_at'];
                    $model->year = $item['year'];

                    $model->save();
                    foreach ($item['translates'] as $lang => $trans){
                        $translations = new ReportTranslation();
                        $translations->locale = $lang;
                        $translations->name = $trans['name'];
                        $translations->report_id = $model->id;
                        $translations->save();
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function getCategories()
    {
        return [
            [
                'sort' => 0,
                'translates' => [
                    'ru' => [
                        'name' => 'Сообщение о проведении общих собраний',
                        'slug' => 'soobschenie-o-provedenii-obschih-sobraniy',
                    ],
                    'uk' => [
                        'name' => 'Повідомлення про проведення загальних зборів',
                        'slug' => 'povidomlennya-pro-provedennya-zagalnih-zboriv',
                    ]
                ],
                'children' => [
                    [
                        'sort' => 0,
                        'translates' => [
                            'ru' => [
                                'name' => 'Уведомления',
                                'slug' => 'uvedomleniya',
                            ],
                            'uk' => [
                                'name' => 'Повідомлення',
                                'slug' => 'povidomlennya',
                            ],
                        ]
                    ],
                    [
                        'sort' => 1,
                        'translates' => [
                            'ru' => [
                                'name' => 'Информация',
                                'slug' => 'informatsiya',
                            ],
                            'uk' => [
                                'name' => 'інформація',
                                'slug' => 'informatsiya',
                            ],
                        ]
                    ],
                ]
            ],
            [
                'sort' => 1,
                'translates' => [
                    'ru' => [
                        'name' => 'Особенная информация',
                        'slug' => 'osobennaya-informatsiya',
                    ],
                    'uk' => [
                        'name' => 'Особлива інформація',
                        'slug' => 'osobliva-informatsiya',
                    ],
                ],
            ],
            [
                'sort' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Годовые отчеты',
                        'slug' => 'godovye-otchety',
                    ],
                    'uk' => [
                        'name' => 'Річні звіти',
                        'slug' => 'richni-zviti',
                    ],
                ],
            ]
        ];
    }

    protected function getData()
    {

        return [
            [
                'category' => 2,
                'published_at' => '11.08.2005',
                'year' => '2005',
                'translates' => [
                    'ru' => [
                        'name' => 'отчет_1',
                    ],
                    'uk' => [
                        'name' => 'звіт_1',
                    ],
                ]
            ],
            [
                'category' => 2,
                'published_at' => '11.11.2005',
                'year' => '2005',
                'translates' => [
                    'ru' => [
                        'name' => 'отчет_2',
                    ],
                    'uk' => [
                        'name' => 'звіт_2',
                    ],
                ]
            ],
            [
                'category' => 2,
                'published_at' => '11.02.2006',
                'year' => '2006',
                'translates' => [
                    'ru' => [
                        'name' => 'отчет_3',
                    ],
                    'uk' => [
                        'name' => 'звіт_3',
                    ],
                ]
            ],
            [
                'category' => 2,
                'published_at' => '11.08.2007',
                'year' => '2007',
                'translates' => [
                    'ru' => [
                        'name' => 'отчет_4',
                    ],
                    'uk' => [
                        'name' => 'звіт_4',
                    ],
                ]
            ],
            [
                'category' => 2,
                'published_at' => '21.08.2013',
                'year' => '2013',
                'translates' => [
                    'ru' => [
                        'name' => 'отчет_5',
                    ],
                    'uk' => [
                        'name' => 'звіт_5',
                    ],
                ]
            ],
        ];
    }
}





