<?php

use Faker\Generator as Faker;

class AboutMenuSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('about_menu')->truncate();
        \DB::table('about_menu_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = $this->getData();

        try {
            \DB::transaction(function () use ($data) {

                foreach ($data as $item){
                    $menu = new \WezomCms\About\Models\Menu();
                    $menu->sort = $item['sort'];

                    $menu->save();
                    foreach ($item['translates'] as $lang => $t){
                        $translations = new \WezomCms\About\Models\MenuTranslation();
                        $translations->locale = $lang;
                        $translations->name = $t['name'];
                        $translations->link = $t['link'];
                        $translations->menu_id = $menu->id;
                        $translations->save();
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function getData()
    {
        return [
            [
                'sort' => 0,
                'translates' => [
                    'ru' => [
                        'name' => 'О нас',
                        'link' => '/ru/about/about-us',
                    ],
                    'uk' => [
                        'name' => 'Про нас',
                        'link' => '/about/about-us',
                    ]
                ],
            ],
            [
                'sort' => 1,
                'translates' => [
                    'ru' => [
                        'name' => 'Информация для ознакомления',
                        'link' => '/ru/about/information',
                    ],
                    'uk' => [
                        'name' => 'Інформація для ознайомлення',
                        'link' => '/about/information',
                    ]
                ],
            ],
            [
                'sort' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Детально о ЧAO CK TEKOM',
                        'link' => '/ru/about/details',
                    ],
                    'uk' => [
                        'name' => 'Детально про ЧAO CK TEKOM',
                        'link' => '/about/details',
                    ]
                ],
            ],
            [
                'sort' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Агенты',
                        'link' => '/ru/about/agent',
                    ],
                    'uk' => [
                        'name' => 'Агенти',
                        'link' => '/about/agent',
                    ]
                ],
            ],
            [
                'sort' => 4,
                'translates' => [
                    'ru' => [
                        'name' => 'Правила страхования',
                        'link' => '/ru/about/rule-insurance',
                    ],
                    'uk' => [
                        'name' => 'Правила страхування',
                        'link' => '/about/rule-insurance',
                    ]
                ],
            ],
            [
                'sort' => 5,
                'translates' => [
                    'ru' => [
                        'name' => 'Лицензии',
                        'link' => '/ru/about/licenses',
                    ],
                    'uk' => [
                        'name' => 'Ліцензії',
                        'link' => '/about/licenses',
                    ]
                ],
            ],
            [
                'sort' => 6,
                'translates' => [
                    'ru' => [
                        'name' => 'Договора публичной оферты',
                        'link' => '/ru/about/contracts',
                    ],
                    'uk' => [
                        'name' => 'Договори публічної оферти',
                        'link' => '/about/contracts',
                    ]
                ],
            ],
        ];
    }

}






