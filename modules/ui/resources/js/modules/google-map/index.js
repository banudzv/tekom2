// ----------------------------------------
// Imports
// ----------------------------------------

import 'custom-jquery-methods/fn/has-inited-key';
import { initEach } from 'js#/utils/init-each';
import createNewFactory  from 'js#/utils/create-new-factory';
import { GoogleMapPluginAbstract } from './abstract';
import * as factory from './factory';

// ----------------------------------------
// Public
// ----------------------------------------

export default function ($containers) {
	initEach($containers, 'GoogleMapPluginIsInitialized', _init);
}

// ----------------------------------------
// Private
// ----------------------------------------

function _init ($container) {
	const data = $container.data('initialize-google-map');
	const instance = createNewFactory(GoogleMapPluginAbstract, factory, $container, data);
	instance.initialize();
}
