<?php

namespace WezomCms\Branches\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Http\Requests\ChangeStatus\RequiredIfMessageTrait;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class BranchRequest extends FormRequest
{
    use LocalizedRequestTrait;
    use RequiredIfMessageTrait {
        messages as traitMessages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'nullable|string|max:255',
                'address' => 'nullable|string|max:255|required_if:published,1',
                'time_working' => 'nullable|string|max:900|required_if:published,1',
                'date_create' => 'nullable|string|max:900',
            ],
            [
                'published' => 'required',
                'is_main' => 'required',
                'phones.*' => 'required|string|max:255|distinct|regex:/^\+?[\d\s\(\)-]+$/',
                'email' => 'nullable|email|max:255',
                'lat' => 'nullable|string|max:50',
                'lon' => 'nullable|string|max:50',
                'type' => 'required',
                'city_id' => 'required|integer',
                'employee_id' => 'nullable|integer',
                'code' => 'nullable|string|max:100',
                'zip_code' => 'nullable|string|max:50',
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-branches::admin.Name'),
                'time_working' => __('cms-branches::admin.Time working'),
                'address' => __('cms-branches::admin.Address'),
                'date_create' => __('cms-branches::admin.Date create'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'is_main' => __('cms-branches::admin.Is main.'),
                'phones.*' => __('cms-branches::admin.Phones'),
                'email' => __('cms-branches::admin.E-mail'),
                'lat' => __('cms-regions::admin.latitude'),
                'lon' => __('cms-regions::admin.longitude'),
                'city_id' => __('cms-branches::admin.City'),
                'employee_id' => __('cms-branches::admin.Employee'),
                'type' => __('cms-branches::admin.Branch type'),
                'code' => __('cms-branches::admin.Code'),
                'zip_code' => __('cms-branches::admin.Zip code'),
            ]
        );
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $messages = $this->traitMessages();

        $messages['phones.*.regex'] = __('cms-core::admin.layout.Invalid phone value');

        return $messages;
    }
}
