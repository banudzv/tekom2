<script>window.$wzmOld_URL_IMG = '{{ asset('assets/css/static/pic/wezom-info-red.gif') }}';</script>
<noscript>
    <link rel="stylesheet" href="{{ asset('assets/css/noscript.css') }}">
    <div class="noscript-msg">
        <input id="noscript-msg__input" class="noscript-msg__input" type="checkbox" title="@lang('cms-ui::site.Закрыть')">
        <div class="noscript-msg__container">
            <label class="noscript-msg__close" for="noscript-msg__input">&times;</label>
            <a class="noscript-msg__wezom-link" href="https://wezom.com.ua/" target="_blank"
               title="@lang('cms-ui::site.Агентство системных интернет-решений Wezom')">&nbsp;</a>
            <div class="noscript-msg__content">
                <strong>@lang('cms-ui::site.В Вашем браузере отключен JavaScript! Для корректной работы с сайтом необходима поддержка Javascript. Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера')</strong>
            </div>
        </div>
    </div>
</noscript>
