<?php

use WezomCms\Branches\Models\Branch;
use WezomCms\Regions\Models\City;

class BranchSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('branches')->truncate();
        \DB::table('branch_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            \DB::transaction(function (){

                foreach ($this->getData() as $item){
                    $city = City::orderBy(DB::raw('RAND()'))->first();
                    $employee = \WezomCms\OurTeam\Models\Employee::orderBy(DB::raw('RAND()'))->first();

                    $b = new Branch();
                    $b->published = $item['published'];
                    $b->email = $item['email'];
                    $b->phones = $item['phones'];
                    $b->sort = $item['sort'];
                    $b->is_main = $item['is_main'] ?? false;
                    $b->city_id = $city->id;
                    $b->employee_id = $employee->id;
                    $b->type = $item['type'];
                    $b->save();

                    // services
                    $someServices = \WezomCms\Services\Models\Service::orderBy(DB::raw('RAND()'))->take(random_int(2, 3))->pluck('id')->toArray();
                    $b->services()->sync($someServices);

                    foreach ($item['translates'] as $lang => $tran){
                        $bt = new \WezomCms\Branches\Models\BranchTranslation();
                        $bt->branch_id = $b->id;
                        $bt->locale = $lang;
                        $bt->address = $tran['address'];
                        $bt->time_working = $tran['time_working'];
                        $bt->save();
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    public function getData()
    {
        return [
            [
                'published' => true,
                'email' => 'odessa@gmail.com',
                'phones' => ['099-999-99-90'],
                'sort' => 1,
                'type' => Branch::TYPE_OFFICE,
                'is_main' => true,
                'translates' => [
                    'ru' => [
                        'address' => 'ул. Малая Арнаутская 17',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Вс : выходной'
                    ],
                    'uk' => [
                        'address' => 'вул. Мала Арнаутська 17',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Нд : вихідний'
                    ]

                ]
            ],
            [
                'published' => true,
                'email' => 'kherson@gmail.com',
                'phones' => ['099-999-99-91', '099-999-99-92'],
                'sort' => 2,
                'type' => Branch::TYPE_OFFICE,
                'translates' => [
                    'ru' => [
                        'address' => 'ул. Ярослава Мудрого 17',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Вс : выходной'
                    ],
                    'uk' => [
                        'address' => 'вул. Ярослава Мудрого 17',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Нд : вихідний'
                    ]
                ]
            ],
            [
                'published' => true,
                'email' => 'nikolaev@gmail.com',
                'phones' => ['099-999-99-93', '099-999-99-94'],
                'sort' => 3,
                'type' => Branch::TYPE_OFFICE,
                'translates' => [
                    'ru' => [
                        'address' => 'ул. Мира 29',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Вс : выходной'
                    ],
                    'uk' => [
                        'address' => 'вул. Мира 29',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Нд : вихідний'
                    ]
                ]
            ],
            [
                'published' => true,
                'email' => 'kiev@gmail.com',
                'phones' => ['099-999-99-95'],
                'sort' => 4,
                'type' => Branch::TYPE_OFFICE,
                'translates' => [
                    'ru' => [
                        'address' => 'ул. Леонтовича 5',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Вс : выходной'
                    ],
                    'uk' => [
                        'address' => 'вул. Леонтовича 5',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Нд : вихідний'
                    ]
                ]
            ],
            [
                'published' => true,
                'email' => 'phoros@gmail.com',
                'phones' => ['099-999-99-91', '099-999-99-92'],
                'sort' => 5,
                'type' => Branch::TYPE_OFFICE,
                'translates' => [
                    'ru' => [
                        'address' => 'ул. Набережная',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Вс : выходной'
                    ],
                    'uk' => [
                        'address' => 'вул. Набережная',
                        'time_working' => 'Пн-Пт : 8:00-17:00 <br> Сб : 9:00-15:00 <br> Нд : вихідний'
                    ]
                ]
            ],
        ];
    }
}
