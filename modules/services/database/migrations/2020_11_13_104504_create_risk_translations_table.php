<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('risk_id');
            $table->string('locale')->index();
            $table->string('name');

            $table->unique(['risk_id', 'locale']);
            $table->foreign('risk_id')
                ->references('id')
                ->on('risks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_translations');
    }
}
