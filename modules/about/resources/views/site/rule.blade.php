@extends('cms-ui::layouts.main')

@php
    /**
     * @var $banner string|null
     * @var $description string|null
     * @var $groups \WezomCms\About\Models\Rules\Group[]
     * @var $rules \WezomCms\About\Models\Rules\Rule[]
     */

@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $description,
        'classes' => 'heading-section--contacts'
    ])

    @widget('about:menu')

    <div class="section section--information section--bg-grey">
        <div class="container">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _lg:cell--10">
                    <div class="text text--size-lg text--color-grey _mb-xl">
                        @lang('cms-about::site.about-rule.title')
                    </div>
                    @foreach($groups as $group)
                        <div class="_mb-xl">
                            <div class="title title--size-h4 _mb-xl">{{ $group->name }}</div>
                            @foreach($group->rules as $rule)
                                @include('cms-about::site.partials.row-file', ['item' => $rule])
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
