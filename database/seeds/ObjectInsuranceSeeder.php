<?php

class ObjectInsuranceSeeder extends BaseSeeder
{
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('object_insurances')->truncate();
        \DB::table('object_insurance_translations')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = $this->getData();

        try {
            \DB::transaction(function () use ($data) {

                foreach ($data as $item){
                    $model = new \WezomCms\Services\Models\ObjectInsurance();
                    $model->sort = $item['sort'];

                    $model->save();
                    foreach ($item['translates'] as $lang => $trans){
                        $translations = new \WezomCms\Services\Models\ObjectInsuranceTranslation();
                        $translations->locale = $lang;
                        $translations->name = $trans['name'];
                        $translations->text = $trans['text'];
                        $translations->object_insurance_id = $model->id;
                        $translations->save();
                    }
                }
            });
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    protected function getData()
    {
        return [
            [
                'sort' => 0,
                'translates' => [
                    'ru' => [
                        'name' => 'Любые грузы',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Будь-які вантажі',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 1,
                'translates' => [
                    'ru' => [
                        'name' => 'Прибыль',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Прибуток',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 2,
                'translates' => [
                    'ru' => [
                        'name' => 'Здоровье',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Здоров\'я',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 3,
                'translates' => [
                    'ru' => [
                        'name' => 'Транспорт',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Транспорт',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 4,
                'translates' => [
                    'ru' => [
                        'name' => 'Жизнь',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Життя',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 5,
                'translates' => [
                    'ru' => [
                        'name' => 'Склады',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Склади',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 6,
                'translates' => [
                    'ru' => [
                        'name' => 'Финансовые риски',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Фінансові ризики',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
            [
                'sort' => 7,
                'translates' => [
                    'ru' => [
                        'name' => 'Репутацию',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                    'uk' => [
                        'name' => 'Репутацію',
                        'text' => $this->getFaker()->realText($this->getFaker()->numberBetween(100, 150)),
                    ],
                ]
            ],
        ];
    }
}





