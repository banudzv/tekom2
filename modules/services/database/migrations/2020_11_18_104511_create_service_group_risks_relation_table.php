<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceGroupRisksRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_groups_risks_relation', function (Blueprint $table) {
            $table->unsignedBigInteger('service_group_id');
            $table->foreign('service_group_id')
                ->references('id')
                ->on('service_groups')
                ->onDelete('cascade');
            $table->unsignedBigInteger('risk_id');
            $table->foreign('risk_id')
                ->references('id')
                ->on('risks')
                ->onDelete('cascade');

            $table->primary(['service_group_id', 'risk_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_groups_risks_relation');
    }
}



