'use strict';

/**
 * @module
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {number} number
 * @param {number} [fractionDigits=2]
 * @return {number}
 */
export const toFixed = (number, fractionDigits = 2) => +number.toFixed(fractionDigits);
