@extends('cms-ui::layouts.main')

@php
    /**
     * @var $banner string|null
     * @var $description string|null
     * @var $informations \WezomCms\About\Models\Information[]
     */

@endphp

@section('content')
    @widget('ui:heading-section', [
        'bannerImage' => $banner,
        'title' => SEO::getH1(),
        'text' => $description,
        'classes' => 'heading-section--contacts'
    ])

    @widget('about:menu')

    <div class="section section--information section--bg-grey">
        <div class="container">
            <div class="_grid _justify-center">
                <div class="_cell _cell--12 _lg:cell--10">
                    <div class="text text--size-lg text--color-grey _mb-md">
                        @lang('cms-about::site.about-information.title')
                    </div>
                    @foreach($informations as $key => $information)
                        <div class="info-block">
                            <div class="info-block__header">
                                <div class="info-block__counter">
                                    {{ ++$key }}
                                </div>
                                <div class="info-block__name">
                                    {{$information->name}}
                                </div>
                            </div>
                            <div class="info-block__content">
                                <div class="wysiwyg wysiwyg--theme-info js-import" data-wrap-media data-draggable-table>
                                    {!! $information->text !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
