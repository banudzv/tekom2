import $ from 'jquery';

function testUAPhone(value) {
	// eslint-disable-next-line no-useless-escape
	return /^(((\+?)(38))\s?)((0[0-9]{2})|(\(0[0-9]{2}\)))(\-|\s)?(([0-9]{3})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{2})|([0-9]{2})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{3})|([0-9]{2})(\-|\s)?([0-9]{3})(\-|\s)?([0-9]{2}))$/.test(
		value
	); // eslint-disable-line no-useless-escape
}

const emailRegExp = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/; // eslint-disable-line no-useless-escape

/**
 * Методы валидации
 * @var {Object} methods
 */
const methods = {
	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::email
	 */
	email(value, element, param) {
		return this.optional(element) || emailRegExp.test(value);
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement} element
	 * @param {string|RegExp} param
	 * @return {boolean}
	 * @method methods::pattern
	 */
	pattern(value, element, param) {
		if (this.optional(element)) {
			return true;
		}
		if (typeof param === 'string') {
			param = new RegExp(`^(?:${param})$`);
		}
		return param.test(value);
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::password
	 */
	password(value, element, param) {
		return this.optional(element) || /^\S.*$/.test(value);
	},

	/**
	 * Проверка общего объема всех файлов в KB
	 * @param {string} value
	 * @param {HTMLInputElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::filesize
	 */
	filesize(value, element, param) {
		let kb = 0;
		for (let i = 0; i < element.files.length; i++) {
			kb += element.files[i].size;
		}
		return this.optional(element) || kb / 1024 <= param;
	},

	/**
	 * Максимальное количество файлов для загрузки
	 * @param {string} value
	 * @param {HTMLInputElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::maxupload
	 */
	maxupload(value, element, param) {
		return element.files.length <= param;
	},

	/**
	 * Проверка объема каждого файла в KB
	 * @param {string} value
	 * @param {HTMLInputElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::filesizeeach
	 */
	filesizeeach(value, element, param) {
		let flag = true;
		for (let i = 0; i < element.files.length; i++) {
			if (element.files[i].size / 1024 > param) {
				flag = false;
				break;
			}
		}
		return this.optional(element) || flag;
	},

	/**
	 * Проверка типа файла по расширению
	 * @param {string} value
	 * @param {HTMLInputElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::filetype
	 */
	filetype(value, element, param) {
		let result;
		const extensions =
			'png|jpe?g|gif|svg|doc|pdf|zip|rar|tar|html|swf|txt|xls|docx|xlsx|odt';
		if (element.files.length < 1) {
			return true;
		}

		param = typeof param === 'string' ? param.replace(/,/g, '|') : extensions;
		if (element.multiple) {
			const files = element.files;
			for (let i = 0; i < files.length; i++) {
				const value = files[i].name;
				const valueMatch = value.match(new RegExp('.(' + param + ')$', 'i'));

				result = this.optional(element) || valueMatch;
				if (result === null) {
					break;
				}
			}
		} else {
			const valueMatch = value.match(new RegExp('\\.(' + param + ')$', 'i'));
			result = this.optional(element) || valueMatch;
		}
		return result;
	},

	/**
	 * Проверка валидности одного из указанных элементов
	 * _этот или другой - должен быть валидным_
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement|HTMLSelectElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::or
	 */
	or(value, element, param) {
		const $module = $(element.form);
		return $module.find(param + ':filled').length;
	},

	/**
	 * Проверка валидности слов (чаще всего используется для имени или фамилии)
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement} element
	 * @return {boolean}
	 * @method methods::word
	 */
	word(value, element) {
		const testValue = /^[a-zа-яіїєёґąćęłńóśźżäöüß\'\`\- ]*$/i.test(value); // eslint-disable-line no-useless-escape
		return this.optional(element) || testValue;
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::login
	 */
	login(value, element, param) {
		// eslint-disable-next-line no-useless-escape
		const testValue = /^[0-9a-zа-яіїєёґąćęłńóśźżäöüß][0-9a-zа-яіїєёґąćęłńóśźżäöüß\-\._]+$/i.test(
			value
		); // eslint-disable-line no-useless-escape
		return this.optional(element) || testValue;
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement} element
	 * @return {boolean}
	 * @method methods::phoneua
	 */
	phoneua(value, element) {
		return this.optional(element) || testUAPhone(value);
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement} element
	 * @param {Object} param
	 * @return {boolean}
	 * @method methods::phone
	 */
	phone(value, element, param) {
		return (
			this.optional(element) ||
			// eslint-disable-next-line no-useless-escape
			/^(((\+?)(\d{1,3}))\s?)?(([0-9]{0,4})|(\([0-9]{3}\)))(\-|\s)?(([0-9]{3})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{2})|([0-9]{2})(\-|\s)?([0-9]{2})(\-|\s)?([0-9]{3})|([0-9]{2})(\-|\s)?([0-9]{3})(\-|\s)?([0-9]{2}))$/.test(
				value
			)
		); // eslint-disable-line no-useless-escape
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement|HTMLSelectElement} element
	 * @return {boolean}
	 * @method methods::validTrue
	 */
	phonemask(value, element) {
		return $(element).data('valid') === true;
	},

	/**
	 * @param {string} value
	 * @return {boolean}
	 * @method methods::nospace
	 */
	nospace(value) {
		const str = String(value).replace(/\s|\t|\r|\n/g, '');
		return str.length > 0;
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement|HTMLSelectElement} element
	 * @return {boolean}
	 * @method methods::validTrue
	 */
	validdata(value, element) {
		return $(element).data('valid') === true;
	},

	/**
	 * @param {string} value
	 * @param {HTMLInputElement|HTMLTextAreaElement|HTMLSelectElement} element
	 * @return {boolean}
	 * @method methods::validTrue
	 */
	emailphone(value, element) {
		return this.optional(element) || testUAPhone(value) || emailRegExp.test(value);
	},

	/**
	 * Check data by format dd.mm.yyyy
	 * @param value
	 * @param element
	 * @returns {boolean}
	 */
	date(value, element) {
		return (
			this.optional(element) ||
			(() => {
				const match = value.match(
					/(?:0[1-9]|[12][0-9]|3[01])\.(?:0[1-9]|1[0-2])\.(?:19\d{2}|20\d{2})/
				);
				if (!match) {
					return false;
				}
				const parse = match.input.split('.');
				const day = +parse[0];
				const month = +parse[1];

				let year = parse[2];
				if (year.length !== 4) {
					return false;
				}

				year = +year;

				if (month < 1 || month > 12) {
					return false;
				} else if (day < 1 || day > 31) {
					return false;
				} else if (
					(month === 4 || month === 6 || month === 9 || month === 11) &&
					day === 31
				) {
					return false;
				} else if (month === 2) {
					const isLeap =
						year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0);
					if (day > 29 || (day === 29 && !isLeap)) {
						return false;
					}
				}

				return true;
			})()
		);
	}
};

export default methods;
