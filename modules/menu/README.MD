## Installation

#### Use console
```
composer require wezom-cms/menu
```
#### Or edit composer.json
```
"require": {
    ...
    "wezom-cms/menu": "^7.0"
}
```
#### Install dependencies:
```
composer update
```
#### Package discover
```
php artisan package:discover
```
#### Run migrations
```
php artisan migrate
```

## Publish
#### Assets
```
php artisan vendor:publish --provider="WezomCms\Menu\MenuServiceProvider" --tag="assets"
```
#### Views
```
php artisan vendor:publish --provider="WezomCms\Menu\MenuServiceProvider" --tag="views"
```
#### Config
```
php artisan vendor:publish --provider="WezomCms\Menu\MenuServiceProvider" --tag="config"
```
#### Lang
```
php artisan vendor:publish --provider="WezomCms\Menu\MenuServiceProvider" --tag="lang"
```
