import $ from 'jquery';
import throttle from 'lodash.throttle';

export default function ($elements) {
	$elements.each((i, el) => {
		const $el = $(el);

		if ($el.hasInitedKey('inviewInitialized')) {
			return true;
		}

		init($el);
	});
}

function init ($element) {
	const calculateOffset = () => {
		const scrollTop = $(window).scrollTop();
		const elementOffset = $element.offset().top;
		const halfScreen = window.innerHeight;

		if (scrollTop + halfScreen > elementOffset) {
			$element.addClass('is-in-view');
			$(window).off('scroll', calculateOffset);
			$element.off('inview');
		}
	};
	$(window).on('scroll', throttle(calculateOffset, 100));
}
