<?php

namespace WezomCms\Reports\ModelFilters;

use Carbon\Carbon;
use EloquentFilter\ModelFilter;
use WezomCms\Reports\Models\Category;
use WezomCms\Core\Contracts\Filter\FilterListFieldsInterface;
use WezomCms\Core\Filter\FilterField;

/**
 * Class CategoryFilter
 * @package WezomCms\Catalog\ModelFilters
 * @mixin Category
 */
class ReportFilter extends ModelFilter implements FilterListFieldsInterface
{
    /**
     * Generate array with fields
     * @return iterable|FilterField[]
     */
    public function getFields(): iterable
    {
        $optionsTree = view(
            'cms-reports::admin.categories.categories-options',
            ['tree' => Category::getForSelect(), 'name' => 'category_id']
        );

        return [
            FilterField::makeName(),
            FilterField::published(),
            FilterField::make()
                ->name('category')
                ->label(__('cms-reports::admin.Category'))
                ->class('js-select2')
                ->type(FilterField::TYPE_SELECT_WITH_CUSTOM_OPTIONS)
                ->customOptions($optionsTree)
            ,
            FilterField::make()->name('published_at')->label(__('cms-reports::admin.Published at'))
                ->type(FilterField::TYPE_DATE_RANGE),
        ];
    }

    public function name($name)
    {
        $this->related('translations', 'name', 'LIKE', '%' . $name . '%');
    }

    public function published($published)
    {
        $this->where('published', $published);
    }

    public function category($categoryId)
    {
        $this->where('category_id', $categoryId);
    }

    public function publishedAtFrom($date)
    {
        $this->where('published_at', '>=', Carbon::parse($date));
    }

    public function publishedAtTo($date)
    {
        $this->where('published_at', '<=', Carbon::parse($date)->endOfDay());
    }
}
