<?php

namespace WezomCms\Orders\Cart\Adapters;

use Illuminate\Support\Collection;
use WezomCms\Catalog\Models\Product;
use WezomCms\Orders\Contracts\CartAdapterInterface;
use WezomCms\Orders\Contracts\CartInterface;
use WezomCms\Orders\Contracts\CartItemInterface;

class JsonAdapter implements CartAdapterInterface
{
    /**
     * @var CartInterface
     */
    private $cart;

    /**
     * JsonAdapter constructor.
     * @param  CartInterface  $cart
     */
    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Adapt data to concrete template.
     *
     * @return mixed
     */
    public function adapt()
    {
        $items = [];

        $ids = $this->cart->content()->map(function (CartItemInterface $cartItem) {
            return $cartItem->getId();
        });

        /** @var Collection|Product[] $products */
        $products = Product::published()->whereIn('id', $ids)->get()->keyBy('id');

        foreach ($this->cart->content() as $cartItem) {
            /** @var $cartItem CartItemInterface */
            $product = $products->get($cartItem->getId());
            if (!$product) {
                continue;
            }

            $items[] = [
                'row_id' => $cartItem->getUniqueId(),
                'url' => $product->getFrontUrl(),
                'src' => $product->getImageUrl(),
                'title' => $product->name,
                'code' => $product->id,
                'price' => money($product->cost),
                'quantity' => [
                    'value' => $cartItem->getQuantity(),
                    'unit' => $product->unit(),
                    'min' => $product->minCountForPurchase(),
                    'max' => 9999,
                    'step' => $product->stepForPurchase(),
                ],
                'discount' => $cartItem->totalDiscounted(),
                'total_price' => $cartItem->getTotal(),
                'promo_code' => $cartItem->isAppliedCondition('WezomCms\PromoCodes\Conditions\PromoCodeCondition'),
                'in_favorite' => method_exists($product, 'isFavorite') ? $product->isFavorite() : false,
            ];
        }

        return [
            'subtotal_price' => $this->cart->subTotal(),
            'discount' => $this->cart->discounted(),
            'total_price' => $this->cart->total(),
            'item_count' => $this->cart->count(),
            'promo_code' => method_exists($this->cart, 'hasCondition')
                ? $this->cart->hasCondition('WezomCms\PromoCodes\Conditions\PromoCodeCondition')
                : false,
            'currency' => money()->siteCurrencySymbol(),
            'checkout_path' => route('checkout.step1'),
            'items' => $items,
        ];
    }
}
