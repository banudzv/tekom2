<?php

use WezomCms\Menu\Widgets;

return [
    'widgets' => [
        'menu' => Widgets\MenuWidget::class,
        'mobile-menu' => Widgets\MobileMenuWidget::class,
    ],
    'groups' => [
        'header' => [
            'name' => 'cms-menu::admin.Header',
            'depth' => 2,
        ],
        'footer' => [
            'name' => 'cms-menu::admin.Footer',
            'depth' => 1,
        ],
        'mobile' => [
            'name' => 'cms-menu::admin.Mobile',
            'depth' => 10,
        ],
    ],
];
