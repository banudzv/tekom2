<?php

namespace WezomCms\Partners\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Core\Settings\MetaFields\SeoText;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\SiteLimit;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\Partners\Http\Requests\Admin\PartnerRequest;
use WezomCms\Partners\Models\Partner;
use WezomCms\Partners\Repositories\PartnerCategoryRepository;

class PartnersController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Partner::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-partners::admin';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.partners';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = PartnerRequest::class;

    /**
     * @var PartnerCategoryRepository
     */
    private $categoryRepository;

    public function __construct(PartnerCategoryRepository $categoryRepository)
    {
        parent::__construct();

        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-partners::admin.Partners');
    }

    /**
     * @return string|null
     */
    protected function frontUrl(): ?string
    {
        if (config('cms.partners.partners.index_page')) {
            return route('partners');
        } else {
            return null;
        }
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param  Partner $obj
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($obj, array $viewData): array
    {
        return [
            'categories' => $this->categoryRepository->getBySelect(),
        ];
    }

    /**
     * @param Partner  $obj
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($obj, array $viewData): array
    {
        return [
            'categories' => $this->categoryRepository->getBySelect(),
        ];
    }

    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        $result = [
            AdminLimit::make(),
        ];

        if (config('cms.partners.partners.index_page')) {
            $result[] = SiteLimit::make()->setName(__('cms-partners::admin.Partners limit at page'));
            $result[] = SeoFields::make(
                'Partners',
                [SeoText::make()->setName(__('cms-partners::admin.Text'))->setSort(3)]
            );
        }

        return $result;
    }
}
