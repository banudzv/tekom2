<?php

namespace WezomCms\News\Http\Controllers\Site;

use Illuminate\Pagination\Paginator;
use Spatie\SchemaOrg\Schema;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\MicroDataTrait;
use WezomCms\Core\Traits\OGImageTrait;
use WezomCms\News\Models\News;
use WezomCms\News\Models\NewsTag;

class NewsController extends SiteController
{
    use OGImageTrait;
    use MicroDataTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // Module settings
        $settings = settings('news.site', []);

        $pageName = array_get($settings, 'name');

        $result = News::published()
            ->orderByDesc('published_at')
            ->latest('id')
            ->paginate(array_get($settings, 'limit', 10));

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('news'));

        // SEO
        $this->seo()
            ->setPageName($pageName)
            ->setTitle(array_get($settings, 'title'))
            ->setH1(array_get($settings, 'h1'))
            ->setDescription(array_get($settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'))
            ->setNext($result->nextPageUrl())
            ->setPrev($result->previousPageUrl());

        // Render
        return view('cms-news::site.index', [
            'result' => $result,
        ]);
    }

    /**
     * @param  string  $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag($slug)
    {
        /** @var NewsTag $tag */
        $tag = NewsTag::publishedWithSlug($slug)->firstOrFail();

        /** @var Paginator $result */
        $result = $tag->news()
            ->published()
            ->orderByDesc('published_at')
            ->latest('id')
            ->paginate(settings('news.site.limit', 10));

        $this->setLangSwitchers($tag, 'news.tag', ['tag' => 'slug'], false);

        // Breadcrumbs
        $this->addBreadcrumb(settings('news.site.name'), route('news'));
        $this->addBreadcrumb($tag->name, $tag->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($tag->title)
            ->setPageName($tag->name)
            ->setH1($tag->h1)
            ->setDescription($tag->description)
            ->metatags()
            ->setKeywords($tag->keywords)
            ->setNext($result->nextPageUrl())
            ->setPrev($result->previousPageUrl());

        // Render
        return view('cms-news::site.index', [
            'result' => $result,
            'currentTag' => $slug,
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \WezomCms\Core\Image\Exceptions\IncorrectImageSizeException
     */
    public function inner($slug)
    {
        /** @var News $obj */
        $obj = News::publishedWithSlug($slug)->firstOrFail();

        $this->setLangSwitchers($obj, 'news.inner');

        // Add views
        views($obj)->record();

        // Breadcrumbs
        $this->addBreadcrumb(settings('news.site.name'), route('news'));
        $this->addBreadcrumb($obj->name, $obj->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($obj->title)
            ->setH1($obj->h1)
            ->setPageName($obj->name)
            ->setDescription($obj->description)
            ->metatags()
            ->setKeywords($obj->keywords);

        $this->setOGImage($obj);

        $this->setMicroData($obj);

        // Render
        return view('cms-news::site.inner', [
            'obj' => $obj,
        ]);
    }

    /**
     * @param  News  $news
     * @throws \WezomCms\Core\Image\Exceptions\IncorrectImageSizeException
     */
    private function setMicroData(News $news)
    {
        $siteAsOrganization = $this->organization();

        $schemaNews = Schema::newsArticle()
            ->url($news->getFrontUrl())
            ->name($news->name)
            ->headline($news->title ?: $news->name)
            ->description($news->description)
            ->author($siteAsOrganization)
            ->publisher($siteAsOrganization)
            ->datePublished($news->created_at)
            ->dateModified($news->updated_at);

        if ($news->imageExists()) {
            $schemaNews->image($news->getImageUrl());
        }

        $this->renderMicroData($schemaNews);
    }
}
