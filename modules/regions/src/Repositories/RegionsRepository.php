<?php

namespace WezomCms\Regions\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Regions\Models\Region;

class RegionsRepository extends AbstractRepository
{
	protected function model()
	{
		return Region::class;
	}

	public function getFrontBySelect(array $ids)
    {
        $data = [];

        $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                }
            ])
            ->whereIn('id', $ids)
            ->orderBy('sort')
            ->get()
            ->each(function($item, $key) use (&$data){
                $data[$key]['id'] = $item->id;
                $data[$key]['name'] = $item->name;
                $data[$key]['slug'] = $item->slug;
            });

        return $data;
    }

    public function getBySlug($slug)
    {
        return $this->getModelQuery()
            ->publishedWithSlug($slug)
            ->with(['branches', 'cities'])
            ->first();
    }
}
