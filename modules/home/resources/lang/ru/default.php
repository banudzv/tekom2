<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
        'Home' => 'Главная страница',
        'Text' => 'Текстовое описание страницы',
        'Edit home page' => 'Редактировать главную страницу',
    ],
    TranslationSide::SITE => [
        'Home' => 'Главная страница',
        'search results found' => 'По вашему запросу “<span>:search</span>” найдено :count',
        'Enter' => 'Ввойти',
        'Registration' => 'Регистрация',
        'Account' => 'Личный кабинет',
        'Catalog' => 'Каталог товаров',
        'in services' => 'В услугах'
    ]
];
