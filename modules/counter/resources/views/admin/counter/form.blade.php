<div class="row">
	<div class="col-lg-7">
		<div class="card mb-3">
			<div class="card-header">
				<h5 class="py-2"><strong>@lang('cms-core::admin.layout.Main data')</strong></h5>
			</div>
			<div class="card-body">
				<div class="form-group">
					{!! Form::label(str_slug('published'), __('cms-core::admin.layout.Published')) !!}
					{!! Form::status('published', old('published', $obj->exists ? $obj->published : true))  !!}
				</div>
				<div class="form-group">
					{!! Form::label(str_slug('start'), __('cms-counter::admin.start')) !!}
					{!! Form::text('start', old('start', $obj->exists ? $obj->start : ''))  !!}
				</div>
				<div class="form-group">
					{!! Form::label(str_slug('end'), __('cms-counter::admin.end')) !!}
					{!! Form::text('end', old('end', $obj->exists ? $obj->end : ''))  !!}
				</div>
				<div class="form-group">
					{!! Form::label(str_slug('step'), __('cms-counter::admin.step')) !!}
					{!! Form::number('step', old('step', $obj->exists ? $obj->step : 0))  !!}
				</div>
				<div class="form-group">
					{!! Form::label(str_slug('duration'), __('cms-counter::admin.duration')) !!}
					{!! Form::number('duration', old('duration', $obj->exists ? $obj->duration : 0))  !!}
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-5">
		<div class="card">
			<div class="card-body">
				@langTabs
				<div class="form-group">
					{!! Form::label($locale . '[name]', __('cms-core::admin.layout.Name')) !!}
                    {!! Form::textarea($locale . '[name]', old($locale . '.name', $obj->translateOrNew($locale)->name), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
				</div>
				@endLangTabs
			</div>
		</div>
	</div>
</div>
