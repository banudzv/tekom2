<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\Services\Models\Document::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\Services\Models\DocumentTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->realText($faker->numberBetween(10, 50)),
        'text' => $faker->realText($faker->numberBetween(200, 250)),
    ];
});
