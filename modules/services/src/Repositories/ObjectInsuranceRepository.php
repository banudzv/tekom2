<?php

namespace WezomCms\Services\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Services\Models\ObjectInsurance;

class ObjectInsuranceRepository extends AbstractRepository
{
    protected function model()
    {
        return ObjectInsurance::class;
    }
}

