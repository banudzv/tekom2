<?php

namespace WezomCms\About\Http\Controllers\Admin\License;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\About\Http\Requests\Admin\License\LicenseRequest;
use WezomCms\About\Models\License\License;
use WezomCms\About\Models\Rules\Rule;
use WezomCms\About\Repositories\LicenseGroupRepository;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;

class LicenseController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = License::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-about::admin.licenses.license';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.about-licenses';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = LicenseRequest::class;

    private LicenseGroupRepository $licenseGroupRepository;

    public function __construct(LicenseGroupRepository $licenseGroupRepository)
    {
        $this->licenseGroupRepository = $licenseGroupRepository;
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-about::admin.Licenses');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
    /**
     * @param  Rule  $model
     * @param  array  $viewData
     * @return array
     */
    protected function formData($model, array $viewData): array
    {
        return [
            'groups' => $this->licenseGroupRepository->getBySelect(),
        ];
    }
}

