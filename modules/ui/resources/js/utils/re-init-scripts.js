/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------


import wsTabs from 'wezom-standard-tabs';
import lozad from 'js#/modules/lozad';
import dmi from 'js#/dmi';

// ----------------------------------------
// Inner data
// ----------------------------------------

// code

// ----------------------------------------
// Public
// ----------------------------------------

export const reInitScripts = ($container = $(document), awaitAll = true) => {
	$.type = window.jqueryType;
	wsTabs.init($container);
	lozad();
	return dmi.importAll($container, awaitAll);
};
