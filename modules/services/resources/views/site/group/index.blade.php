@extends('cms-ui::layouts.base', ['lightHeader' => true])

@php
    /**
     * @var $group \WezomCms\Services\Models\ServiceGroup
     * @var $service \WezomCms\Services\Models\Service
     * @var $risk \WezomCms\Services\Models\Risk
     */
    $hasServices = count($group->services ?? []);
@endphp

@section('main')
    @widget('ui:heading-section', [
        'bannerImage' => $group->getImageBanner(),
        'title' => SEO::getH1(),
        'text' => $group->sub_title,
        'classes' => 'heading-section--group'
    ])
    <div class="section section--services-list {{ !$hasServices ? 'no-items' : '' }}">
        <div class="container">
            <div class="_grid _spacer _justify-center _lg:justify-start _spacer--md">
                @foreach($group->services ?? [] as $service)
                    <div class="_cell _cell--12 _xs:cell--10 _sm:cell--7 _md:cell--5 _lg:cell--4 service-card-wrapper">
                        <div class="service-card service-card--theme-main-service js-import" data-show-more>
                            <div class="service-card__main">
                                <div class="service-card__top">
                                    <div class="service-card__icon">
                                        @svg($service->icon)
                                    </div>
                                    <div class="service-card__title">
                                        {{ $service->name }}
                                    </div>
                                </div>
                                <div class="service-card__content-wrapper" data-show-more-block="156">
                                    <div class="service-card__content wysiwyg wysiwyg--theme-service"
                                         data-show-more-content>
                                        {!! $service->text !!}
                                    </div>
                                </div>
                                <button type="button" class="service-card__show-more" data-show-more-trigger>
                                    <span class="service-card__text service-card__text--more">
                                        @lang('cms-core::site.show more')
                                    </span>
                                    <span class="service-card__text service-card__text--less">
                                        @lang('cms-core::site.show less')
                                    </span>
                                </button>
                            </div>
                            <div class="_flex _justify-start">
                                @widget('ui:button', [
                                    'component' => 'a',
                                        'attrs' => [
                                        'href' => route('services.inner', ['slug' => $service->slug])
                                    ],
                                    'text' => __('cms-callbacks::site.Order service'),
                                    'modificators' => [
                                        'color-accent',
                                        'size-md',
                                        'uppercase',
                                        'ls-def',
                                        'bold'
                                    ]
                                ])
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-block text-block--inner">
                <div class="_grid _justify-center _spacer _spacer--md">
                    <div class="_cell _cell--12">
                        <div class="text-block__text">
                            <div class="text-block__image">
                                <div class="image image--decorated">
                                    <div class="image__decor"></div>
                                    <img src="{{ url('/images/empty.gif') }}" class="lozad js-import" data-src="{{ $group->getImage() }}" alt="">
                                </div>
                            </div>
                            {!! $group->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--risks">
        <div class="container">
            <div class="risks">
                <div class="risks__title">{{ $group->title_for_risk }}</div>
                <div class="_grid _spacer _spacer--md _justify-center">
                    <div class="_cell _cell--12 _df:cell--10">
                        <div class="risks__content-wrapper">
                            <div class="risks__content">
                                @foreach($group->risks ?? [] as $risk)
                                    <div class="risk">
                                        <div class="risk__icon">
                                            @svg($risk->icon)
                                        </div>
                                        <div class="risk__title">
                                            {{ $risk->name }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @todo на услуге для корпоративных клиентов --}}
    @if($group->parent->isCorporateClient())
        <div class="section section--questions section--questions-service">
            <div class="section__decor" data-scroll-window="#consultation">
                <span>@lang('cms-core::site.or')</span>
                @svg('arrow-bottom-lg')
            </div>
            <div class="container">
                <div class="questions">
                    <div class="_grid _justify-center">
                        <div class="_cell _cell--12 _md:cell--10 _df:cell-8 _lg:cell--6 _flex _flex-column _items-center">
                            <div class="questions__title">
                                @lang('cms-services::site.Hotline title', ['service' => $service->getParentalCaseName()])
                            </div>
                            <a href="tel:{{ $hotline }}" class="questions__link">
                                {{ $hotline }}
                            </a>
                            <div class="questions__button">
                                @widget('ui:button', [
                                    'component' => 'button',
                                    'classes' => 'js-import',
                                    'attrs' => [
                                        'type' => 'button',
                                        'data-mfp' => 'ajax',
                                        'data-mfp-src' => route('callbacks.request-popup')
                                    ],
                                    'text' => __('cms-callbacks::site.send order by contract'),
                                    'modificators' => [
                                        'color-accent',
                                        'size-def',
                                        'uppercase',
                                        'ls-def',
                                        'bold'
                                    ]
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section--consultation" id="consultation">
            <div class="consultation-block">
                <div class="container">
                    <div class="_grid _spacer _spacer--md _justify-center">
                        <div class="_cell--12 _lg:cell--10">
                            <div class="_flex _flex-column _items-center">
                                <div class="consultation-block__title">
                                    @lang('cms-callbacks::site.Consultation send button')
                                </div>
                                <div class="consultation-block__text">
                                    @lang('cms-callbacks::site.Questions description with br')
                                </div>
                            </div>
                            @component('cms-ui::components.form.form-ajax', [
                                'class' => 'js-import',
                                'method' => 'POST',
                                'autocomplete' => 'off',
                                'url' => route('callbacks.consultation'),
                                'id' => uniqid('consultation-')])
                                @csrf
                                <div class="_grid _spacer _spacer--md _df:mb-none _mb-xs">
                                    <div class="_cell--12 _df:cell--4">
                                        @include('cms-ui::components.form.input', [
                                            'name' => 'name',
                                            'attributes' => [ 'placeholder' => __('cms-callbacks::site.Name') ],
                                            'modificators' => ['inactive'],
                                            'classes' => 'form-item--theme-default',
                                            'mode' => 'text'
                                        ])
                                    </div>
                                    <div class="_cell--12 _df:cell--4">
                                        @php
                                            $inputmaskConfig = json_encode((object)[
                                                'mask' => '+38(999)-99-99-999',
                                                'androidMask' => '+38(999)-99-99-999'
                                            ])
                                        @endphp
                                        @include('cms-ui::components.form.input', [
                                            'name' => 'phone',
                                            'attributes' => [
                                                'placeholder' => __('cms-callbacks::site.Phone'),
                                                'data-mask' => $inputmaskConfig,
                                                'class' => 'js-import form-item__control',
                                                'required',
                                                'data-rule-phone'
                                            ],
                                            'modificators' => [],
                                            'component' => 'input',
                                            'classes' => 'form-item--theme-default',
                                            'mode' => 'tel'
                                        ])
                                    </div>
                                    <div class="_cell--12 _df:cell--4">
                                        @include('cms-ui::components.form.input', [
                                            'name' => 'text',
                                            'attributes' => [ 'placeholder' => __('cms-callbacks::site.Time') ],
                                            'modificators' => [],
                                            'component' => 'input',
                                            'classes' => 'form-item--theme-default',
                                            'mode' => 'text'
                                        ])
                                    </div>
                                </div>
                                <div class="_flex _flex-column _items-center _df:mt-sm _df:pt-xxs">
                                    <button type="submit"
                                            class="button button--bold button--ls-def button--uppercase button--color-accent button--size-def">
                                        @lang('cms-callbacks::site.Consultation send button')
                                    </button>
                                    <div class="consultation-block__description">@lang('cms-callbacks::site.Consultation send text')</div>
                                </div>
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{-- @todo на услуге для частных лиц --}}
    @if($group->parent->isPrivatePerson())
        <div class="section section--buy">
            <div class="container">
                <div class="buy-block">
                    <div class="buy-block__title">
                        {{ $group->buyModule->title ?? null }}
                    </div>
                    <div class="buy-block__text">
                        {{ $group->buyModule->sub_title ?? null }}
                    </div>
                    <div class="buy-block__content">
                        <div class="_grid _spacer _spacer--md _justify-center">
                            <div class="_cell _cell--12 _sm:cell--8 _df:cell--6 _lg:cell--4">
                                <div class="buy-card">
                                    <div class="buy-card__main">
                                        <div class="buy-card__top">
                                            @if($group->buyModule->icon_by_online ?? null)
                                                <div class="buy-card__icon">
                                                    @svg($group->buyModule->icon_by_online)
                                                </div>
                                            @endif
                                            <div class="buy-card__title">
                                                @lang('cms-services::site.buy module.online')
                                            </div>
                                        </div>
                                        <div class="buy-card__text">
                                            {{ $group->buyModule->text_by_online ?? null }}
                                        </div>
                                    </div>
                                    <div class="_flex _justify-start _pl-xs">
                                        @widget('ui:button', [
                                        'component' => 'button',
                                        'classes' => 'js-import',
                                        'attrs' => [
                                            'type' => 'button',
                                            'data-mfp' => 'ajax',
                                            'data-mfp-src' => route('callbacks.request-popup')
                                        ],
                                        'text' => __('cms-callbacks::site.Order service'),
                                        'modificators' => [
                                            'color-accent',
                                            'size-md',
                                            'uppercase',
                                            'ls-def',
                                            'bold'
                                            ]
                                        ])
                                    </div>
                                </div>
                            </div>
                            <div class="_cell _cell--12 _sm:cell--8 _df:cell--6 _lg:cell--4">
                                <div class="buy-card">
                                    <div class="buy-card__main">
                                        <div class="buy-card__top">
                                            @if($group->buyModule->icon_by_phone ?? null)
                                                <div class="buy-card__icon">
                                                    @svg($group->buyModule->icon_by_phone)
                                                </div>
                                            @endif
                                            <div class="buy-card__title">
                                                @lang('cms-services::site.buy module.phone')
                                            </div>
                                        </div>
                                        <div class="buy-card__text">
                                            {{ $group->buyModule->text_by_phone ?? null }}
                                        </div>
                                    </div>
                                    <div class="_flex _justify-start _pl-xs">
                                        @widget('ui:button', [
                                            'component' => 'button',
                                            'classes' => 'js-import',
                                            'attrs' => [
                                                'type' => 'button',
                                                'data-mfp' => 'ajax',
                                                'data-mfp-src' => route('callbacks.popup')
                                            ],
                                            'text' => __('cms-callbacks::site.Callback'),
                                            'modificators' => [
                                                'color-accent',
                                                'size-md',
                                                'uppercase',
                                                'ls-def',
                                                'bold'
                                                ]
                                            ])
                                    </div>
                                </div>
                            </div>
                            <div class="_cell _cell--12 _sm:cell--8 _df:cell--6 _lg:cell--4">
                                <div class="buy-card">
                                    <div class="buy-card__main">
                                        <div class="buy-card__top">
                                            @if($group->buyModule->icon_by_office ?? null)
                                                <div class="buy-card__icon">
                                                    @svg($group->buyModule->icon_by_office)
                                                </div>
                                            @endif
                                            <div class="buy-card__title">
                                                @lang('cms-services::site.buy module.office')
                                            </div>
                                        </div>
                                        <div class="buy-card__text">
                                            {{ $group->buyModule->text_by_office ?? null }}
                                        </div>
                                    </div>
                                    <div class="_flex _justify-start _pl-xs">
                                        @widget('ui:button', [
                                            'component' => 'a',
                                            'attrs' => [
                                                'href' => route('contacts')
                                            ],
                                            'text' => __('cms-contacts::site.Contacts'),
                                            'modificators' => [
                                                'color-accent',
                                                'size-md',
                                                'uppercase',
                                                'ls-def',
                                                'bold'
                                            ]
                                        ])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @widget('branches:list')
@endsection
