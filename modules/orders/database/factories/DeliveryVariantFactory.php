<?php

use Faker\Generator as Faker;
use WezomCms\Orders\Models\DeliveryVariant;

$factory->define(DeliveryVariant::class, function (Faker $faker) {
    return [
        'sort' => rand(0, 127),
        'published' => $faker->boolean(80),
        'name' => $faker->word,
        'text' => $faker->realText(),
    ];
});
