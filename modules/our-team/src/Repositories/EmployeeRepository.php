<?php

namespace WezomCms\OurTeam\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\OurTeam\Models\Employee;

class EmployeeRepository extends AbstractRepository
{
    protected function model()
    {
        return Employee::class;
    }

    public function getBySelect($forChoice = true)
    {
        $models = $this->getModelQuery()
            ->published()
            ->with([
                'translations' => function ($query) {
                    $query->orderBy('name');
                }
            ])
            ->orderBy('sort')
            ->get()
            ->pluck('name', 'id')
            ->toArray();

        if($forChoice){
            $choice[0] = __('cms-our-team::admin.Choice');
            $models = $choice + $models;
        }

        return $models;
    }

    public function getByAbout()
    {
        return $this->getModelQuery()
            ->published()
            ->where('for_about_page', true)
            ->orderBy('sort')
            ->get();
    }
}

