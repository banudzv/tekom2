<?php

namespace WezomCms\OurTeam;

use WezomCms\Core\BaseServiceProvider;
use WezomCms\Core\Contracts\PermissionsContainerInterface;
use WezomCms\Core\Contracts\SitemapXmlGeneratorInterface;
use WezomCms\Core\Traits\SidebarMenuGroupsTrait;

class OurTeamServiceProvider extends BaseServiceProvider
{
    use SidebarMenuGroupsTrait;

    /**
     * Dashboard widgets.
     *
     * @var array|string|null
     */
    protected $dashboard = 'cms.our-team.our-team.dashboards';

    /**
     * @param  PermissionsContainerInterface  $permissions
     */
    public function permissions(PermissionsContainerInterface $permissions)
    {
        $permissions->add('employees', __('cms-our-team::admin.Our team'))->withEditSettings();
    }

    public function adminMenu()
    {
        $this->contentGroup()
            ->add(__('cms-our-team::admin.Our team'), route('admin.employees.index'))
            ->data('permission', 'employees.view')
            ->data('icon', 'fa-users')
            ->data('position', 15)
            ->nickname('employees');
    }
}
