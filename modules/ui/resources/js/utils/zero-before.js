/**
 * @module zeroBefore
 * @author Oleg Dutchenko <dutchenko.o.dev@gmail.com>
 * @version 1.0.0
 */

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {Number} number
 * @param {Number} [zero=10]
 * @returns {String}
 */
export const zeroBefore = (number, zero) => {
	// noinspection FallThroughInSwitchStatementJS
	switch (zero) {
		case 1000:
			if (number < 1000) {
				number = '0' + number;
			}
		case 100: // eslint-disable-line no-fallthrough
			if (number < 100) {
				number = '0' + number;
			}
		case 10: // eslint-disable-line no-fallthrough
			if (number < 10) {
				number = '0' + number;
			}
			break;
	}
	return number.toString();
};
