<?php

namespace WezomCms\Services\Models;

use Illuminate\Database\Eloquent\Model;
use WezomCms\Core\ExtendPackage\Translatable;
use WezomCms\Core\Traits\Model\PublishedTrait;

/**
 *
 * @property int $id
 * @property bool $published
 * @property int $service_group_id
 * @property string $icon_by_phone
 * @property string $icon_by_office
 * @property string $icon_by_online
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \WezomCms\Services\Models\ServiceGroup|null $group
 * @property-read \WezomCms\Services\Models\ServiceTranslation $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\WezomCms\Services\Models\ServiceTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service filter($input = array(), $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service orWhereTranslation($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service orWhereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service orderByTranslation($translationField, $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service paginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service published()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service publishedWithSlug($slug, $slugField = 'slug')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service query()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service simplePaginateFilter($perPage = null, $columns = array(), $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereServiceGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereTranslation($translationField, $value, $locale = null, $method = 'whereHas', $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereTranslationLike($translationField, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\WezomCms\Services\Models\Service withTranslation()
 * @mixin \Eloquent
 * @mixin BuyModuleTranslation
 */
class BuyModule extends Model
{
    use Translatable;

    protected $table = 'buy_module_service_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_group_id',
        'icon_by_phone',
        'icon_by_office',
        'icon_by_online',
    ];

    /**
     * Names of the fields being translated in the "Translation" model.
     *
     * @var array
     */
    protected $translatedAttributes = ['title', 'sub_title', 'text_by_phone', 'text_by_office', 'text_by_online'];


    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    public static function create(array $data,int $serviceGroupId)
    {
        try {
            \DB::transaction(function () use ($data, $serviceGroupId) {
                $model = new BuyModule();
                $model->service_group_id = $serviceGroupId;
                $model->icon_by_online = $data['icon_by_online'];
                $model->icon_by_office = $data['icon_by_office'];
                $model->icon_by_phone = $data['icon_by_phone'];
                $model->save();

                foreach($data as $lang => $item){
                    $t = new BuyModuleTranslation();
                    $t->buy_module_id = $model->id;
                    $t->locale = $lang;
                    $t->title = $item['title'] ?? null;
                    $t->sub_title = $item['sub_title'] ?? null;
                    $t->text_by_phone = $item['text_by_phone'] ?? null;
                    $t->text_by_office = $item['text_by_office'] ?? null;
                    $t->text_by_online = $item['text_by_online'] ?? null;
                    $t->save();
                }
            });
        } catch (\Throwable $e) {
            log($e->getMessage());
        }
    }

    public function edit(array $data)
    {
        try {
            \DB::transaction(function () use ($data) {

                $this->icon_by_office = $data['icon_by_office'] ?? null;
                $this->icon_by_phone = $data['icon_by_phone'] ?? null;
                $this->icon_by_online = $data['icon_by_online'] ?? null;
                $this->save();

                foreach ($this->translations ?? [] as $tran){
                    $tran->title = $data[$tran->locale]['title'] ?? null;
                    $tran->sub_title = $data[$tran->locale]['sub_title'] ?? null;
                    $tran->text_by_phone = $data[$tran->locale]['text_by_phone'] ?? null;
                    $tran->text_by_office = $data[$tran->locale]['text_by_office'] ?? null;
                    $tran->text_by_online = $data[$tran->locale]['text_by_online'] ?? null;
                    $tran->save();
                }
            });
        } catch (\Throwable $e) {
            log($e->getMessage());
        }

    }

}


