@extends('cms-core::admin.crud.index')

@php
    /**
     * @var $result \WezomCms\About\Models\AboutUs\Event[]
     */
@endphp

@section('content')

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th width="1%">@massControl($routeName)</th>
                <th>@lang('cms-about::admin.Image')</th>
                <th>@lang('cms-core::admin.layout.Name')</th>
                <th>@lang('cms-core::admin.layout.Text')</th>
                <th>@lang('cms-about::admin.Year')</th>
                <th width="1%" class="text-center">@lang('cms-core::admin.layout.Manage')</th>
            </tr>
            </thead>
            <tbody class="js-sortable"
                   data-params="{{ json_encode(['model' => encrypt($model)]) }}">
            @foreach($result as $obj)
                <tr data-id="{{ $obj->id }}">
                    <td>@massCheck($obj)</td>
                    <td>
                        <a href="{{ url($obj->getImageUrl()) }}" data-fancybox>
                            <img src="{{ url($obj->getImageUrl()) }}" alt="{{ $obj->name }}" height="50">
                        </a>
                    </td>
                    <td>@editResource($obj)</td>
                    <td>@editResource(['obj' => $obj, 'text' => str_limit(strip_tags(html_entity_decode($obj->text)), 50) ?: null])</td>
                    <td>{{$obj->year}}</td>
                    <td>
                        <div class="btn-group list-control-buttons" role="group">
                            @smallStatus($obj)
                            @editResource($obj, false)
                            @deleteResource($obj)
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
