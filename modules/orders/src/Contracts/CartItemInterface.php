<?php

namespace WezomCms\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Collection;

interface CartItemInterface extends Arrayable, Jsonable
{
    /**
     * CartItemInterface constructor.
     * @param  CartInterface  $cart
     * @param  int  $id
     * @param  float  $price
     * @param  float  $quantity
     * @param  array  $options
     */
    public function __construct(
        CartInterface $cart,
        int $id,
        float $price,
        float $quantity,
        array $options = []
    );

    /**
     * @param  CartInterface  $cart
     * @return CartItemInterface
     */
    public function setCart(CartInterface $cart): CartItemInterface;

    /**
     * @return string
     */
    public function getUniqueId(): string;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param  bool  $format
     * @return float|string
     */
    public function getPrice(bool $format = true);

    /**
     * Get sub total price. Without applied discount, promo etc.
     *
     * @param  bool  $format
     * @return float|string
     */
    public function getSubTotal(bool $format = true);

    /**
     * @param  bool  $format
     * @return float|string
     */
    public function getTotal(bool $format = true);

    /**
     * Get total discounted price.
     *
     * @param  bool  $format
     * @return float|string
     */
    public function totalDiscounted(bool $format = true);

    /**
     * @return Collection
     */
    public function getAppliedConditions(): Collection;

    /**
     * @param  string  $conditionName
     * @return CartItemConditionInterface|null
     */
    public function getAppliedCondition(string $conditionName): ?CartItemConditionInterface;

    /**
     * @param  string  $conditionName
     * @return bool
     */
    public function isAppliedCondition(string $conditionName): bool;

    /**
     * @param  float  $quantity
     * @return CartItemInterface
     */
    public function setQuantity(float $quantity): CartItemInterface;

    /**
     * @return float
     */
    public function getQuantity(): float;

    /**
     * @return iterable
     */
    public function getOptions(): iterable;
}
