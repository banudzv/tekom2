<?php

namespace WezomCms\About\Http\Controllers\Site;

use WezomCms\About\Repositories\InsuranceRateRepository;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\ImageFromSettings;

class InsuranceRateController extends SiteController
{
    use ImageFromSettings;

    private $settings;

    private InsuranceRateRepository $newRuleRepository;

    public function __construct(InsuranceRateRepository $newRuleRepository)
    {
        $this->newRuleRepository = $newRuleRepository;
        $this->settings = settings('about.site', []);
    }

    public function index()
    {
        $localSetting = settings('about.insurance_rate', []);

        $pageName = array_get($this->settings, 'name');
        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('about.about-us'));
        $this->seo()
            ->setTitle(array_get($localSetting, 'title'))
            ->setH1(array_get($this->settings, 'h1'))
            ->setPageName($pageName)
            ->setDescription(array_get($localSetting, 'description'))
            ->metatags()
            ->setKeywords(array_get($localSetting, 'keywords'));
        $rules = $this->newRuleRepository->getByFront();


        return view('cms-about::site.insurance-rate', [
            'banner' => $this->getBannerUrl('about.page'),
            'description' => array_get($this->settings, 'description'),
            'rules' => $rules
        ]);
    }
}
