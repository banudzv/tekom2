<?php

namespace WezomCms\OurTeam\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Core\Settings\AdminLimit;
use WezomCms\Core\Settings\Fields\AbstractField;
use WezomCms\Core\Settings\MetaFields\SeoFields;
use WezomCms\Core\Settings\MetaFields\SeoText;
use WezomCms\Core\Settings\MultilingualGroup;
use WezomCms\Core\Settings\SiteLimit;
use WezomCms\Core\Traits\SettingControllerTrait;
use WezomCms\OurTeam\Http\Requests\Admin\EmployeeRequest;
use WezomCms\OurTeam\Models\Employee;

class EmployeesController extends AbstractCRUDController
{
    use SettingControllerTrait;

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-our-team::admin';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.employees';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = EmployeeRequest::class;

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-our-team::admin.Our team');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param $obj
     * @param  FormRequest  $request
     * @return array|mixed
     */
    protected function fill($obj, FormRequest $request): array
    {
        $data = parent::fill($obj, $request);

        $data['phones'] = array_filter($request->get('phones', []));

        return $data;
    }

    public function destroy($id)
    {
        $obj = $this->model()::findOrFail($id);

        if($obj->branch){
            flash(__('cms-our-team::admin.Do not can delete employ'))->error();

            return redirect()->route($this->makeRouteName('index'));
        }

        return parent::destroy($id);
    }


    /**
     * @return array|AbstractField[]|MultilingualGroup[]
     * @throws \Exception
     */
    protected function settings(): array
    {
        return [
            SiteLimit::make()->setName(__('cms-our-team::admin.Employees limit at page')),
            SeoFields::make('Our team', [SeoText::make()->setName(__('cms-our-team::admin.Text'))->setSort(3)]),
            AdminLimit::make(),
        ];
    }
}
