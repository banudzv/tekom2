<?php

namespace WezomCms\Reports\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use WezomCms\Core\Contracts\Filter\FilterFieldInterface;
use WezomCms\Core\Foundation\Helpers;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Reports\Http\Requests\Admin\CategoryRequest;
use WezomCms\Reports\ModelFilters\CategoryFilter;
use WezomCms\Reports\Models\Category;

class CategoryController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Indicates whether to use pagination.
     *
     * @var bool
     */
    protected $paginate = false;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-reports::admin.categories';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.report-categories';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = CategoryRequest::class;

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-reports::admin.Categories report');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }

    /**
     * @param $result
     * @param array $viewData
     * @return array
     */
    protected function indexViewData($result, array $viewData): array
    {

        $request = app('request');

        $presentFilterInputs = array_filter(
            (new CategoryFilter($this->model()::query(), $request->all()))->getFields(),
            function (FilterFieldInterface $filterField) use ($request) {
                $name = $filterField->getName();

                return $name !== 'per_page' && $request->get($name) !== '' && $request->get($name) !== null;
            }
        );

        if (count($presentFilterInputs)) {
            // Render groups as one level
            $firstLevel = $result->all();
            $result = [null => $result->all()];
        } else {
            // Multidimensional tree
            $result = Helpers::groupByParentId($result);
            $firstLevel = $result[null] ?? [];
        }

        $limit = $this->getLimit($request);

        // Paginate only first level items
        $pagination = new LengthAwarePaginator(
            $firstLevel,
            count($firstLevel),
            $limit,
            null,
            ['path' => $request->url(), 'query' => $request->query()]
        );

        $paginatedResult = array_slice($firstLevel, ($pagination->currentPage() - 1) * $limit, $limit);

        return ['result' => $result, 'paginatedResult' => $paginatedResult, 'pagination' => $pagination];
    }

    /**
     * @param Category $obj
     * @param array $viewData
     * @return array
     */
    protected function createViewData($obj, array $viewData): array
    {
        return [
            'tree' => $this->buildTree(),
        ];
    }

    /**
     * @param Category $obj
     * @param array $viewData
     * @return array
     */
    protected function editViewData($obj, array $viewData): array
    {
        return [
            'tree' => $this->buildTree($obj->id),
        ];
    }

    /**
     * @param array $groupedItems
     * @param int $maxDepth
     * @param null $parentId
     * @param int $depth
     * @return array
     */
    private function generateTree(array $groupedItems, int $maxDepth, $parentId = null, $depth = 0)
    {
        $result = [];

        if ($depth + 1 >= $maxDepth) {
            return $result;
        }

        $elements = isset($groupedItems[$parentId]) ? $groupedItems[$parentId] : [];

        foreach ($elements as $item) {
            $result[$item->id] = str_repeat('&nbsp;', $depth * 4) . $item->name;

            $result += $this->generateTree($groupedItems, $maxDepth, $item->id, $depth + 1);
        }

        return $result;
    }

    /**
     * @param int|null $id
     * @return array
     */
    private function buildTree(int $id = null)
    {
        $tree = [
            '' => __('cms-catalog::admin.categories.Root'),
        ];

        $query = Category::select('id', 'parent_id');

        if ($id) {
            $query->where('id', '<>', $id);
        }

        $items = $query
            ->orderBy('sort')
            ->latest('id')
            ->get();

        $groupedItems = Helpers::groupByParentId($items);

        $maxDepth = config('cms.reports.reports.max_depth', 100);

        $tree += $this->generateTree($groupedItems, $maxDepth);

        return $tree;
    }
}
