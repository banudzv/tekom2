@php
    /**
     * @var $menu array|\WezomCms\Menu\Models\Menu[][]
     * @var $maxDepth int
     */
@endphp
<nav class="footer-menu _px-df">
    <ul class="footer-menu__items">
        @if(isset($menu[null]))
            @foreach($menu[null] as $item)
                <li class="footer-menu__item">
                    @if($mode = $item->activeMode())
                        @if($mode === \WezomCms\Menu\Models\Menu::MODE_SPAN)
                            <span class="footer-menu__link is-active">{{ $item->name }}</span>
                        @else
                            <a href="{{ url($item->url) }}" class="footer-menu__link is-active">{{ $item->name }}</a>
                        @endif
                    @else
                        <a href="{{ url($item->url) }}" class="footer-menu__link">{{ $item->name }}</a>
                    @endif
                    @if(count($item->children))
                        <ul class="footer-menu__subitems">
                            @foreach($item->children as $child)
                                <li class="footer-menu__subitem">
                                    @if($mode = $child->activeMode())
                                        @if($child === \WezomCms\Menu\Models\Menu::MODE_SPAN)
                                            <span class="footer-menu__sublink is-active">{{ $child->name }}</span>
                                        @else
                                            <a href="{{ url($child->url) }}" class="footer-menu__sublink is-active">{{ $child->name }}</a>
                                        @endif
                                    @else
                                        <a href="{{ url($child->url) }}" class="footer-menu__sublink">{{ $child->name }}</a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        @endif
    </ul>
</nav>
