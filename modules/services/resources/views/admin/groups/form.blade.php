@php
    /**
     * @var $viewPath string
     */
    $tabs = [
        __('cms-core::admin.layout.Main data') => $viewPath . '.tabs.main',
        __('cms-core::admin.tabs.seo') => $viewPath . '.tabs.seo',
        __('cms-core::admin.tabs.relation element') => $viewPath . '.tabs.relations',
    ];

    if($obj->parent && $obj->parent->isPrivatePerson()){
        $tabs[__('cms-services::admin.Buy module')] = $viewPath . '.tabs.buy-module';
    }


@endphp

<div class="row">
    <div class="col-lg-7">
        <div class="card mb-3">
            <div class="card-body">
                @tabs($tabs)
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('published', __('cms-core::admin.layout.Published')) !!}
                    {!! Form::status('published') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('is_top', __('cms-services::admin.Is top')) !!}
                    {!! Form::status('is_top') !!}
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('type'), __('cms-services::admin.Type')) !!}
                    <div class="input-group">
                        {!! Form::select('type', $obj::getTypes(), old('type', $obj->type), ['class' => 'js-select2']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label(str_slug('icon'), __('cms-core::admin.layout.Icon')) !!}
                    <div class="input-group">
                        {!! Form::select('icon', \WezomCms\Core\Foundation\Icon::getForSelect(), old('icon', $obj->icon), ['class' => 'js-select2']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('parent_id', __('cms-services::admin.Parent')) !!}
                    {!! Form::select('parent_id', $tree, old('parent_id', $obj->parent_id ?: request()->get('parent_id')), ['class' => 'js-select2']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('template', __('cms-services::admin.Template')) !!}
                    {!! Form::select('template', config('cms.services.services.templates'), old('template', $obj->template ?: request()->get('template')), ['class' => 'js-select2']) !!}
                </div>
                @if($obj->template == 'new-index')
                    <div class="form-group">
                        {!! Form::label('image_text_top', __('cms-services::admin.Image for text top')) !!}
                        {!! Form::imageUploader('image_text_top', $obj, route($routeName . '.delete-image', [$obj->id, 'field' => 'image_text_top'])) !!}
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label('image', __('cms-services::admin.Image')) !!}
                    {!! Form::imageUploader('image', $obj, route($routeName . '.delete-image', $obj->id)) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('image_banner', __('cms-services::admin.Image banner')) !!}
                    {!! Form::imageUploader('image_banner', $obj, route($routeName . '.delete-image', ['id' => $obj->id, 'field' => 'image_banner'])) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('image_for_block', __('cms-services::admin.Image for block')) !!}
                    {!! Form::imageUploader('image_for_block', $obj, route($routeName . '.delete-image', ['id' => $obj->id, 'field' => 'image_for_block'])) !!}
                </div>
            </div>
        </div>
    </div>
</div>
