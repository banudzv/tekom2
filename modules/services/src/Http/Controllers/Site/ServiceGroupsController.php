<?php

namespace WezomCms\Services\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use WezomCms\Core\Foundation\JsResponse;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Services\Models\ServiceGroup;
use WezomCms\Services\Repositories\ServiceGroupRepository;

class ServiceGroupsController extends SiteController
{
    /**
     * @var ServiceGroupRepository
     */
    private $serviceGroupRepository;

    private $hotline;

    public function __construct(ServiceGroupRepository $serviceGroupRepository)
    {
        $this->serviceGroupRepository = $serviceGroupRepository;

        $siteSettings = settings('contacts.page-settings', []);
        $this->hotline = array_get($siteSettings, 'hotline');
    }

    /**
     * страница для базовой категории
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function innerBase($slug)
    {
        /** @var ServiceGroup $group */
        $group = $this->serviceGroupRepository->getBySlugForFront($slug,[
            'translation',
            'children',
            'children.services'
        ]);

        $template = $group->template ? $group->template : 'base-index';

        $this->setLangSwitchers($group, 'service-group-base.inner');

        $settings = settings('services.site', []);

        // Breadcrumbs
        $this->addBreadcrumb($group->name, $group->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($group->title)
            ->setH1($group->h1)
            ->setPageName($group->name)
            ->setDescription($group->description)
            ->metatags()
            ->setKeywords($group->keywords);

        // Render

        return view('cms-services::site.group.'.$template, [
            'group' => $group,
            'hotline' => $this->hotline
        ]);
    }

    /**
     * страница для категории
     * @param $baseSlug
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inner($baseSlug, $slug)
    {
        /** @var ServiceGroup $baseGroup */
        $baseGroup = ServiceGroup::query()
            ->publishedWithSlug($baseSlug)
            ->firstOrFail();

        /** @var ServiceGroup $group */
        $group = $this->serviceGroupRepository->getBySlugForFront($slug,[
            'translations',
            'parent',
            'services' => function($query){
                $query->where('published', true)->orderBy('sort');
            },
            'risks'
        ]);
        $group->parent()->with([
            'translations'=>
            function($query){
                $query->whereLocale('uk');
        }
                ])->firstOrFail();
        $this->setLangSwitchers($group, 'service-group.inner',[
            'not-model'=> [

            ],
            'slug' => 'slug',
            'baseSlug' => 'group_slug',
        ]);

        $settings = settings('services.site', []);

        // Breadcrumbs
        $this->addBreadcrumb(array_get($settings, 'name'), route('services'));
        $this->addBreadcrumb($group->parent->name, $group->parent->getFrontUrl());
        $this->addBreadcrumb($group->name, $group->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($group->title)
            ->setH1($group->h1)
            ->setPageName($group->name)
            ->setDescription($group->description)
            ->metatags()
            ->setKeywords($group->keywords);

        // Render
        return view('cms-services::site.group.index', [
            'group' => $group,
            'hotline' => $this->hotline
        ]);
    }

    public function ajaxMoreServices(Request $request)
    {
        $group = ServiceGroup::query()
            ->published()
            ->where('id', $request['id'])
            ->first();

        $groups = $this->serviceGroupRepository->getMoreItems($request['id'], $request['count'],[
            'translations', 'services'
        ]);

        $counts = resolve(ServiceGroupRepository::class)->countChildren($request['id']);
        $more = ($request['count'] + ServiceGroup::DEFAULT_ON_PAGE) < $counts;

        return response([
            'current_count' => $request['count'] + $groups->count(),
            'more' => $more,
            'html' => view('cms-services::site.group.partials.service-groups' , [
                'childs' => $groups,
                'groupSlug' => $group->slug
            ])->render()
        ]);
    }
}
