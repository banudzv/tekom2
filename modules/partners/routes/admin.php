<?php

use WezomCms\Partners\Http\Controllers\Admin\CategoryController;
use WezomCms\Partners\Http\Controllers\Admin\PartnersController;

Route::adminResource('partners',PartnersController::class)->settings();
Route::adminResource('partner-categories',CategoryController::class);
