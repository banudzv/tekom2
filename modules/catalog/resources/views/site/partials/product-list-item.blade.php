@php
    /**
     * @var $product WezomCms\Catalog\Models\Product
     */
@endphp
<div>
    <a href="{{ $product->getFrontUrl() }}">
        <img src="{{ $product->getImageUrl() }}" alt="{{ $product->image_alt }}" title="{{ $product->image_title }}">
    </a>
    @if($product->has_flag)
        @foreach($product->flags as $flag)
            <div class="{{ $flag['color'] }}">{{ $flag['text'] }}</div>
        @endforeach
    @endif

    <div>
        @if($product->sale && $product->expires_at > now())
            @lang('cms-catalog::site.До конца акции')
            {{ $product->expires_at->format('Y.m.d H:i:s') }}
        @endif
        @foreach($product->variations as $variant)
            <a href="{{ $variant->getFrontUrl() }}" class="{{ $product->id === $variant->id ? 'is-current' : '' }}"
               title="{{ $variant->color->name }}">
                <span style="width: 20px; height:20px; display:block; background-color: {{ $variant->color->color }}"></span>
            </a>
        @endforeach
    </div>
    <div>
        <a href="{{ $product->getFrontUrl() }}">{{ $product->name }}</a>

        @if($product->availableForPurchase())
            <div>@money($product->cost, true)</div>

            @if($product->sale && $product->old_cost > $product->cost)
                <div>
                    <span>@money($product->old_cost)</span>
                    <small>{{ money()->siteCurrencySymbol() }}</small>
                </div>
            @endif
        @endif

        <div>
            @widget('comparison:product-button', ['product' => $product])

            @if($product->availableForPurchase())
                <button class="{{ $product->in_cart ? 'in-cart' : '' }}"
                        data-cart-button="{{ $product->id }}"
                        title="{{ $product->in_cart ? __('cms-catalog::site.Открыть корзину') : __('cms-catalog::site.Добавить в корзину') }}"
                        data-cart-target="{{ json_encode(['action' => 'add', 'id' => $product->id, 'count' => $product->minCountForPurchase()]) }}">{{ $product->in_cart ? __('cms-catalog::site.в корзине') : __('cms-catalog::site.купить') }}</button>
            @else
                <button disabled="disabled">@lang('cms-catalog::site.Нет в наличии')</button>
            @endif

{{--            @widget('favorites:product-list-button', ['favorable' => $product, 'removeFromFavorites' => $removeFromFavorites ?? false])--}}
        </div>
    </div>
    @if($loop->last && isset($ns) && $result->count() == $limit)
        @php
            $route = route('catalog.category.filter', [
                'slug' => $product->category->slug,
                'id' => $product->category->id,
                'filter' => $ns . '=1'
            ]);
        @endphp
        <a class="prod-i-more" href="{{ $route }}">
            @svg('common', 'arrow-loader', 55)
            @lang('cms-catalog::site.Показать еще')
        </a>
    @endif
</div>
