<?php

namespace WezomCms\Services\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Http\Requests\Admin\ObjectInsuranceRequest;
use WezomCms\Services\Models\ObjectInsurance;

class ObjectInsuranceController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = ObjectInsurance::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-services::admin.object-insurances';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.object-insurances';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = ObjectInsuranceRequest::class;

    /**
     * ServicesController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-services::admin.Object insurances');
    }

    /**
     * @param  Builder  $query
     * @param  Request  $request
     */
    protected function selectionIndexResult($query, Request $request)
    {
        $query->orderBy('sort');
    }
}


