<?php

namespace WezomCms\Orders\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;
use WezomCms\Orders\Enums\DeliveryTypes;

class DeliveryRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            ['name' => 'required|string|max:255'],
            [
                'published' => 'required',
                'type' => 'required|in:' . implode(',', DeliveryTypes::getValues()),
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            ['name' => __('cms-orders::admin.deliveries.Name')],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'type' =>  __('cms-orders::admin.deliveries.Delivery type')
            ]
        );
    }
}
