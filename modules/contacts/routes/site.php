<?php

Route::namespace('WezomCms\\Contacts\\Http\\Controllers\\Site')
    ->group(function () {
        Route::get('contacts', 'ContactsController@index')->name('contacts');

        Route::middleware([
            \WezomCms\Core\Http\Middleware\CheckForSpam::class,
            'form_throttle'
        ])
            ->post('contacts/form', 'ContactsController@form')
            ->name('contacts.form');
    });
