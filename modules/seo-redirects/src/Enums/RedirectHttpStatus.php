<?php

namespace WezomCms\SeoRedirects\Enums;

use BenSampo\Enum\Enum;

final class RedirectHttpStatus extends Enum
{
    public const STATUS_301 = 301;
    public const STATUS_302 = 302;
    public const STATUS_303 = 303;
    public const STATUS_307 = 307;

    public static function toSelectArray(): array
    {
        return [
            '' => __('cms-core::admin.layout.Not set'),
            static::STATUS_301 => __('cms-seo-redirects::admin.301 Moved Permanently «перемещено навсегда»'),
            static::STATUS_302 => __('cms-seo-redirects::admin.302 Moved Temporarily «перемещено временно»'),
            static::STATUS_303 => __('cms-seo-redirects::admin.303 See Other смотреть другое'),
            static::STATUS_307 => __('cms-seo-redirects::admin.307 Temporary Redirect «временное перенаправление»'),
        ];
    }
}
