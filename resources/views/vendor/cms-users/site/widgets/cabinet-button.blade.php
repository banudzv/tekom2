@php
    /**
     * @var $user \WezomCms\Users\Models\User|null
     */
@endphp
@auth
    <a href="{{ route('cabinet') }}" title="@lang('cms-users::site.Перейти в личный кабинет')">
        @lang('cms-users::site.Мой кабинет')
    </a>
    <div>
        <div>@lang('cms-users::site.Здравствуйте'),</div>
        <strong>{{ $user->full_name }}</strong>!
    </div>
@else
    <div class="js-import" data-mfp="ajax" data-mfp-src="{{ route('auth.login-form') }}">
        @lang('cms-users::site.Вход / Регистрация')
    </div>
    <div>
        @lang('cms-users::site.Для доступа в личный кабинет'),
        <span class="js-import" data-mfp="ajax" data-mfp-src="{{ route('auth.login-form') }}">
            @lang('cms-users::site.авторизуйтесь')
        </span>
    </div>
@endauth
