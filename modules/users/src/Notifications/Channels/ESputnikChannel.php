<?php

namespace WezomCms\Users\Notifications\Channels;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Notifications\Notification;
use WezomCms\Users\Exceptions\ESputnikException;

class ESputnikChannel
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string|null
     */
    private $from;

    /**
     * ESputnikChannel constructor.
     * @param  string  $user
     * @param  string  $password
     * @param  string  $from
     */
    public function __construct(string $user, string $password, string $from)
    {
        $this->client = new Client([
            'base_uri' => 'https://esputnik.com.ua/api/',
            RequestOptions::HEADERS => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            RequestOptions::AUTH => [$user, $password],
            RequestOptions::CONNECT_TIMEOUT => 2,
        ]);

        $this->from = $from;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  Notification  $notification
     * @return void
     * @throws ESputnikException
     */
    public function send($notifiable, Notification $notification)
    {
        $to = $notifiable->routeNotificationFor('eSputnik');

        $this->request('POST', 'v1/message/sms', [
            'phoneNumbers' => $to,
            'text' => $notification->toESputnik($notifiable),
        ]);
    }

    /**
     * @param  string  $method
     * @param  string  $action
     * @param  array  $data
     * @throws ESputnikException
     */
    protected function request(string $method, string $action, $data = [])
    {
        $data['from'] = $this->from;

        try {
            $response = $this->client->request($method, $action, [
                RequestOptions::JSON => $data
            ]);

            $body = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

            if (array_get($body, 'results.status') !== 'OK') {
                logger()->error('Bad response', ['response' => $body]);
                throw new \Exception('Failed to send SMS');
            }
        } catch (\Throwable $e) {
            report($e);
            throw new ESputnikException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
