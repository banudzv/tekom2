<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCounterTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('counters', function (Blueprint $table) {
			$table->dropColumn('position');
			$table->dropColumn('active');
			$table->integer('sort')->default(0);
			$table->boolean('published')->default(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('counters', function (Blueprint $table) {
			$table->dropColumn('sort');
			$table->dropColumn('published');
			$table->integer('position')->default(0);
			$table->boolean('active')->default(true);
		});
	}
}
