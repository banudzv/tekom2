@extends('cms-users::site.layouts.cabinet')
@php
/**
 * @var $passwordMinLength int
 */
@endphp
@section('content')
    <div class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            @widget('cabinet-menu')
        </div>
        <div class="grid">
            <div class="gcell">
                <form class="js-import" method="post" action="{{ route('cabinet.save-new-password') }}">
                    <div>
                        <input type="password"
                               name="old_password"
                               min="{{ $passwordMinLength }}"
                               required="required"
                               placeholder="@lang('cms-users::site.Введите текущий пароль') *">
                    </div>
                    <div>
                        <input type="password"
                               name="password"
                               min="{{ $passwordMinLength }}"
                               required="required"
                               placeholder="@lang('cms-users::site.Введите новый пароль') *">
                    </div>
                    <div>
                        <input type="password"
                               name="password_confirmation"
                               min="{{ $passwordMinLength }}"
                               required="required"
                               placeholder="@lang('cms-users::site.Повторите новый пароль') *">
                    </div>
                    <button type="submit">@lang('cms-users::site.Сохранить')</button>
                </form>
            </div>
            <div class="gcell">
                @widget('cabinet-socials')
                @widget('cabinet-submenu')
            </div>
        </div>
    </div>
@endsection
