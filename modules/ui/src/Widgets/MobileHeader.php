<?php

namespace WezomCms\Ui\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;

class MobileHeader extends AbstractWidget
{
	/**
	 * View name.
	 *
	 * @var string|null
	 */
	protected $view = 'cms-ui::widgets.header-mobile';
}
