<?php

namespace WezomCms\Partners\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Core\Traits\LocalizedRequestTrait;

class PartnerRequest extends FormRequest
{
    use LocalizedRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->localizeRules(
            [
                'name' => 'required|string|max:255',
                'description' => 'nullable|string|max:255',
            ],
            [
                'published' => 'required',
                'image' => 'nullable|image',
                'category_id' => 'required|int',
            ]
        );
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->localizeAttributes(
            [
                'name' => __('cms-partners::admin.Name'),
                'description' => __('cms-partners::admin.Description'),
            ],
            [
                'published' => __('cms-core::admin.layout.Published'),
                'image' => __('cms-partners::admin.Image'),
                'category_id' => __('cms-partners::admin.Category')
            ]
        );
    }
}
