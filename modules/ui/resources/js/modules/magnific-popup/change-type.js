/**
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import { uniqueId } from 'js#/utils/unique-id';
import { globalData } from 'js#/globals/global-data';
import { SiteModuleDynamicImport } from 'assetsSite#/js/site-module-dynamic-imports';

// ----------------------------------------
// Public
// ----------------------------------------

/**
 * @param {jQuery} $element
 * @param {jQuery} $content
 * @param {string} initedKey
 * @returns {jQuery} $group
 */
export const ajax2inline = ($element, $content, initedKey) => {
	let id = uniqueId('id-');
	let group = $element.data('mfp-change-group');
	let $group = $(`[data-mfp="ajax"][data-mfp-change-group="${group}"]`);
	let $container = $('#' + globalData.magnificPopup.changeTypeId);

	$content
		.clone(false)
		.prop('id', id)
		.appendTo($container);

	$group.each((i, el) => {
		$(el)
			.attr('data-mfp', 'inline')
			.attr('data-mfp-src', '#' + id);
	});

	$element.removeInitedKey(initedKey);
	SiteModuleDynamicImport.instance().importAll($container);
	return $group;
};
