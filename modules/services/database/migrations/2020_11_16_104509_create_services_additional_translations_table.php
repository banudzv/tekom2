<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesAdditionalTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_additional_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('additional_id');
            $table->string('locale')->index();
            $table->string('name');
            $table->mediumText('text')->nullable();

            $table->unique(['additional_id', 'locale']);
            $table->foreign('additional_id')
                ->references('id')
                ->on('service_additionals')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_additional_translations');
    }
}

