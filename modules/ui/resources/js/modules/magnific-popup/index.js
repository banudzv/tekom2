'use strict';

/**
 *
 * @module
 */

// ----------------------------------------
// Imports
// ----------------------------------------

import $ from 'jquery';
import 'custom-jquery-methods/fn/has-inited-key';
import 'custom-jquery-methods/fn/get-my-elements';
import 'magnific-popup/dist/jquery.magnific-popup.min';
import { globalData } from 'js#/globals/global-data';
import { extend } from './mfp-extend';
import { reInitScripts } from 'js#/utils/re-init-scripts';

// ----------------------------------------
// Public
// ----------------------------------------

export default function ($elements) {
	mfp($elements);
}

/**
 * @param $elements
 */
export function mfp ($elements) {
	if (globalData.magnificPopup.l18n) {
		$.ajax({
			url: globalData.magnificPopup.l18n,
			dataType: 'json',
			method: 'get'
		}).done(function (translations) {
			extend(translations);
		}).always(function () {
			_mfp($elements);
		});
	} else {
		_mfp($elements);
	}
}

// ----------------------------------------
// Private
// ----------------------------------------

function classAnim ($el) {
	if (!$el) return {};

	const classes = [];
	const anim = $el.data('mfp-show');

	if (anim && anim.length) {
		classes.push('mfp-animate-' + anim.replace(' ', ' mfp-animate-'));
	}

	return classes.length ? { mainClass: classes.join(' ') } : {};
}

function _mfp ($elements) {
	_ajax($elements.filter('[data-mfp="ajax"]'));
	_iframe($elements.filter('[data-mfp="iframe"]'));
	_inline($elements.filter('[data-mfp="inline"]'));
	_gallery($elements.filter('[data-mfp="gallery"]'));

	$('body').on('click', '[data-mfp-close], .mfp-close', () => {
		$.magnificPopup.close();
	});
}

/**
 * @param {jQuery} $elements
 * @private
 */
function _ajax ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);
		const initedKey = 'mfpAjaxIsInitialized';
		if ($element.hasInitedKey(initedKey)) {
			return true;
		}
		// const changeTypeMethod = $element.data('mfp-change-type');
		const config = $.extend(true, {
			type: 'ajax',
            closeMarkup: `
                <button data-mfp-close title="%title%" type="button" class="mfp-close">
                    <svg width="20" height="19" viewBox="0 0 20 19" xmlns="http://www.w3.org/2000/svg">
						<path d="M9.58796 9.43923L1.0852 0.936461C0.971886 0.823112 0.971886 0.639373 1.0852 0.526025C1.19855 0.412712 1.38229 0.412712 1.49564 0.526025L9.9984 9.02879L18.5012 0.526025C18.6165 0.414671 18.8002 0.417864 18.9116 0.533172C19.0202 0.64565 19.0202 0.823983 18.9116 0.936461L10.4088 9.43923L18.9116 17.942C19.0269 18.0534 19.0301 18.2371 18.9187 18.3524C18.8073 18.4677 18.6236 18.4709 18.5083 18.3595C18.5058 18.3572 18.5035 18.3548 18.5012 18.3524L9.9984 9.84966L1.49564 18.3524C1.38033 18.4638 1.19659 18.4606 1.0852 18.3453C0.97653 18.2328 0.97653 18.0545 1.0852 17.942L9.58796 9.43923Z"
							  />
						<path d="M1.77843 0.243112C1.50888 -0.0263499 1.07194 -0.0263499 0.802395 0.243112C0.532932 0.51266 0.532843 0.949686 0.802305 1.21923L9.02227 9.4392L0.797478 17.664C0.539045 17.9315 0.539082 18.3557 0.797504 18.6232C1.06235 18.8974 1.49932 18.905 1.77354 18.6401L1.77851 18.6353L9.99839 10.4153L18.2164 18.6334C18.22 18.637 18.2251 18.6422 18.2312 18.648C18.5054 18.9121 18.9418 18.9042 19.2064 18.6303C19.4703 18.3571 19.4637 17.9223 19.1925 17.6572L10.9745 9.4392L19.1994 1.21436C19.4577 0.946873 19.4577 0.522753 19.1993 0.255267C18.9345 -0.0188892 18.4975 -0.0265495 18.2233 0.238266L18.2183 0.243114L9.99839 8.46308L1.77843 0.243112Z"
							  />
					</svg>
                </button>
            `,
			mainClass: 'mfp-animate-zoom-in',
			callbacks: {
				elementParse (item) {
					if (item.el !== undefined) {
						const {
							url,
							type = 'GET',
							data = {}
						} = item.el.data();
						this.st.ajax.settings = { url, type, data, dataType: 'html' };
					}
				},
				ajaxContentAdded () {
					this.contentContainer.find('[data-mfp-close]').on('click', () => {
						this.close();
					});

					if (this.contentContainer) {
						reInitScripts(this.contentContainer, true, false);
					}
				},
				close: function () {
					$(document).trigger('mfp-close');
				}
			}
		}, {});

		$element.magnificPopup(config);
	});
}

/**
 * @param {jQuery} $elements
 * @private
 */
function _iframe ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);
		if ($element.hasInitedKey('mfpIframe-initialized')) {
			return true;
		}

		const config = $.extend(true, {
			mainClass: 'mfp-animate-zoom-in',
			type: 'iframe'
		});

		$element.magnificPopup(config);
	});
}

/**
 * @param {jQuery} $elements
 * @private
 */
function _inline ($elements) {
	$elements.each((i, el) => {
		const $element = $(el);

		if ($element.hasInitedKey('mfpInline-initialized')) {
			return true;
		}

		const config = $.extend(true, {
			mainClass: 'mfp-animate-zoom-in',
			type: 'inline'
		}, classAnim($element));

		$element.magnificPopup(config);
	});
}

/**
 * @param {jQuery} $elements
 * @private
 */
function _gallery ($elements) {
	$elements.each((i, el) => {
		let $element = $(el);
		if ($element.hasInitedKey('mfpGallery-initialized')) {
			return true;
		}

		const config = $.extend(true, {
			type: 'image',
			delegate: '[data-mfp-src]',
			mainClass: 'mfp-animate-zoom-in',
			gallery: {
				enabled: true,
				preload: [0, 2],
				navigateByImgClick: true,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>'
			}
		}, {});

		$element.magnificPopup(config);
	});
}
