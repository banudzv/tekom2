@langTabs
    @include('cms-core::admin.partials.form-meta-inputs', compact('obj', 'locale'))
    <div class="form-group">
        {!! Form::label($locale . '[seo_text]', __('cms-core::admin.seo.Seo text')) !!}
        {!! Form::textarea($locale . '[seo_text]', old($locale . '.seo_text', $obj->translateOrNew($locale)->seo_text), ['class' => 'js-wysiwyg', 'data-lang' => $locale]) !!}
    </div>
@endLangTabs
