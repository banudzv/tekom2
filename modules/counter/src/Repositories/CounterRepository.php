<?php

namespace WezomCms\Counter\Repositories;

use WezomCms\Core\Repositories\AbstractRepository;
use WezomCms\Counter\Models\Counter;

class CounterRepository extends AbstractRepository
{
    protected function model()
    {
        return Counter::class;
    }
}
