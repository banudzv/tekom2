@php
    /**
     * @var $classes string|null
     */

    $classes = $classes ?? '';
    $bottomLinks = $bottomLinks ?? false;
    $blogPanel = $blogPanel ?? false;
@endphp
<div class="heading-section js-import lozad {{ $classes }}" data-background-image="{{ $bannerImage }}">
    <div class="container">
        <div class="_flex _justify-center _df:justify-start">
            @widget('ui:breadcrumbs')
        </div>
        <div class="heading-section__content">
            <h1 class="heading-section__title">
                {!! $title !!}
            </h1>
            @if(isset($text))
                <div class="heading-section__text">
                    {!! $text !!}
                </div>
            @endif
            @if(isset($button))
                <div class="heading-section__button">
                    @widget('ui:button', [
                        'component' => $button->component ?? 'a',
                        'attrs' => $button->attrs ?? [],
                        'text' => $button->text,
                        'classes' => $button->classes ?? '',
                        'modificators' => [
                            'color-accent',
                            'size-def',
                            'uppercase',
                            'ls-def',
                            'bold'
                        ]
                    ])
                </div>
            @endif
            @if($bottomLinks)
                <div class="heading-section__bottom _spacer _spacer--md">
                    @foreach($bottomLinks ?? [] as $group)
                        <div>
                            {{-- @todo добавлять класс is-active активной ссылке и убирать href --}}
                            @widget('ui:button', ['component' => 'a', 'attrs' => ['href' => route('article-groups', ['slug' => $group->slug])], 'text' => $group->name, 'modificators' => ['color-accent-3-light', 'size-sm']])
                        </div>
                    @endforeach
                </div>
            @endif
            @if($blogPanel)
                <div class="heading-section__bottom">
                    <div class="_flex _flex-wrap _justify-between">
                        @include('cms-ui::widgets.date-block', ['date' => $blogPanel->date])
                        <div>
                            @foreach($blogPanel->tags as $tag)
                                <a class="tag" href="{{ $tag->url }}">
                                    {{ $tag->name }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
