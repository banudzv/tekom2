@extends('cms-ui::layouts.main')

@php
    /**
     * @var $userInfo array
     */
@endphp

@section('content')
    <div style="margin-top: 100px" class="container">
        <h1>{{ SEO::getH1() }}</h1>
        <div>
            <div>@lang('cms-orders::site.Шаг первый')</div>

            <div>
                <div>@lang('cms-orders::site.Контактные данные')</div>
                @if(Auth::check())
                    <form class="js-import" method="post" action="{{ route('checkout.save-step1') }}">
                        @csrf
                        <div>
                            <input type="text" name="name"
                                   value="{{ array_get($userInfo, 'name') }}"
                                   placeholder="@lang('cms-orders::site.Имя') *"
                                   inputmode="text"
                                   spellcheck="true" required="required">
                        </div>
                        <div>
                            <input type="text" name="surname"
                                   value="{{ array_get($userInfo, 'surname') }}"
                                   placeholder="@lang('cms-orders::site.Фамилия') *"
                                   inputmode="text"
                                   spellcheck="true" required="required">
                        </div>
                        <div>
                            <input type="text" name="email"
                                   placeholder="@lang('cms-orders::site.E-mail')"
                                   value="{{ array_get($userInfo, 'email') }}">
                        </div>
                        <div>
                            <input type="tel" name="phone"
                                   value="{{ array_get($userInfo, 'phone') }}"
                                   inputmode="tel"
                                   placeholder="@lang('cms-orders::site.Телефон') *"
                                   required="required">
                        </div>
                        <button type="submit">@lang('cms-orders::site.Далее')</button>
                    </form>
                @else
                    <div>
                        <div>@lang('cms-orders::site.Постоянный клиент?')</div>
                        <span class="js-import" data-mfp="ajax" data-mfp-src="{{ route('auth.login-form') }}"
                            data-param='{"redirect": "{{ route('checkout.step1') }}"}'>
                            @lang('cms-orders::site.Войти')
                        </span>
                    </div>
                    <form class="js-import" method="post" action="{{ route('checkout.save-step1') }}">
                        <div>
                            <input type="text" name="email"
                                   value="{{ array_get($userInfo, 'email') }}"
                                   placeholder="@lang('cms-orders::site.Электронная почта')">
                        </div>
                        <div>
                            <input type="tel" name="phone"
                                   value="{{ array_get($userInfo, 'phone') }}"
                                   placeholder="@lang('cms-orders::site.Номер телефона') *"
                                   inputmode="tel"
                                   required="required">
                        </div>
                        <div>
                            <label class="form-group__label">@lang('cms-orders::site.Имя') *</label>
                            <input type="text" name="name"
                                   value="{{ array_get($userInfo, 'name') }}"
                                   placeholder="@lang('cms-orders::site.Имя') *"
                                   inputmode="text"
                                   spellcheck="true" required="required">
                        </div>
                        <div>
                            <input type="text" name="surname"
                                   value="{{ array_get($userInfo, 'surname') }}"
                                   placeholder="@lang('cms-orders::site.Фамилия') *"
                                   inputmode="text"
                                   spellcheck="true" required="required">
                        </div>
                        <div>
                            <button type="submit">@lang('cms-orders::site.Продолжить')</button>
                        </div>
                    </form>
                    @widget('cabinet-auth-socials')
                @endif
            </div>

            <div>@lang('cms-orders::site.Шаг второй')</div>
            <div>@lang('cms-orders::site.Доставка и оплата')</div>
        </div>
        <div>
            <div data-cart-display="compact" data-cart-order="true">@lang('cms-orders::site.Загрузка')...</div>
        </div>
    </div>
@endsection
