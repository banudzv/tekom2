<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Details\Detail;
use WezomCms\Core\Repositories\AbstractRepository;

class DetailRepository extends AbstractRepository
{
    protected function model()
    {
        return Detail::class;
    }
}
