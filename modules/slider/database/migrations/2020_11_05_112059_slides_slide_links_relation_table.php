<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SlidesSlideLinksRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides_slide_links_relation', function (Blueprint $table) {
            $table->unsignedBigInteger('slide_id');
            $table->foreign('slide_id', 'fk-sslr-slide_id')
                ->references('id')
                ->on('slides')
                ->onDelete('cascade');
            $table->unsignedBigInteger('slide_link_id');
            $table->foreign('slide_link_id', 'fk-sslr-slide_link_id')
                ->references('id')
                ->on('slide_links')
                ->onDelete('cascade');

            $table->primary(['slide_id', 'slide_link_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slides_slide_links_relation');
    }
}

