<?php

use WezomCms\Core\Enums\TranslationSide;

return [
    TranslationSide::ADMIN => [
		'Benefits' => 'Преимущества',
		'Name' => 'Название',
		'For main' => 'Выводить на главной',
		'module site data' => 'Данные для модуля на гл. стр.',
    ],
    TranslationSide::SITE => [
    	'title' => '<strong>Преимущества</strong> работы с нами'
    ],
];
