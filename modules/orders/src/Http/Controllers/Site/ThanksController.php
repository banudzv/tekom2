<?php

namespace WezomCms\Orders\Http\Controllers\Site;

use WezomCms\Core\Http\Controllers\SiteController;

class ThanksController extends SiteController
{
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        $settings = settings('orders.site_thanks', []);
        $pageName = array_get($settings, 'name');

        // Meta
        $this->addBreadcrumb(array_get($settings, 'title') ?: $pageName);
        $this->seo()
            ->setTitle($pageName)
            ->setH1(array_get($settings, 'h1') ?: $pageName);

        return view(
            'cms-orders::site.thanks',
            [
                'text' => 'Ви успішно зробили замовлення',
                'orderId' => $id,
            ]
        );
    }
}
