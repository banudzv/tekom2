@php
    /**
     * @var $models \Illuminate\Database\Eloquent\Collection|\WezomCms\Partners\Models\Category[]
     * @var $partner \WezomCms\Partners\Models\Partner
     */
@endphp
<div class="section section--partners _mt-sm">
    <div class="container">
        <div class="title title--size-h2 _text-center">@lang('cms-partners::site.title')</div>
        <div class="tabs tabs--blog tabs--slider">
            <div class="_grid _mb-sm _mt-lg">
                @foreach($models ?? [] as $category)
                    @widget('ui:button', [
                    'component' => 'button',
                    'attrs' => ['data-wstabs-ns' => 'partners', 'data-wstabs-button' => $category->id],
                    'text' => $category->name,
                    'modificators' => ['button button--tab button--size-sm _mr-xs']
                    ])
                @endforeach
            </div>
            <div class="tabs__body">
                @foreach($models ?? [] as $category)
                    <div class="wstabs-block" data-wstabs-ns="partners" data-wstabs-block="{{ $category->id}}">
                        <div class="tabs__carousel tabs__carousel--partner js-import" dir="ltr" data-slick-carousel='{!! r2d2()->attrJsonEncode([
                            'factory' => 'PartnerSlickCarousel',
                            'props' => [
                                'countOfId' => ''
                                ]]) !!}'>
                            <div class="tabs__carousel-slider" data-slick-carousel-list>
                                @foreach($category->partners ?? [] as $partner)
                                    @include('cms-ui::widgets.partner-item',[
                                          'img' => $partner->getImage(),
                                          'name' => $partner->name
                                     ])
                                @endforeach
                            </div>
                            <div class="tabs__carousel-arrows">
                                <div class="carousel-arrow" data-slick-carousel-prev-arrow>
                                    @svg('arrow-left', 22, 23, '')
                                </div>
                                <div class="carousel-arrow" data-slick-carousel-next-arrow>
                                    @svg('arrow-right', 22, 23, '')
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>

