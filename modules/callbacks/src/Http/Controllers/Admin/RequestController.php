<?php

namespace WezomCms\Callbacks\Http\Controllers\Admin;

use Illuminate\Foundation\Http\FormRequest;
use WezomCms\Callbacks\Http\Requests\Admin\CallbackRequest;
use WezomCms\Callbacks\Http\Requests\Admin\QualityControlRequest;
use WezomCms\Callbacks\Models\QualityControl;
use WezomCms\Callbacks\Models\Request;
use WezomCms\Core\Http\Controllers\AbstractCRUDController;
use WezomCms\Services\Repositories\ServiceRepository;

class RequestController extends AbstractCRUDController
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = Request::class;

    /**
     * Base view path name.
     *
     * @var string
     */
    protected $view = 'cms-callbacks::admin.request';

    /**
     * Resource route name.
     *
     * @var string
     */
    protected $routeName = 'admin.callback-requests';

    /**
     * Form request class name.
     *
     * @var string
     */
    protected $request = CallbackRequest::class;
    /**
     * @var ServiceRepository
     */
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        parent::__construct();
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * Resource name for breadcrumbs and title.
     *
     * @return string
     */
    protected function title(): string
    {
        return __('cms-callbacks::admin.Request');
    }

    /**
     * @param  QualityControl  $model
     * @param  FormRequest  $request
     * @return array
     */
    protected function fill($model, FormRequest $request): array
    {
        $data = parent::fill($model, $request);
        if(isset($data['service_id']) && $data['service_id'] == 0){
            $data['service_id'] = null;
        }

        return $data;
    }

    /**
     * @param  QualityControl $model
     * @param  array  $viewData
     * @return array
     */
    protected function createViewData($model, array $viewData): array
    {
        return [
            'services' => $this->serviceRepository->getBySelect(),
            'selectedService' => [],

        ];
    }

    /**
     * @param  QualityControl $model
     * @param  array  $viewData
     * @return array
     */
    protected function editViewData($model, array $viewData): array
    {
        return [
            'services' => $this->serviceRepository->getBySelect(),
            'selectedService' => [$model->service_id],
        ];
    }
}


