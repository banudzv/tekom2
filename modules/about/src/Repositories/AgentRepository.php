<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Agent;
use WezomCms\Core\Repositories\AbstractRepository;

class AgentRepository extends AbstractRepository
{
    protected function model()
    {
        return Agent::class;
    }
}
