@extends('cms-ui::layouts.base', ['lightHeader' => true])


@section('main')

    <div class="section section--search-results">
        <div class="search-results">
            <div class="container">
                <div class="search-results__title _mb-sm _md:pb-lg">
                    @lang('cms-home::site.search results found', [
                        'search' => $search,
                        'count' => $count
                    ])
                </div>

                @if($services->isNotEmpty())
                    <div class="search-results__subtitle">
                        @lang('cms-home::site.in services')
                    </div>
                    <div class="_grid _spacer _spacer--md _justify-center _md:justify-start">
                        @foreach($services ?? [] as $service)
                            <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                                <div class="service-card service-card--theme-related">
                                    <div class="service-card__body">
                                        <div class="service-card__top">
                                            <div class="service-card__icon">
                                                @svg($service->icon)
                                            </div>
                                            <div class="service-card__title">
                                                {{ $service->name }}
                                            </div>
                                        </div>
                                        <div class="service-card__text">
                                            {!! $service->shortDescription() !!}
                                        </div>
                                    </div>
                                    <div class="_flex _justify-start">
                                        @widget('ui:button', [
                                            'component' => 'a',
                                            'attrs' => [
                                                'href' => route('services.inner',['slug' => $service->slug])
                                            ],
                                            'text' => __('cms-services::site.more details'),
                                            'modificators' => [
                                                'color-accent',
                                                'size-md',
                                                'uppercase',
                                                'ls-def',
                                                'bold'
                                            ]
                                        ])
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

                @foreach($articles as $title => $data)
                    <div class="search-results__subtitle _md:pt-lg _mt-sm">
                        {{ $title }}
                    </div>
                    <div class="_grid _spacer _spacer--md _justify-center _md:justify-start">
                        @foreach($data ?? [] as $article)
                            <div class="_cell _cell--12 _sm:cell--8 _md:cell--6 _df:cell--4 _lg:cell--3">
                                @include('cms-ui::widgets.blog-item', [
                                       'img' => $article->getPreview(),
                                       'text' => $article->name,
                                       'date' => $article->published_human,
                                       'href' => $article->getFrontUrl(),
                                       'class' => 'blog-item--theme-white'
                                  ])
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="_mb-df">
        @include('cms-ui::widgets.feedback-form')
    </div>
@endsection
