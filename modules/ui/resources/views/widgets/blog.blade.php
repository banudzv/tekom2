@php
    $items = [
          ['img' => '/static/images/blog1.jpg',
              'date' => '01.06.2020',
              'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль'
          ],
          ['img' => '/static/images/blog1.jpg',
              'date' => '01.06.2020',
              'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль'
          ],
          ['img' => '/static/images/blog2.jpg',
              'date' => '01.06.2020',
              'text' => 'СК "Теком" выплатила более 830 тыс.гр'
          ],
          ['img' => '/static/images/blog1.jpg',
              'date' => '01.06.2020',
               'text' => 'СК "Теком" выплатила более 830 тыс.гр'
          ],
            ['img' => '/static/images/blog3.jpg',
              'date' => '01.06.2020',
                  'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль'
          ],
          ['img' => '/static/images/blog1.jpg',
              'date' => '01.06.2020',
              'text' => 'D-Link представляет новую аппаратную версию популярного IP-телефона с поддержкой PoE DPH-400GE.'
          ],
          ['img' => '/static/images/blog1.jpg',
              'date' => '01.06.2020',
              'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль'
          ],
          ['img' => '/static/images/blog4.jpg',
              'date' => '01.06.2020',
              'text' => 'СК "Теком" выплатила более 830 тыс.грн. за поврежденный специальный автомобиль'
          ]
      ];
@endphp
<div class="title title--size-h2 _text-center">
    Блог
</div>
<div class="tabs tabs--blog">
    <div class="_grid _mb-lg _mt-lg">
        @widget('ui:button', [
            'component' => 'button',
            'attrs' => ['data-wstabs-ns' => 'accordion-example-single', 'data-wstabs-button' => '1'],
            'text' => 'Акции',
            'modificators' => ['button button--tab button--size-sm _mr-xs _mb-xs']
        ])
        @widget('ui:button', [
            'component' => 'button',
            'attrs' => ['data-wstabs-ns' => 'accordion-example-single', 'data-wstabs-button' => '2'],
            'text' => 'Новости',
            'modificators' => ['button button--tab button--size-sm _mr-xs _mb-xs']
        ])
        @widget('ui:button', [
            'component' => 'button',
            'attrs' => ['data-wstabs-ns' => 'accordion-example-single', 'data-wstabs-button' => '3'],
            'text' => 'Статьи',
            'modificators' => ['button button--tab button--size-sm _mb-xs']
        ])
    </div>
    <div class="tabs__body">
        <div class="wstabs-block" data-wstabs-ns="accordion-example-single" data-wstabs-block="1">
            <div class="tabs__carousel js-import" data-slick-carousel='{!! r2d2()->attrJsonEncode([
                'factory' => 'InfoSlickCarousel',
                'props' => [
                    'countOfId' => ''
                    ]]) !!}'>
                <div class="tabs__carousel-slider" data-slick-carousel-list>
                    @foreach($items as $item)
                        @include('cms-ui::widgets.blog-item',[
                               'img' => $item['img'],
                                 'text' => $item['text'],
                               'date' => $item['date']
                          ])
                    @endforeach
                </div>
                <div class="tabs__carousel-arrows">
                    <div class="carousel-arrow" data-slick-carousel-prev-arrow>
                        @svg('arrow-left', 22, 23, '')
                    </div>
                    <div class="carousel-arrow" data-slick-carousel-next-arrow>
                        @svg('arrow-right', 22, 23, '')
                    </div>
                </div>
            </div>
        </div>
        <div class="wstabs-block" data-wstabs-ns="accordion-example-single" data-wstabs-block="2">
            <div class="tabs__carousel js-import" data-slick-carousel='{!! r2d2()->attrJsonEncode([
                'factory' => 'InfoSlickCarousel2',
                'props' => [
                    'countOfId' => ''
                    ]]) !!}'>
                <div class="tabs__carousel-slider" data-slick-carousel-list>
                    @foreach($items as $item)
                        @include('cms-ui::widgets.blog-item',[
                               'img' => $item['img'],
                                 'text' => $item['text'],
                               'date' => $item['date']
                          ])
                    @endforeach
                </div>
                <div class="tabs__carousel-arrows">
                    <div class="carousel-arrow" data-slick-carousel-prev-arrow>
                        @svg('arrow-left', 22, 23, '')
                    </div>
                    <div class="carousel-arrow" data-slick-carousel-next-arrow>
                        @svg('arrow-right', 22, 23, '')
                    </div>
                </div>
            </div>
        </div>
        <div class="wstabs-block" data-wstabs-ns="accordion-example-single" data-wstabs-block="3">
            <div class="tabs__carousel js-import" data-slick-carousel='{!! r2d2()->attrJsonEncode([
                'factory' => 'InfoSlickCarousel3',
                'props' => [
                    'countOfId' => ''
                ]]) !!}'>
                <div class="tabs__carousel-slider" data-slick-carousel-list>
                    @foreach($items as $item)
                        @include('cms-ui::widgets.blog-item',[
                               'img' => $item['img'],
                                 'text' => $item['text'],
                               'date' => $item['date']
                          ])
                    @endforeach
                </div>
                <div class="tabs__carousel-arrows">
                    <div class="carousel-arrow carousel-arrow__prev" data-slick-carousel-prev-arrow>
                        @svg('arrow-left', 22, 23, '')
                    </div>
                    <div class="carousel-arrow carousel-arrow__next" data-slick-carousel-next-arrow>
                        @svg('arrow-right', 22, 23, '')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

