<?php

namespace WezomCms\Reports\ModelFilters;

use EloquentFilter\ModelFilter;
use WezomCms\Reports\Models\Category;
use WezomCms\Core\Contracts\Filter\FilterListFieldsInterface;
use WezomCms\Core\Filter\FilterField;

/**
 * Class CategoryFilter
 * @package WezomCms\Catalog\ModelFilters
 * @mixin Category
 */
class CategoryFilter extends ModelFilter implements FilterListFieldsInterface
{
    /**
     * Generate array with fields
     * @return iterable|FilterField[]
     */
    public function getFields(): iterable
    {
        return [
            FilterField::makeName(),
            FilterField::published(),
        ];
    }

    public function name($name)
    {
        $this->related('translations', 'name', 'LIKE', '%' . $name . '%');
    }

    public function published($published)
    {
        $this->where('published', $published);
    }
}
