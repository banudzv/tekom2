<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesObjectInsuranceRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_object_insurance_relation', function (Blueprint $table) {
            $table->unsignedBigInteger('service_id');
            $table->foreign('service_id', 'fk-soir_services')
                ->references('id')
                ->on('services')
                ->onDelete('cascade');
            $table->unsignedBigInteger('object_insurance_id');
            $table->foreign('object_insurance_id', 'fk-soir_object_insurances')
                ->references('id')
                ->on('object_insurances')
                ->onDelete('cascade');

            $table->primary(['service_id', 'object_insurance_id'], 'services_object_insurance_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_object_insurance_relation');
    }
}



