<?php

use Faker\Generator as Faker;
use WezomCms\Orders\Models\OrderStatus;

$factory->define(OrderStatus::class, function (Faker $faker) {
    return [
        'sort' => rand(0, 127),
        'name' => $faker->word,
    ];
});
