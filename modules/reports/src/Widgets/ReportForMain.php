<?php

namespace WezomCms\Reports\Widgets;

use WezomCms\Core\Foundation\Widgets\AbstractWidget;
use WezomCms\Reports\Models\Category;
use WezomCms\Reports\Repositories\CategoryRepository;

class ReportForMain extends AbstractWidget
{
    /**
     * @return array|null
     */
    public function execute(): ?array
    {
        /** @var $models Category[] */
        $models = resolve(CategoryRepository::class)->getByMain();

        if($models->isEmpty()){
            return null;
        }

        return compact('models');
    }
}
