<?php

namespace WezomCms\Reports\Repositories;

use WezomCms\Reports\Models\Report;
use WezomCms\Core\Repositories\AbstractRepository;

class ReportRepository extends AbstractRepository
{
	protected function model()
	{
		return Report::class;
	}

	public function getYears()
    {
        $years = $this->getModelQuery()
            ->published()
            ->select('year')
            ->orderBy('year')
            ->groupBy('year')
            ->get()
            ->pluck('year')
            ->toArray();

        return $years;
    }
}
