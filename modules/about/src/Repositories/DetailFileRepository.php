<?php

namespace WezomCms\About\Repositories;

use WezomCms\About\Models\Details\DetailFile;
use WezomCms\Core\Repositories\AbstractRepository;

class DetailFileRepository extends AbstractRepository
{
    protected function model()
    {
        return DetailFile::class;
    }
}
