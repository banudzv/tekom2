<?php

namespace WezomCms\Services\Http\Controllers\Site;

use Spatie\SchemaOrg\Schema;
use WezomCms\Core\Contracts\Assets\AssetManagerInterface;
use WezomCms\Core\Http\Controllers\SiteController;
use WezomCms\Core\Traits\MicroDataTrait;
use WezomCms\Core\Traits\OGImageTrait;
use WezomCms\Faq\Models\FaqQuestion;
use WezomCms\Services\Models\Service;
use WezomCms\Services\Repositories\ServiceRepository;

class ServicesController extends SiteController
{
    use OGImageTrait;
    use MicroDataTrait;

    /**
     * @var ServiceRepository
     */
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = settings('services.site', []);

        $pageName = array_get($settings, 'name');

        $result = Service::published()
            ->orderBy('sort')
            ->latest('id')
            ->paginate(array_get($settings, 'limit', 10));

        // Breadcrumbs
        $this->addBreadcrumb($pageName, route('services'));

        // SEO
        $this->seo()
            ->setTitle(array_get($settings, 'title'))
            ->setPageName($pageName)
            ->setH1(array_get($settings, 'h1'))
            ->setDescription(array_get($settings, 'description'))
            ->metatags()
            ->setKeywords(array_get($settings, 'keywords'))
            ->setNext($result->nextPageUrl())
            ->setPrev($result->previousPageUrl());

        // Render
        return view('cms-services::site.index', [
            'result' => $result,
        ]);
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \WezomCms\Core\Image\Exceptions\IncorrectImageSizeException
     */
    public function inner($slug)
    {

        /** @var Service $service */
        $service = $this->serviceRepository->getBySlugForFront($slug,[
            'translations', 'group', 'group.parent', 'risks', 'additionals',
            'steps', 'groups', 'groups.parent', 'documents', 'relatedServices',
            'faqs', 'objectInsurances'
        ]);

        $this->setLangSwitchers($service, 'services.inner');

        // Breadcrumbs
        $this->addBreadcrumb(settings('services.site.name'), route('services'));
        $this->addBreadcrumb($service->group->parent->name, $service->group->parent->getFrontUrl());
        $this->addBreadcrumb($service->group->name, $service->group->getFrontUrl());
        $this->addBreadcrumb($service->name, $service->getFrontUrl());

        // SEO
        $this->seo()
            ->setTitle($service->title)
            ->setH1($service->h1)
            ->setSeoText($service->seo_text)
            ->setPageName($service->name)
            ->setDescription($service->description)
            ->metatags()
            ->setKeywords($service->keywords);

        $this->setOGImage($service);

        $this->setMicroData($service);

        $this->faqMicroData($service->faqs);

        $siteSettings = settings('contacts.page-settings', []);
        $hotline = array_get($siteSettings, 'hotline');

        // Render
        return view('cms-services::site.service.index', [
            'service' => $service,
            'hotline' => $hotline
        ]);
    }

    protected function faqMicroData($questions)
    {
        // Render faq micro markup
        $questions = $questions->filter(function (FaqQuestion $question) {
            return $question->question && strip_tags($question->answer);
        });

        if ($questions->isNotEmpty()) {
            $things = $questions->map(function (FaqQuestion $question) {
                return Schema::question()
                    ->name($question->question)
                    ->acceptedAnswer(Schema::answer()->text(strip_tags($question->answer)));
            })
                ->values()
                ->all();

            app(AssetManagerInterface::class)
                ->addInlineScript(
                    Schema::fAQPage()->mainEntity($things)->toScript(),
                    '',
                    ['type' => 'application/ld+json']
                );
        }
    }

    /**
     * @param  Service  $service
     * @throws \WezomCms\Core\Image\Exceptions\IncorrectImageSizeException
     */
    private function setMicroData(Service $service)
    {
        $schemaService = Schema::service()
            ->url($service->getFrontUrl())
            ->name($service->name)
            ->description($service->description);

        if (config('cms.services.services.use_groups')) {
            $group = $service->group;

            if ($group && $group->published) {
                $schemaGroup = Schema::thing()
                    ->url($group->getFrontUrl())
                    ->name($group->name)
                    ->description($group->description);

                $schemaService->category($schemaGroup);
            }
        }

        if ($service->imageExists()) {
            $schemaService->image($service->getImageUrl());
        }

        $this->renderMicroData($schemaService);
    }
}
