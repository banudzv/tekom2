<?php

use Faker\Generator as Faker;

$factory->define(\WezomCms\About\Models\License\License::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\License\LicenseTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
    ];
});

$factory->define(\WezomCms\About\Models\License\Group::class, function (Faker $faker) {
    return [
        'published' => true,
    ];
});

$factory->define(\WezomCms\About\Models\License\GroupTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
    ];
});
