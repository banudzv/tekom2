<?php

namespace WezomCms\Orders\Contracts;

use Illuminate\Http\Request;

interface PaymentDriverInterface
{
    /**
     * PaymentDriverInterface constructor.
     * @param $order
     */
    public function __construct($order);

    /**
     * Generate link to payment system.
     *
     * @param  string  $resultUrl  - Url where the buyer would be redirected.
     * @param  string  $serverUrl
     * @return mixed
     */
    public function redirectUrl(string $resultUrl, string $serverUrl);

    /**
     * Handle server request from payment system.
     *
     * @param  Request  $request
     * @return bool - is successfully payed
     */
    public function handleServerRequest(Request $request): bool;
}
