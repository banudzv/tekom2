<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutContractsOwnershipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_contracts_ownership', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('published')->default(true);
            $table->unsignedBigInteger('contract_group_id')->nullable();
            $table->tinyInteger('sort')->default(0);
            $table->string('file')->nullable();
            $table->timestamps();


            $table->foreign('contract_group_id')
                ->references('id')
                ->on('about_contract_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_contracts_ownership');
    }
}
