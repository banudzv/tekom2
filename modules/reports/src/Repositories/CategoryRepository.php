<?php

namespace WezomCms\Reports\Repositories;

use WezomCms\Reports\Models\Category;
use WezomCms\Core\Repositories\AbstractRepository;

class CategoryRepository extends AbstractRepository
{
    protected function model()
    {
        return Category::class;
    }

    public function getByMain()
    {
        return $this->getModelQuery()
            ->published()
            ->where('for_main', true)
            ->get();
    }

    public function getMainCategory()
    {
        return $this->getModelQuery()->published()
            ->with(['translations'])
            ->where('parent_id', null)
            ->orderBy('sort')
            ->get()
            ->pluck('name', 'slug')
            ->toArray()
            ;
    }

    public function getSubCategory($categorySlug)
    {
        return $this->getModelQuery()->published()->with([
            'parent'
        ])->whereHas('parent', function($q) use($categorySlug){
            $q->publishedWithSlug($categorySlug);
        })
            ->get();
    }

    public function getBySlug($slug)
    {
        return $this->getModelQuery()
            ->publishedWithSlug($slug)
            ->get();
    }

    public function getByFrontForSelect()
    {
        $data = [];
        $categories = $this->getModelQuery()->published()
            ->with(['translations', 'children'])
            ->where('parent_id', null)
            ->orderBy('sort')
            ->get();

        foreach ($categories as $key => $category){
            $data[$key]['name'] = $category->name;
            $data[$key]['slug'] = $category->slug;
            if($category->children->isNotEmpty()){
                foreach($category->children as $k => $child){
                    $data[$key]['children'][$k]['name'] = $child->name;
                    $data[$key]['children'][$k]['slug'] = $child->slug;
                }
            }
        }

        return $data;
    }
}
